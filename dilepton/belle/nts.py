# vim: foldmethod=marker
# py2nb: title=Next-to-soft stability
## Loading data{{{
from pymule import *
from pymule.compress import *


datLP = np.array(uncompress(
    "1:eJxTTMoPSuNmYGAoZgESPpnFJWlMyLyi3Xuv37xfc9u2SM1T4Xz"
    "NtTpbNPm11w9r//HitisydersbZm3Dl2epfMaZ7GktV1Rrob/3I9p"
    "99Dl76zYcd75RZZd0QTts0UH9fns0OSZQmxv5W6fZVe06PvEj09+2"
    "KLLTz0f3/6h7ZRdkc0c25svD+aiy9fmO5qdDvtlV7Q1M/6tf/tcdP"
    "mjDeruf9W07Iv8OXOia3XPossvMK+UbvseaV+0pO9WvvOt3+jyvgv"
    "3iqae6LQvKlbKignaomSPJj9rJgjstC+SW/PX7Wtp3n4AVrh/wg=="
))


datNLP = np.array(uncompress(
    "1:eJxTTMoPSuNmYGAoZgESPpnFJWlMyLyi3Xuv37xfc9u2"
    "6FtOu7OF0fbdaPJrrx/W/uPFbVdUe2RHVOA9sT1o8iyd1z"
    "iLJa3til5Ky7+b2V2GLn9nxY7zzi+y7Ipe8Afs57W6hi7P"
    "FGJ7K3f7LLsiu1vVb1a8NN2LJj/1fHz7h7ZTdkUVU278y5"
    "w1FV2+Nt/R7HTYL7uinMotbEk+X9Hljzaou/9V07Ivevq3"
    "95wRQ+g+NPkF5pXSbd8j7YsO6k5sfbtrC7q878K9oqknOu"
    "2LRO8lRX9bIrIfTX7WTBDYaV8UH2TW/FapbD8AHjWE/Q=="
))
#####################################################################}}}
## Make plot{{{
fig = figure()

plot(datLP[:,0], abs(datLP[:,1]), 'C1o--')
plot(datNLP[:,0], abs(datNLP[:,1]), 'C2o--')

xscale('log') ; yscale('log')

xlabel(r'$\xi = 2E_\gamma/\sqrt s$')
ylabel(r'$\Big|1-\mathcal{M}_{\rm soft} / \mathcal{M}_{\rm exact}\Big|$')

legend(['LP', 'NLP'], loc=4)
mulify(gcf())

fig.savefig('plots/nts.pdf')
#####################################################################}}}
