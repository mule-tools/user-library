# vim: foldmethod=marker
# py2nb: title=Dilepton production at Belle II
from pymule import *
import re
import tarfile
import fnmatch
from tabulate import tabulate, SEPARATING_LINE
setup(folder='belle-legacy/out.tar.bz2', cachefolder='/tmp/mcmule/')
## Validate data{{{
### Consisteny of runs{{{
# To make sure every run is consistent, we load the workers and plot the cross section as a function of the iteration
tf = tarfile.open('belle-legacy/workers.tar.bz2')


def load1(fn):
    dat = re.findall(
        br"internal avgi, sd: *([\.\dE-]+) +([\.\dE-]+)",
        tf.extractfile(fn).read()
    )
    ans = []
    for y,e in dat:
        ans.append([float(y), float(e)])
    return np.array(ans)


def mkplot(pat):
    ans = []
    for fn in tf:
        if not fnmatch.fnmatch(fn.name, pat):
            continue

        dat = load1(fn)
        dat /= dat[-1,0]
        errorbar(np.arange(len(dat)), dat[:,0] - 1, dat[:,1])


# Some of the ee2mmF jobs didn't work
fig, axs = plt.subplots(nrows=2, sharex=True, gridspec_kw={'hspace': 0})
sca(axs[0])
title(r"${\tt ee2mmF}$")
mkplot('workers/worker_*ee2mmF_*')
axvline(20, color='black', linewidth=0.4, zorder=1)
sca(axs[1])
mkplot('workers/broken/worker_*ee2mmF_*')
axvline(20, color='black', linewidth=0.4, zorder=1)
xlabel("iteration")


fig, axs = plt.subplots(nrows=2, sharex=True, gridspec_kw={'hspace': 0})
sca(axs[0])
title(r"${\tt ee2mmR}$")
mkplot('workers/worker_*ee2mmR_*')
ylim(-.01,.01)
axvline(20, color='black', linewidth=0.4, zorder=1)

sca(axs[1])
title(r"${\tt ee2mmA}$")
mkplot('workers/worker_*ee2mmA_*')
xlabel("iteration")
axvline(20, color='black', linewidth=0.4, zorder=1)


fig, axs = plt.subplots(nrows=3, sharex=True, gridspec_kw={'hspace': 0})
sca(axs[0])
mkplot('workers/worker_*eeZmmFX_*')
title(r"${\tt eeZmmFX}$")
sca(axs[1])
mkplot('workers/worker_*eeZmmRX_*')
ylim(-.1,.1)
title(r"${\tt eeZmmRX}$")
sca(axs[2])
mkplot('workers/worker_*eeZmmAX_*')
title(r"${\tt eeZmmAX}$")
xlabel("iteration")
##########################################################}}}
### $\xi_c$ dependence{{{
# NLO QED works
setup(obs='30')
mergefkswithplot([[sigma('ee2mmF')], [sigma('ee2mmR')]]);
# NLO EW works
mergefkswithplot([[sigma('eeZmmFX')], [sigma('eeZmmRX')]]);
# NNLO QED
mergefkswithplot([
    [sigma('ee2mmFFEEEE')],
    [sigma('ee2mmRFEEEE')],
    [sigma('ee2mmRREEEE')]
])


setup(obs='31')
##########################################################}}}
#####################################################################}}}
## Load data{{{
def check_distr(dic):
    r = [
        abs(dic['value'][0] / integratehistogram(dic[i]) - 1)
        for i in [
            'th3CMS', 'th4CMS', 'mttNC'
        ]
    ]
    if sum(r) > 1e-2:
        print(r)
    return dic


def load_data_qed(obs):
    setup(obs='3' + obs)
    setup(sanitycheck=lambda d: d['iteration'] > 1)
    lo = check_distr(scaleset(mergefks(
        sigma('ee2mm0')
    ), 1e6*conv*alpha**2))
    nlo = check_distr(scaleset(mergefks(
        sigma('ee2mmF'), sigma('ee2mmR'),
        anyxi=sigma('ee2mmA')
    ), 1e6*conv*alpha**3))

    nnloNoVP = mergefks(
        sigma('ee2mmFFEEEE'), sigma('ee2mmRFEEEE'), sigma('ee2mmRREEEE')
    )
    nnloVP = mergefks(
        sigma('ee2mmAFEE'), sigma('ee2mmAREE'),
        anyxiAA=sigma('ee2mmAA'), anyxiNF=sigma('ee2mmNFEE')
    )
    nnlo = check_distr(scaleset(addsets([nnloVP, nnloNoVP]), 1e6*conv*alpha**4))

    return lo, nlo, nnlo


def load_data_ew(obs):
    setup(obs='3' + obs)
    loZ = check_distr(scaleset(mergefks(sigma('eeZmm0')), 1e6*conv*alpha**2))
    nloZ = check_distr(scaleset(mergefks(
        sigma('eeZmmFX'), sigma('eeZmmRX'),
        anyxi=sigma('eeZmmAX')
    ), 1e6*conv*alpha**3))

    return loZ, nloZ

def load_data_ew_0X(obs):
    setup(obs='3' + obs)
    loZX = check_distr(scaleset(mergefks(sigma('eeZmm0X')), 1e6*conv*alpha**2))

    return loZX

# Let us load data for all polarisation configurations. The numbering is
# ```fortran
#   select case(pola)
#     case(0)
#       pol1 = 0.
#       pol2 = 0.
#     case(1)  ! -+ in the paper
#       pol1 = boost_back(q1, +(/ 0., 0., .7, 0./))
#       pol2 = boost_back(q2, +(/ 0., 0., .7, 0./))
#     case(2)  ! ++ in the paper
#       pol1 = boost_back(q1, +(/ 0., 0., .7, 0./))
#       pol2 = boost_back(q2, -(/ 0., 0., .7, 0./))
#     case(3)  ! -- in the paper
#       pol1 = boost_back(q1, -(/ 0., 0., .7, 0./))
#       pol2 = boost_back(q2, +(/ 0., 0., .7, 0./))
#     case(4)  ! +- in the paper
#       pol1 = boost_back(q1, -(/ 0., 0., .7, 0./))
#       pol2 = boost_back(q2, -(/ 0., 0., .7, 0./))
#   end select
# ```
lo0, nlo0, nnlo0 = load_data_qed('0')
lo1, nlo1, nnlo1 = load_data_qed('1')
lo2, nlo2, nnlo2 = load_data_qed('2')

loZ0X = load_data_ew_0X('0')
loZ0, nloZ0 = load_data_ew('0')
loZ1X = load_data_ew_0X('1')
loZ1, nloZ1 = load_data_ew('1')
loZ2X = load_data_ew_0X('2')
loZ2, nloZ2 = load_data_ew('2')
loZ3X = load_data_ew_0X('3')
loZ3, nloZ3 = load_data_ew('3')
loZ4X = load_data_ew_0X('4')
loZ4, nloZ4 = load_data_ew('4')

NLO0 = addsets([lo0, nlo0])
QED0 = addsets([lo0, nlo0, nnlo0])
Z0 = addsets([loZ0, nloZ0])
ALL0 = addsets([QED0, Z0])

NLO1 = addsets([lo1, nlo1])
QED1 = addsets([lo1, nlo1, nnlo1])
Z1 = addsets([loZ1, nloZ1])
ALL1 = addsets([QED1, Z1])

NLO2 = addsets([lo2, nlo2])
QED2 = addsets([lo2, nlo2, nnlo2])
Z2 = addsets([loZ2, nloZ2])
ALL2 = addsets([QED2, Z2])

Z3 = addsets([loZ3, nloZ3])
ALL3 = addsets([QED2, Z3])

Z4 = addsets([loZ4, nloZ4])
ALL4 = addsets([QED1, Z4])

#####################################################################}}}
## Cross section and asymmetries{{{
# We define the asymmetry as
# \begin{align}
#     A_{\rm FB} = \frac{
#         \int_{+1}^{ 0}\D(\cos\theta_{\tau^-}^*)\tfrac{\D\sigma}{\D\cos\theta_{\tau^-}^*}
#        -\int_{ 0}^{-1}\D(\cos\theta_{\tau^-}^*)\tfrac{\D\sigma}{\D\cos\theta_{\tau^-}^*}
#    }{
#         \int_{+1}^{ 0}\D(\cos\theta_{\tau^-}^*)\tfrac{\D\sigma}{\D\cos\theta_{\tau^-}^*}
#        +\int_{ 0}^{-1}\D(\cos\theta_{\tau^-}^*)\tfrac{\D\sigma}{\D\cos\theta_{\tau^-}^*}
#    }
# \end{align}
# 
def asym(dic, ang='th3CMS'):
    spec = mergebins(dic[ang],90)
    diff = plusnumbers(-spec[1,1:],spec[2,1:])
    tot  = plusnumbers(spec[1,1:],+spec[2,1:])
    return dividenumbers(diff,tot)

print(tabulate([
    [
        "0", "\sigma/pb",
        printnumber(lo0['value']),
        printnumber(nlo0['value']),
        printnumber(nnlo0['value']),
        printnumber(Z0['value']),
        ""
    ], [
        "0", "dK / %",
        "",
        printnumber(100*dividenumbers(nlo0['value'], lo0['value'])),
        printnumber(100*dividenumbers(nnlo0['value'], NLO0['value'])),
        printnumber(100*dividenumbers(Z0['value'], QED0['value'])),
        ""
    ], [
        "0", "AFB",
        printnumber(asym(lo0)),
        printnumber(asym(NLO0)),
        "n/a",
        printnumber(asym(ALL0)),
        ""
    ],
    SEPARATING_LINE,
    [
        "-+", "\sigma/pb",
        printnumber(lo1['value']),
        printnumber(nlo1['value']),
        printnumber(nnlo1['value']),
        printnumber(Z1['value']),
        printnumber(Z4['value'])
    ], [
        "-+", "dK / %",
        "",
        printnumber(100*dividenumbers(nlo1['value'], lo1['value'])),
        printnumber(100*dividenumbers(nnlo1['value'], NLO1['value'])),
        printnumber(100*dividenumbers(Z1['value'], QED1['value'])),
        printnumber(100*dividenumbers(Z4['value'], QED1['value']))
    ], [
        "-+", "AFB",
        printnumber(asym(lo1)),
        printnumber(asym(NLO1)),
        "n/a",
        printnumber(asym(ALL1)),
        printnumber(asym(ALL4))
    ],
    SEPARATING_LINE,
    [
        "++", "\sigma/pb",
        printnumber(lo2['value']),
        printnumber(nlo2['value']),
        printnumber(nnlo2['value']),
        printnumber(Z2['value']),
        printnumber(Z3['value'])
    ], [
        "++", "dK / %",
        "",
        printnumber(100*dividenumbers(nlo2['value'], lo2['value'])),
        printnumber(100*dividenumbers(nnlo2['value'], NLO2['value'])),
        printnumber(100*dividenumbers(Z2['value'], QED2['value'])),
        printnumber(100*dividenumbers(Z3['value'], QED2['value']))
    ], [
        "++", "AFB",
        printnumber(asym(lo2)),
        printnumber(asym(NLO2)),
        "n/a",
        printnumber(asym(ALL2)),
        printnumber(asym(ALL3))
    ]
], headers=[
    "P", "",
    "\sigma^(0)_QED", "\sigma^(1)_QED", "\sigma^(2)_QED",
    "\sigma_EW",
    "\sigma_EW"
]))

def forwardpart(dic,ang='th3CMS'):
    spec = mergebins(dic[ang],90)
    return 90*spec[2,1:]

def backwardpart(dic,ang='th3CMS'):
    spec = mergebins(dic[ang],90)
    return 90*spec[1,1:]

print(tabulate([
    [
        "0", "\sigma^f/pb",
        printnumber(forwardpart(loZ0)),
        printnumber(forwardpart(nloZ0)),
        printnumber(forwardpart(loZ0X))
    ],[
        "0", "\sigma^b/pb",
        printnumber(backwardpart(loZ0)),
        printnumber(backwardpart(nloZ0)),
        printnumber(backwardpart(loZ0X))
    ],[
        "0", "\sigma/pb",
        printnumber(loZ0['value']),
        printnumber(nloZ0['value']),
        printnumber(loZ0X['value'])
    ],
    SEPARATING_LINE,
    [
        "-+", "\sigma^f/pb",
        printnumber(forwardpart(loZ1)),
        printnumber(forwardpart(nloZ1)),
        printnumber(forwardpart(loZ1X))
    ],[
        "-+", "\sigma^b/pb",
        printnumber(backwardpart(loZ1)),
        printnumber(backwardpart(nloZ1)),
        printnumber(backwardpart(loZ1X))
    ],[
        "-+", "\sigma/pb",
        printnumber(loZ1['value']),
        printnumber(nloZ1['value']),
        printnumber(loZ1X['value'])
    ],
    SEPARATING_LINE,
    [
        "+-", "\sigma^f/pb",
        printnumber(forwardpart(loZ4)),
        printnumber(forwardpart(nloZ4)),
        printnumber(forwardpart(loZ4X))
    ],[
        "+-", "\sigma^b/pb",
        printnumber(backwardpart(loZ4)),
        printnumber(backwardpart(nloZ4)),
        printnumber(backwardpart(loZ4X))
    ],[
        "+-", "\sigma/pb",
        printnumber(loZ4['value']),
        printnumber(nloZ4['value']),
        printnumber(loZ4X['value'])
    ],
    SEPARATING_LINE,
    [
        "++", "\sigma^f/pb",
        printnumber(forwardpart(loZ2)),
        printnumber(forwardpart(nloZ2)),
        printnumber(forwardpart(loZ2X))
    ],[
        "++", "\sigma^b/pb",
        printnumber(backwardpart(loZ2)),
        printnumber(backwardpart(nloZ2)),
        printnumber(forwardpart(loZ2X))
    ],[
        "++", "\sigma/pb",
        printnumber(loZ2['value']),
        printnumber(nloZ2['value']),
        printnumber(loZ2X['value'])
    ],
], headers=[
    "P", "",
    "\sigma^(0)_EW", "\sigma^(1)_EW","\sigma^(0)_EW (1/MZ^2)"
]))
#####################################################################}}}
## Distributions{{{
def mklegend(cols, style, labs, ctx, **kwargs):
    return ctx.legend(
        [matplotlib.lines.Line2D([0], [0], color=c, linestyle=s) for c,s in zip(cols, style)],
        labs,
        **kwargs
    )

def mklab(lab, *args, **kwargs):
    if isinstance(lab, str):
        lab = [lab]
    gca().add_artist(
        mklegend(
            ['black']*len(lab), ['-']*len(lab),
            lab,
            gca(),
            handlelength=0, *args, **kwargs
        )
    )

def mymerge(dat, n, m=1):
    return np.concatenate((
        mergebins(dat[:70], m),
        mergebins(dat[70:151], n)
    ))
### unpolarised $\theta_{\tau^\pm}${{{
def myplot(style, dat, *args, cut=True, **kwargs):
    if style == plot:
        # plot(dat[18:52,0], dat[18:52,1], *args, **kwargs)
        # plot(dat[53:151,0], dat[53:151,1], *args, **kwargs)
        plot(dat[18:151,0], dat[18:151,1], *args, **kwargs)
    elif style == errorband:
        # errorband(dat[18:52], *args, **kwargs)
        # errorband(dat[53:151], *args, **kwargs)
        errorband(dat[18:151], *args, **kwargs)



fig, axs = plt.subplots(
    2, sharex=True, gridspec_kw={'hspace': 0}
)
sca(axs[0])

errorband(lo0['th3'][18:151], col='C2')
myplot(plot, nlo0['th3'], 'C0')
myplot(plot, nlo0['th4'], 'C0--')

sca(axs[1])
myplot(errorband, mymerge(nnlo0['th3'], 3), col='C3')
myplot(plot, Z0['th3'], 'C1')
myplot(plot, Z0['th4'], 'C1--')

axhline(0, color='black', linewidth=0.4, zorder=1)

ylim(-0.08,0.08)
fig.text(0.04, 0.5,
    r'$\D\sigma/\D \theta_{\tau^\pm}\ /\ {\rm pb}$',
    va='center', rotation='vertical'
)
xlabel(r"$\theta_{\tau^\pm}\,/\,{\rm deg}$")
mulify(fig)

mklegend(
    ['C2', 'C0', 'C3', 'C1', 'black', 'black'],
    ['-', '-', '-', '-', '-', '--'],
    [
        r'$\sigma^{(0)}_{\rm QED}$', r'$\sigma^{(1)}_{\rm QED}$', r'$\sigma^{(2)}_{\rm QED}$',
        r'$\sigma_{\rm EW}$',
        r'$\tau^-$', r'$\tau^+$'
    ],
    fig,
    ncol=6,
    loc=9,
)

fig.savefig('plots/th.pdf')

###########################################################}}}
### unpolarised $\theta_{\tau^\pm}$ in the CMS frame{{{
lo0['th3CMS'][0,0] = 0.
nlo0['th3CMS'][0,0] = 0.
nlo0['th4CMS'][0,0] = 0.

fig, axs = plt.subplots(
    2, sharex=True, gridspec_kw={'hspace': 0}
)
sca(axs[0])
errorband(lo0['th3CMS'][0:182], col='C2')

plot(nlo0['th3CMS'][0:182,0], nlo0['th3CMS'][0:182,1], 'C0')
plot(nlo0['th4CMS'][0:182,0], nlo0['th4CMS'][0:182,1], 'C0--')
ylim(0.07,7.25)
yscale('log')

sca(axs[1])
errorband(lo0['th3CMS'][0:182], col='C2')

plot(nlo0['th3CMS'][0:182,0], nlo0['th3CMS'][0:182,1], 'C0')
plot(nlo0['th4CMS'][0:182,0], nlo0['th4CMS'][0:182,1], 'C0--')
errorband(mergebins(nnlo0['th3CMS'][0:182], 3), col='C3')
plot(Z0['th3CMS'][0:181,0], Z0['th3CMS'][0:181,1], 'C1')
plot(Z0['th4CMS'][0:181,0], Z0['th4CMS'][0:181,1], 'C1--')
ylim(-0.05,0.06)
axhline(0, color='black', linewidth=0.4, zorder=1)

fig.text(0.02, 0.5,
    r'$\D\sigma/\D \theta_{\tau^\pm}^*\ /\ {\rm pb}$',
    va='center', rotation='vertical'
)
xlabel(r"$\theta_{\tau^\pm}^*\,/\,{\rm deg}$")
mulify(fig)

h=mklegend(
    ['C2', 'C0', 'C3', 'C1', 'black', 'black'],
    ['-', '-', '-', '-', '-', '--'],
    [
        r'$\sigma^{(0)}_{\rm QED}$', r'$\sigma^{(1)}_{\rm QED}$', r'$\sigma^{(2)}_{\rm QED}$',
        r'$\sigma_{\rm EW}$',
        r'$\tau^-$', r'$\tau^+$'
    ],
    ctx=fig,
    ncol=6,
    loc=9,
)

fig.savefig('plots/thCMS.pdf')
###########################################################}}}
### Polarisation in the CMS frame w/o cuts{{{
# Let us consider the ratio between the polarised and unpolarised
# distributions for the angular distribution of the $\tau^-$, both in
# the lab frame with cuts ($\theta_{\tau^-}$) and the CMS frame
# without ($\theta_{\tau^-}^*$)
# \begin{align}
#     \mathcal{R}^{(*)}(\pm+) =
#         \frac{\D\sigma(\pm+)/\D\theta_{\tau^-}^{(*)}}
#              {\D\sigma( 00 )/\D\theta_{\tau^-}^{(*)}}
#        -\frac{  \sigma(\pm+)}{  \sigma(00)}
# \end{align}
# We include the normalisation factor $\sigma(\pm+)/\sigma(00)$ to
# account for the fact that the integrated cross section is vastly
# different.
def mkplot(dp, d0, k, n=5, **kwargs):
    pp = mergebins(dp[k], n) ; p0 = mergebins(d0[k], n)
    s = integratehistogram(pp) / integratehistogram(p0)
    mask = abs(pp[:,1]) > 0
    errorband([1,100,100]*(
        divideplots(pp, p0)[mask] - [0,s,0]
    ), **kwargs)


fig, axs = plt.subplots(
    2, sharex=True, gridspec_kw={'hspace': 0}
)
sca(axs[0])
mkplot(NLO1, NLO0, 'th3CMS')
mkplot(NLO2, NLO0, 'th3CMS')
mklab(
    r'$\sigma^{(0)}_{\rm QED} + '
    r' \sigma^{(1)}_{\rm QED}$',
    loc='upper right'
)
ylim(-0.9, 0.9)
axhline(0, color='black', linewidth=0.4, zorder=1)

sca(axs[1])
mkplot(ALL1, ALL0, 'th3CMS')
mkplot(ALL2, ALL0, 'th3CMS')
mklab(
    r'$\sigma^{(0)}_{\rm QED} + '
    r' \sigma^{(1)}_{\rm QED} + '
    r' \sigma^{(2)}_{\rm QED} + '
    r' \sigma_{\rm EW}$',
    loc='upper right'
)
ylim(-0.9, 0.9)
axhline(0, color='black', linewidth=0.4, zorder=1)

fig.legend(['$P=-+$', '$P=++$'], ncol=2, loc=9)

fig.text(0.03, 0.5,
    r"$\mathcal{R}(\pm+) / \%$",
    va='center', rotation='vertical'
)
xlabel(r"$\theta_{\tau^-}^*\,/\,{\rm deg}$")

mulify(fig)

fig.savefig('plots/polCMS.pdf')
###########################################################}}}
### Polarisation in the lab frame with cuts{{{
def mkplot(dp, d0, k, n=10, m=5, **kwargs):
    pp = mymerge(dp[k], n, m) ; p0 = mymerge(d0[k], n, m)
    s = integratehistogram(pp) / integratehistogram(p0)
    mask = abs(pp[:,1]) > 0
    errorband([1,100,100]*(
        divideplots(pp, p0)[mask] - [0,s,0]
    ), **kwargs)


fig, axs = plt.subplots(
    2, sharex=True, gridspec_kw={
        'hspace': 0,
        'height_ratios':[1,0.4]}
)

sca(axs[0])
mkplot(ALL1, ALL0, 'th3')
mkplot(ALL2, ALL0, 'th3')
ylim(-5, 5)
axhspan(-0.25,0.25, color=colours.alpha_composite('white', 'black', 0.1), lw=0)
axhline(0, color='black', linewidth=0.4, zorder=1)

mklab(
    r'$\sigma^{(0)}_{\rm QED} + '
    r' \sigma^{(1)}_{\rm QED} + '
    r' \sigma^{(2)}_{\rm QED} + '
    r' \sigma_{\rm EW}$',
    loc='upper right'
)

sca(axs[1])
mkplot(ALL1, ALL0, 'th3')
mkplot(ALL2, ALL0, 'th3')
ylim(-0.25, 0.25)
axhline(0, color='black', linewidth=0.4, zorder=1)

fig.legend(['$P=-+$', '$P=++$'], ncol=2, loc=9)

fig.text(0.03, 0.5,
    r"$\mathcal{R}(\pm+) / \%$",
    va='center', rotation='vertical'
)
xlabel(r"$\theta_{\tau^-}\,/\,{\rm deg}$")

mulify(fig)
fig.savefig('plots/pol.pdf')
###########################################################}}}
#####################################################################}}}
