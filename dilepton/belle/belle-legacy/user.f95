                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions
  use user_dummy, only: pass_cut, userweight, names, filenamesuffix

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 10
  integer, parameter :: nr_bins = 200
  real, parameter :: &
     min_val(nr_q) = (/   0.,   0.,   0.,   0., -2., -2.,  0.  ,  0.  ,  0.  ,  0.  /)
  real, parameter :: &
     max_val(nr_q) = (/ 200., 200., 200., 200., +2., +2., 10.e3, 10.e3, 20.e3, 20.e3/)
  integer :: userdim = 0
  integer :: pola

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer, parameter :: namesLen=6
  integer, parameter :: filenamesuffixLen=10


!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU

  musq = mm**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  integer cut, ex
  SW2 = 0.22620
  ! This read a two digit number AB. A corresponds to the exponent
  ! of the ntsSwitch and B of the polarisation scenario
  read*,cut
  write(filenamesuffix,'(I2)') cut

  ex = cut / 10
  pola = mod(cut, 10)

  ntsSwitch = 10.**real(-ex, kind=prec)
  print*, "This is a run for Belle II using the nominal "
  print*, "cuts 17deg < th < 150deg"
  print*, "ntsSwitch", ntsSwitch
  select case(pola)
    case(0)
      print*, "polarisation: ", pola, " (unpolarised)"
    case(1)
      print*, "polarisation: ", pola, " (++)"
    case(2)
      print*, "polarisation: ", pola, " (+-)"
    case(3)
      print*, "polarisation: ", pola, " (-+)"
    case(4)
      print*, "polarisation: ", pola, " (--)"
  end select

  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: q1Lab(4),q2Lab(4),q3Lab(4),q4Lab(4), q5Lab(4),q6Lab(4)
  real (kind=prec) :: quant(nr_q), theta3, theta4, theta3CMS, theta4CMS

  real (kind=prec), parameter :: Eold = sqrt((4e3+7e3)**2 - (sqrt(4e3**2-Mel**2) - sqrt(7e3**2-Mel**2))**2)/2
  real (kind=prec), parameter :: Enew = 7000._prec
  real (kind=prec), parameter :: Eboost = (Enew * Eold - sqrt((Mel**2-Enew**2)*(Mel**2-Eold**2)))/Mel
  real (kind=prec), parameter :: qLab(4) = (/ 0., 0.,-sqrt(Eboost**2-Mel**2), Eboost /)

  select case(pola)
    case(0)
      pol1 = 0.
      pol2 = 0.
    case(1)
      pol1 = boost_back(q1, +(/ 0., 0., .7, 0./))
      pol2 = boost_back(q2, +(/ 0., 0., .7, 0./))
    case(2)
      pol1 = boost_back(q1, +(/ 0., 0., .7, 0./))
      pol2 = boost_back(q2, -(/ 0., 0., .7, 0./))
    case(3)
      pol1 = boost_back(q1, -(/ 0., 0., .7, 0./))
      pol2 = boost_back(q2, +(/ 0., 0., .7, 0./))
    case(4)
      pol1 = boost_back(q1, -(/ 0., 0., .7, 0./))
      pol2 = boost_back(q2, -(/ 0., 0., .7, 0./))
  end select

  quant = 0.
  pass_cut = .true.
  call fix_mu
  q1Lab = boost_rf(qLab, q1)
  q2Lab = boost_rf(qLab, q2)
  q3Lab = boost_rf(qLab, q3)
  q4Lab = boost_rf(qLab, q4)
  q5Lab = boost_rf(qLab, q5)
  q6Lab = boost_rf(qLab, q6)

  theta3CMS = acos(cos_th(q2,q3))*180/pi
  theta4CMS = acos(cos_th(q2,q4))*180/pi
  theta3 = acos(cos_th(q2Lab,q3Lab))*180/pi
  theta4 = acos(cos_th(q2Lab,q4Lab))*180/pi

  if (theta3 < 17 .or. theta3 > 150) pass_cut((/1,2,5,6,7,8,9/)) = .false.
  if (theta4 < 17 .or. theta4 > 150) pass_cut((/1,2,5,6,7,8,9/)) = .false.

  names(1) = "th3"
  quant(1) = theta3
  names(2) = "th4"
  quant(2) = theta4

  names(3) = "th3CMS"
  quant(3) = theta3CMS
  names(4) = "th4CMS"
  quant(4) = theta4CMS

  names(5) = "eta3"
  quant(5) = eta(q3lab)
  names(6) = "eta4"
  quant(6) = eta(q4lab)

  names(7) = "E3"
  quant(7) = q3Lab(4)
  names(8) = "E4"
  quant(8) = q4Lab(4)

  names(9) = "mttC"
  names(10) = "mttNC"
  quant(9:10) = sqrt(sq(q3+q4))


  END FUNCTION QUANT





  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE

                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!



