echo "conf belle-legacy/ee2mm-belleTau.conf" > menu-nlo.menu
echo "conf belle-legacy/ee2mm-belleTau.conf" > menu-nlo-ew.menu
echo "conf belle-legacy/ee2mm-belleTau.conf" > menu-rf.menu
echo "conf belle-legacy/ee2mm-belleTau.conf" > menu-rr.menu
echo "conf belle-legacy/ee2mm-belleTau.conf" > menu-vp.menu

cat menu-ee2mm-belleTau.menu | grep "ee2mm. "                     >> menu-nlo.menu
cat menu-ee2mm-belleTau.menu | grep "eeZmm"                       >> menu-nlo-ew.menu
cat menu-ee2mm-belleTau.menu | grep "ee2mmRF"                     >> menu-rf.menu
cat menu-ee2mm-belleTau.menu | grep "ee2mmRR"                     >> menu-rr.menu
cat menu-ee2mm-belleTau.menu | grep -E "(ee2mm[NF]F|ee2mmA[FAR])" >> menu-vp.menu
diff <(cat menu-ee2mm-belleTau.menu|sort | grep -v '#' | sed '/^$/d') <(cat menu-vp.menu menu-nlo* menu-r* | sort)


echo "menu-nlo.menu    $(cat menu-nlo.menu    | grep run | wc)"
echo "menu-nlo-ew.menu $(cat menu-nlo-ew.menu | grep run | wc)"
echo "menu-rf.menu     $(cat menu-rf.menu     | grep run | wc)"
echo "menu-rr.menu     $(cat menu-rr.menu     | grep run | wc)"
echo "menu-vp.menu     $(cat menu-vp.menu     | grep run | wc)"
