                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions
  use user_dummy, only: pass_cut, userweight, names, filenamesuffix

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 68
  integer, parameter :: nr_bins = 250
  real, parameter :: &
     min_val(nr_q) = [(/ 0.5,  0.5,  0.5,    0.5,    0.0,  0.0,  0.0,   0.0,  0.0,  0.0,&
                         0.5,  0.5,  0.5,    0.5,    0.0,  0.0,  0.0,   0.0,  0.0,  0.0,&
                         0.0,  0.0,  -2500., -2500., 0.0,  0.0 /), &
                         spread(0.5, 1, 2*21)&
                      ]
  real, parameter :: &
     max_val(nr_q) = [(/ 6.75 ,  6.75 ,   6.75 ,   6.75,  2500.,  2500.,   2500.,   2500.,  5.,  5.,&
                         6.75 ,  6.75 ,   6.75 ,   6.75,  2500.,  2500.,   2500.,   2500.,  5.,  5.,&
                         25.,   2500.,   0.,      0.,    250.,   10. /),&
                         spread(6.75, 1, 2*21)&
                      ]


  real(kind=prec) :: nsigma, sigmaE, sigmaPhi, thetaCut(2), q2Cut(2), prad_setting, cut_setting
  integer :: userdim = 0

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer, parameter :: namesLen=11
  integer, parameter :: filenamesuffixLen=10

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

  musq = me**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  integer cut
  real(kind=prec) :: Ebeam
  character(len=11) :: varname
  integer ido
  read*,cut
  write(filenamesuffix,'(I2)') cut

  print*,"Calculate ee->ee for the PRad collaboration"
  print*,"at NNLO. Configuration", cut

  prad_setting = modulo(cut,10)
  cut_setting = cut/10
  if(prad_setting == 1) then
    print*," * PRad I, 1.1 GeV"
    Ebeam = 1.1e3
    nsigma = 3.5
    sigmaE = 27.3
    sigmaPhi = 2.1
    thetaCut = (/ 0.7, 3.8 /)
    q2Cut = (/ -928.2713105032453, -195.4041396428758 /)
  else if(prad_setting == 2) then
    print*," * PRad I, 2.143 GeV"
    Ebeam = 2.143e3
    nsigma = 3.5
    sigmaE = 38.1
    sigmaPhi = 2.1
    thetaCut = (/ 0.7, 3.8 /)
    q2Cut = (/ -1667.5312430248007, -522.0880168220028 /)
  else if(prad_setting == 3) then
    print*," * PRad II, 0.7 GeV"
    Ebeam = 0.7e3
    nsigma = 3.5
    sigmaE = 21.8
    sigmaPhi = 2.1
    thetaCut = (/ 0.5, 6.5 /)
    q2Cut = (/ -642.6507030157081, -72.22558713053476 /)
  else if(prad_setting == 4) then
    print*," * PRad II, 1.4 GeV"
    Ebeam = 1.4e3
    nsigma = 3.5
    sigmaE = 30.8
    sigmaPhi = 2.1
    thetaCut = (/ 0.5, 6.5 /)
    q2Cut = (/ -1295.1110428413178, -135.16377730481327 /)
  else if(prad_setting == 5) then
    print*," * PRad II, 2.1 GeV"
    Ebeam = 2.1e3
    nsigma = 3.5
    sigmaE = 37.7
    sigmaPhi = 2.1
    thetaCut = (/ 0.5, 6.5 /)
    q2Cut = (/ -1855.271590195861, -290.4017599504441 /)
  end if

  call initflavour("prad", 2*me**2+2*me*Ebeam)

  if(cut_setting == 1) then
    print*, "fiducial cuts with:"
    print*, " * cut_X = ", nSigma, " x sigma_X"
    print*, " * sigma_E = ", SigmaE
    print*, " * sigma_Phi = ", SigmaPhi
  else if(cut_setting == 2) then
    print*, "simplified cuts with:"
    print*, q2cut(1), "< t,u <", q2cut(2)
  end if

  ! 2-dim distributions
  do ido=1,21
    write(varname, '(A,I2.2)') "th_n_e_n_", ido
    names(26+ido) = trim(varname)
    write(varname, '(A,I2.2)') "th_w_e_w_", ido
    names(47+ido) = trim(varname)
  enddo
  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)
  implicit none
  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: quant(nr_q)
  real (kind=prec) :: q1lab(4), q2lab(4), q3lab(4), q4lab(4), q5lab(4), q6lab(4), q3labj(4), q4labj(4)
  real (kind=prec) :: e_beam, phi_3, phi_4
  real (kind=prec) :: pT_34, m_34
  real (kind=prec) :: th_n, th_w, th_s, th_h, e_s, e_h, e_n, e_w, dth_sw, dth_hn
  real (kind=prec) :: th_nj, th_wj, th_sj, th_hj, e_sj, e_hj, e_nj, e_wj, dth_swj, dth_hnj
  real (kind=prec) :: inela, copla, q2_min, q2_max
  integer :: nbin

  !! ==== keep the line below in any case ==== !!
  call fix_mu

  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  quant = 0.
  pass_cut = .true.
  call fix_mu

  !boost to lab frame = rest frame of q2   
  q1lab = boost_rf(q2,q1)
  q2lab = boost_rf(q2,q2)
  q3lab = boost_rf(q2,q3)
  q4lab = boost_rf(q2,q4)
  q5lab = boost_rf(q2,q5)
  q6lab = boost_rf(q2,q6)

  q3labj = merge2jet(q3lab, q5lab, q6lab)
  q4labj = merge2jet(q4lab, q5lab, q6lab)

  e_beam = q1lab(4)


  ! compute measurable quantities for electrons
  call get_th_e(q3Lab , q4Lab , th_n , th_w , th_s , th_h , E_n , E_w , E_s , E_h )
  dth_sw = abs(th_s - th_w)
  dth_hn = abs(th_h - th_n)
  ! compute measurable quantities for jets
  call get_th_e(q3LabJ, q4LabJ, th_nJ, th_wJ, th_sJ, th_hJ, E_nJ, E_wJ, E_sJ, E_hJ)
  dth_swj = abs(th_sj - th_wj)
  dth_hnj = abs(th_hj - th_nj)

  call minmax(sq(q1-q3), sq(q1-q4), q2_min, q2_max)

  pT_34 = PT(q3lab + q4lab)
  m_34 = sq(q3+q4)
  inela = e_beam + me - E_s - E_h

  phi_3 = phi(q3lab)*180/pi
  phi_4 = phi(q4lab)*180/pi
  copla = abs(180 - abs(phi_3 - phi_4))

  ! cuts
  if(cut_setting == 1) then
    if(th_n < thetaCut(1)) pass_cut = .false.
    if(thetaCut(2) < th_w) pass_cut = .false.
    if(inela > nSigma*sigmaE) pass_cut = .false.
    if(copla > nSigma*sigmaPhi) pass_cut = .false.
  else if(cut_setting == 2) then
    if(q2_min < q2Cut(1)) pass_cut = .false.
    if(q2Cut(2) < q2_max) pass_cut = .false.
  end if

  if(.not.any(pass_cut)) return

  ! 1-dim distributions
  names(1) = "th_n"
  names(2) = "th_w"
  names(3) = "th_s"
  names(4) = "th_h"
  names(5) = "e_n"
  names(6) = "e_w"
  names(7) = "e_s"
  names(8) = "e_h"
  names(9) = "dth_sw"
  names(10) = "dth_hn"
  names(11) = "th_nj"
  names(12) = "th_wj"
  names(13) = "th_sj"
  names(14) = "th_hj"
  names(15) = "e_nj"
  names(16) = "e_wj"
  names(17) = "e_sj"
  names(18) = "e_hj"
  names(19) = "dth_swj"
  names(20) = "dth_hnj"
  names(21) = "pT_34"
  names(22) = "m_34"
  names(23) = "q2_min"
  names(24) = "q2_max"
  names(25) = "inela"
  names(26) = "copla"


  quant(1) = th_n
  quant(2) = th_w
  quant(3) = th_s
  quant(4) = th_h
  quant(5) = e_n
  quant(6) = e_w
  quant(7) = e_s
  quant(8) = e_h
  quant(9) = dth_sw
  quant(10) = dth_hn
  quant(11) = th_nj
  quant(12) = th_wj
  quant(13) = th_sj
  quant(14) = th_hj
  quant(15) = e_nj
  quant(16) = e_wj
  quant(17) = e_sj
  quant(18) = e_hj
  quant(19) = dth_swj
  quant(20) = dth_hnj
  quant(21) = pT_34
  quant(22) = m_34
  quant(23) = q2_min
  quant(24) = q2_max
  quant(25) = inela
  quant(26) = copla

  pass_cut(27:47) = .false.
  nbin = wbin(e_n,0.,2500.) + 26
  pass_cut(nbin) = .true.
  quant(nbin) = th_n

  pass_cut(48:68) = .false.
  nbin = wbin(e_w,0.,2500.) + 47
  pass_cut(nbin) = .true.
  quant(nbin) = th_w

  END FUNCTION QUANT


  SUBROUTINE USEREVENT(X, NDIM)
  implicit none
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE USEREVENT



  FUNCTION WBIN(X, XMI, XMA)
  implicit none
  integer wbin
  integer, parameter :: nr = 21
  real(kind=prec) :: x, xmi, xma
  real(kind=prec) :: bin_length

  bin_length = (xma - xmi)/nr
  if(abs(x-xmi).lt.zero) then
    ! first bin, corner
    wbin = 1
  elseif(abs(x-xma).lt.zero) then
    ! last bin, corner
    wbin = nr
  elseif(x > xmi .and. x < xma) then
    ! somewhere in the middle
    wbin = int(ceiling((x - xmi)/bin_length ))
  elseif (x < xmi) then
    ! underflow
    wbin = 1
  elseif (x > xma) then
    ! overflow
    wbin = nr
  endif
  END FUNCTION WBIN


  FUNCTION MERGE2JET(p,k1,k2)
  implicit none
  real(kind=prec) :: merge2jet(4)
  real(kind=prec) :: p(4), k1(4), k2(4)
  real(kind=prec) :: th1, th2, mergeCut
  mergeCut = nSigma * sigmaPhi

  merge2jet = p

  if(k1(4) > zero) then
    th1 = acos(cos_th(p,k1))*180/pi
    if(th1 < mergeCut) merge2jet = merge2jet + k1
  endif

  if(k2(4) > zero) then
    th2 = acos(cos_th(p,k2))*180/pi
    if(th2 < mergeCut) merge2jet = merge2jet + k2
  endif
  END FUNCTION MERGE2JET


  SUBROUTINE GET_TH_E(q3, q4, thN, thW, thS, thH,&
                               En,  Ew,  Es,  Eh)
  implicit none
  real(kind=prec), parameter :: ez(4) = (/ 0., 0., 1._prec, 0._prec/)
  real(kind=prec), intent(in) :: q3(4), q4(4)
  real(kind=prec), intent(out) :: thN, thW, En, Ew
  real(kind=prec), intent(out) :: ths, thh, Es, Eh
  real(kind=prec) :: e3, e4, th3, th4

  e3 = q3(4)
  e4 = q4(4)
  th3 = acos(cos_th(ez,q3))*180/pi
  th4 = acos(cos_th(ez,q4))*180/pi

  if(th3 < th4) then
    thn = th3
    en = e3
    thw = th4
    ew = e4
  else
    thn = th4
    en = e4
    thw = th3
    ew = e3
  end if

  if(e3 < e4) then
    ths = th3
    es = e3
    thh = th4
    eh = e4
  else
    ths = th4
    es = e4
    thh = th3
    eh = e3
  end if

  END SUBROUTINE GET_TH_E

  SUBROUTINE MINMAX(A, B, mi, ma)
  implicit none
  real(kind=prec), intent(in) :: a, b
  real(kind=prec), intent(out) :: mi, ma

  mi = min(A, B)
  ma = max(A, B)
  END SUBROUTINE

                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
