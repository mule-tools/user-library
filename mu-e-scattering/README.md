# Muon electron scattering $`\mu e\to \mu e`$

References: [[2007.01654]](https://arxiv.org/abs/2007.01654)

Configurations implemented
 * MUonE experiment, Setups 2 and 4
   [[ref]](https://gitlab.com/mule-tools/user-library/-/jobs/artifacts/master/file/ci/mu-e-scattering/muone/muone.ipynb?job=run)

 * MUonE experiment with cut on $`\theta_\mu`$
   [[ref]](https://gitlab.com/mule-tools/user-library/-/jobs/artifacts/master/file/ci/mu-e-scattering/legacy/legacy.ipynb?job=run)

