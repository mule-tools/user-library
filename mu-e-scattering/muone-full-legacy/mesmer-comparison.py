# vim: foldmethod=marker
# py2nb: title=Comparison with Mesmer
# *This work was done in collaboration with Alessandro Broggio, William J. Torres Bobadilla, Andrea Ferroglia, Manoj Kumar Mandal, Pierpaolo Mastrolia, Jonathan Ronca, Max Zoller and with the help of Ettore Budassi, Carlo Carloni Calame, Clara Del Pio, Andrea Gurgone, Syed Mehedi Hasan, Guido Montagna, Oreste Nicrosini, Fulvio Piccinini*
## Initialisation{{{
from pymule import *
from pymule.plot import twopanel
#########################################################################}}}
## Comparison with Mesmer{{{
# Total cross section for $e\mu\to e\mu\gamma$ @ NLO
# (i.e. RR+RV for $e\mu\to e\mu$ @ NNLO)

rvMM = np.array([[1e-4, -0.404480977, 0.000100070], [1e-5,-0.606934042, 0.000120659], [1e-6, -0.852275813, 0.000138015]])
rvPV = np.array([[1e-4, -0.404418452, 0.000139515], [1e-5,-0.606686531, 0.000221212], [1e-6, -0.852152752, 0.000318787]])

rrMM = np.array([[1e-4, 0.104383508, 0.000020031], [1e-5, 0.181939606, 0.000031621], [1e-6, 0.281112331, 0.000046199]])
rrPV = np.array([[1e-4, 0.104392787, 0.000008349], [1e-5, 0.181935963, 0.000012837], [1e-6, 0.281070148, 0.000024496]])

pv = addplots(rvPV, rrPV)
mm = addplots(rvMM, rrMM)
both = mergeplots([mm,pv])


fig, axs = plt.subplots(2, sharex=True, gridspec_kw={'hspace': 0})

sca(axs[0])
errorbar(mm[:,0],-mm[:,1],mm[:,2], fmt='.')
errorbar(0.9*pv[:,0],-pv[:,1],pv[:,2], fmt='.')
legend([r'\sc McMule', '\sc MESMER'])
yscale('log') ; xscale('log')
ylabel(r'$-\sigma(e\mu\to e\mu\gamma)\,/\,\upmu{\rm b}$')

sca(axs[1])
errorbar(mm[:,0],mm[:,1]-both[:,1],mm[:,2], fmt='.')
errorbar(0.9*pv[:,0],pv[:,1]-both[:,1],pv[:,2], fmt='.')
ylabel(r'$\sigma_i-\langle\sigma\rangle\,/\,\upmu{\rm b}$')
axhline(0, color='black', linewidth=0.4, zorder=1)

xlabel(r'$\xi_c\equiv 2E_{\gamma}^{\rm max}/\sqrt s$')
mulify(gcf())
fig.savefig('plots/comp-pv-totxsec.pdf')


# $\theta_e$ differential cross section for $e\mu\to e\mu\gamma$ @ NLO 
# (i.e. RR+RV for $e\mu\to e\mu$ @ NNLO)
#
# at $\xi_c = 2E_{\gamma}^{\rm max}/\sqrt s = 10^{-6}$

setup(obs='783')
setup(folder='em2em_muone_paper/nlo-rad-mixd-6-mm')
nnlo6 = scaleset(mergefks(sigma('em2emRFMIXD15'),sigma('em2emRFMIXD35'),
                          sigma('em2emRRMIXD1516'),sigma('em2emRRMIXD3536')), alpha**4*conv)

thE = np.loadtxt('em2em_muone_paper/nlo-rad-mixd-6-pv/ethlab_PV_100.txt')
bw = (thE[1,0]-thE[0,0])/2.
thE[:,0] = thE[:,0] + bw


hnnlo6 = mergebins(scaleplot(nnlo6['thetae'],1e-3),1)
fig,(ax1,ax2)=twopanel(
    r'$\theta_e\,/\,{\rm mrad}$',
    upleft=[hnnlo6, thE],
    downleft=divideplots(hnnlo6, thE),
    labupleft=r"$\D\sigma/\D\theta_e\,/\,\upmu{\rm b}$",
    labdownleft=r'$\rm rel. difference$'
)
ax1.axhline(0, color='black', linewidth=0.4, zorder=1)
ax1.legend(
    [matplotlib.lines.Line2D([0], [0]), matplotlib.lines.Line2D([0], [0],color='C1')],
    [r'\sc McMule',r'\sc Mesmer']
)
axhline(1, color='black', linewidth=0.4, zorder=1)
mulify(gcf())
savefig('plots/comp-pv-thediff.pdf')

#########################################################################}}}
