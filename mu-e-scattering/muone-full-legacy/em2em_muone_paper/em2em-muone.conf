
# specify the program to run relative to `pwd`
binary=mcmule

# specify the output folder
folder=muone/

# Specify the variables nenter_ad, itmx_ad, nenter and itmx
# for each piece you want to run.

# PHOTONIC-
declare -A STAT=(
  ['em2em0']='1000\n10\n10000\n20'
  ['em2emFEE']='1000\n10\n10000\n20'
  ['em2emFEM']='1000\n10\n10000\n20'
  ['em2emFMM']='1000\n10\n10000\n20'
  ['em2emREE15']='10000\n10\n100000\n20'
  ['em2emREE35']='10000\n10\n100000\n20'
  ['em2emREM']='10000\n10\n100000\n20'
  ['em2emRMM']='10000\n10\n100000\n20'
  ['em2emFFEEEE']='1000\n10\n10000\n20'
  ['em2emFFMIXDz']='10\n2\n24\n10'
  ['em2emFFMMMM']='1000\n10\n10000\n20'
  ['em2emRFEEEE15']='5000\n10\n50000\n20'
  ['em2emRFEEEE35']='5000\n10\n50000\n20'
  ['em2emRFMMMM']='5000\n10\n50000\n20'
  ['em2emRFMIXD15']='2000\n10\n20000\n20'
  ['em2emRFMIXD35']='2000\n10\n20000\n20'
  ['em2emRREEEE1516']='200000\n20\n2000000\n40'
  ['em2emRREEEE3536']='200000\n20\n2000000\n40'
  ['em2emRRMMMM']='200000\n20\n2000000\n40'
  ['em2emRRMIXD1516']='50000\n20\n500000\n40'
  ['em2emRRMIXD3536']='50000\n20\n500000\n40'
)
containerid=d869a672-1f1d-3c5b-a40b-49a36ba638ed

# PHOTONIC-_THM
declare -A STAT=(
  ['em2em0']='1000\n10\n10000\n20'
  ['em2emFEE']='1000\n10\n10000\n20'
  ['em2emFEM']='1000\n10\n10000\n20'
  ['em2emFMM']='1000\n10\n10000\n20'
  ['em2emREE15']='10000\n10\n100000\n20'
  ['em2emREE35']='10000\n10\n100000\n20'
  ['em2emREM']='10000\n10\n100000\n20'
  ['em2emRMM']='10000\n10\n100000\n20'
  ['em2emFFEEEE']='1000\n10\n10000\n20'
  ['em2emFFMIXDz']='10\n2\n24\n10'
  ['em2emFFMMMM']='1000\n10\n10000\n20'
  ['em2emRFEEEE15']='5000\n10\n50000\n20'
  ['em2emRFEEEE35']='5000\n10\n50000\n20'
  ['em2emRFMMMM']='5000\n10\n50000\n20'
  ['em2emRFMIXD15']='2000\n10\n20000\n20'
  ['em2emRFMIXD35']='2000\n10\n20000\n20'
  ['em2emRREEEE1516']='200000\n20\n2000000\n40'
  ['em2emRREEEE3536']='200000\n20\n2000000\n40'
  ['em2emRRMMMM']='200000\n20\n2000000\n40'
  ['em2emRRMIXD1516']='50000\n20\n500000\n40'
  ['em2emRRMIXD3536']='50000\n20\n500000\n40'
)
containerid=362b3b54-2083-3da6-85e2-16cf465ffa57

# PHOTONIC+ & PHOTONIC+_THM
declare -A STAT=(
  ['em2emFEE']='1000\n10\n10000\n20'
  ['em2emFEM']='1000\n10\n10000\n20'
  ['em2emFMM']='1000\n10\n10000\n20'
  ['em2emREE15']='10000\n10\n100000\n20'
  ['em2emREE35']='10000\n10\n100000\n20'
  ['em2emREM']='10000\n10\n100000\n20'
  ['em2emRMM']='10000\n10\n100000\n20'
)
containerid=362b3b54-2083-3da6-85e2-16cf465ffa57

# PHOTONIC-_MIXD & PHOTONIC-_THM_MIXD
# we ran the following without docker, so there is no containerid for them
# refer to the public release of McMule, v.0.4.2
declare -A STAT=(
  ['em2emFF31z']='10\n2\n24\n10'
  ['em2emRF3115']='2000\n10\n20000\n20'
  ['em2emRF3135']='2000\n10\n20000\n20'
  ['em2emRR311516']='50000\n20\n500000\n40'
  ['em2emRR313536']='50000\n20\n500000\n40'
)
declare -A STAT=(
  ['em2emFF22z']='10\n2\n24\n10'
  ['em2emRF2215']='2000\n10\n20000\n20'
  ['em2emRF2235']='2000\n10\n20000\n20'
  ['em2emRR221516']='50000\n20\n500000\n40'
  ['em2emRR223536']='50000\n20\n500000\n40'
)
declare -A STAT=(
  ['em2emFF13z']='10\n2\n24\n10'
  ['em2emRF1315']='2000\n10\n20000\n20'
  ['em2emRF1335']='2000\n10\n20000\n20'
  ['em2emRR131516']='50000\n20\n500000\n40'
  ['em2emRR133536']='50000\n20\n500000\n40'
)

# LEPTONIC- & LEPTONIC-_THM
declare -A STAT=(
  ['em2emA']='1000\n10\n10000\n20'
  ['em2emAA']='1000\n10\n10000\n20'
  ['em2emAF']='1000\n10\n10000\n20'
  ['em2emAR15']='5000\n10\n50000\n20'
  ['em2emAR35']='5000\n10\n50000\n20'
  ['em2emNF']='1000\n10\n10000\n20'
)
containerid=d869a672-1f1d-3c5b-a40b-49a36ba638ed

# LEPTONIC+ & LEPTONIC+_THM
declare -A STAT=(
  ['em2emAF']='1000\n10\n10000\n20'
  ['em2emAR15']='5000\n10\n50000\n20'
  ['em2emAR35']='5000\n10\n50000\n20'
  ['em2emNF']='1000\n10\n10000\n20'
)
containerid=d869a672-1f1d-3c5b-a40b-49a36ba638ed

# HADRONIC- & HADRONIC-_THM
declare -A STAT=(
  ['em2emA']='1000\n10\n10000\n20'
  ['em2emAA']='1000\n10\n10000\n20'
  ['em2emAF']='1000\n10\n10000\n20'
  ['em2emAR15']='5000\n10\n50000\n20'
  ['em2emAR35']='5000\n10\n50000\n20'
  ['em2emNF']='1000\n10\n10000\n20'
)
containerid=d869a672-1f1d-3c5b-a40b-49a36ba638ed

# HADRONIC+ & HADRONIC+_THM
declare -A STAT=(
  ['em2emAF']='1000\n10\n10000\n20'
  ['em2emAR15']='5000\n10\n50000\n20'
  ['em2emAR35']='5000\n10\n50000\n20'
  ['em2emNF']='1000\n10\n10000\n20'
)
containerid=d869a672-1f1d-3c5b-a40b-49a36ba638ed