##RR (91 tasks)

conf em2em_nnlo_acocut/em2em-mu-e.conf


#RRc (8 tasks)

run 88612 0.003000 em2emRREEc muone 9 
run 88612 0.010000 em2emRREEc muone 9 
run 88612 0.030000 em2emRREEc muone 9 
run 88612 0.100000 em2emRREEc muone 9 
run 88612 0.220000 em2emRREEc muone 9 
run 88612 0.300000 em2emRREEc muone 9 
run 88612 1.000000 em2emRREEc muone 9 

#RR1 (42 tasks)

run 88612 0.003000 em2emRREE1 muone 9
run 81198 0.003000 em2emRREE1 muone 9
run 82635 0.003000 em2emRREE1 muone 9
run 80258 0.003000 em2emRREE1 muone 9
run 83838 0.003000 em2emRREE1 muone 9
run 89140 0.003000 em2emRREE1 muone 9

run 88612 0.010000 em2emRREE1 muone 8
run 81198 0.010000 em2emRREE1 muone 8
run 82635 0.010000 em2emRREE1 muone 8
run 80258 0.010000 em2emRREE1 muone 9
run 83838 0.010000 em2emRREE1 muone 9
run 89140 0.010000 em2emRREE1 muone 9

run 88612 0.030000 em2emRREE1 muone 8
run 81198 0.030000 em2emRREE1 muone 8
run 82635 0.030000 em2emRREE1 muone 8
run 80258 0.030000 em2emRREE1 muone 9
run 83838 0.030000 em2emRREE1 muone 9
run 89140 0.030000 em2emRREE1 muone 9

run 88612 0.100000 em2emRREE1 muone 8
run 81198 0.100000 em2emRREE1 muone 8
run 82635 0.100000 em2emRREE1 muone 8
run 80258 0.100000 em2emRREE1 muone 9
run 83838 0.100000 em2emRREE1 muone 9
run 89140 0.100000 em2emRREE1 muone 9

run 88612 0.220000 em2emRREE1 muone 8
run 81198 0.220000 em2emRREE1 muone 8
run 82635 0.220000 em2emRREE1 muone 8
run 80258 0.220000 em2emRREE1 muone 9
run 83838 0.220000 em2emRREE1 muone 9
run 89140 0.220000 em2emRREE1 muone 9

run 88612 0.300000 em2emRREE1 muone 8
run 81198 0.300000 em2emRREE1 muone 8
run 82635 0.300000 em2emRREE1 muone 8
run 80258 0.300000 em2emRREE1 muone 9
run 83838 0.300000 em2emRREE1 muone 9
run 89140 0.300000 em2emRREE1 muone 9

run 88612 1.000000 em2emRREE1 muone 8
run 81198 1.000000 em2emRREE1 muone 8
run 82635 1.000000 em2emRREE1 muone 8
run 80258 1.000000 em2emRREE1 muone 9
run 83838 1.000000 em2emRREE1 muone 9
run 89140 1.000000 em2emRREE1 muone 9

#RR3 (42 tasks)

run 58612 0.003000 em2emRREE3 muone 8
run 51198 0.003000 em2emRREE3 muone 8
run 52635 0.003000 em2emRREE3 muone 8
run 50258 0.003000 em2emRREE3 muone 9
run 53838 0.003000 em2emRREE3 muone 9
run 59140 0.003000 em2emRREE3 muone 9

run 58612 0.010000 em2emRREE3 muone 8
run 51198 0.010000 em2emRREE3 muone 8
run 52635 0.010000 em2emRREE3 muone 8
run 50258 0.010000 em2emRREE3 muone 9
run 53838 0.010000 em2emRREE3 muone 9
run 59140 0.010000 em2emRREE3 muone 9

run 58612 0.030000 em2emRREE3 muone 8
run 51198 0.030000 em2emRREE3 muone 8
run 52635 0.030000 em2emRREE3 muone 8
run 50258 0.030000 em2emRREE3 muone 9
run 53838 0.030000 em2emRREE3 muone 9
run 59140 0.030000 em2emRREE3 muone 9

run 58612 0.100000 em2emRREE3 muone 8
run 51198 0.100000 em2emRREE3 muone 8
run 52635 0.100000 em2emRREE3 muone 8
run 50258 0.100000 em2emRREE3 muone 9
run 53838 0.100000 em2emRREE3 muone 9
run 59140 0.100000 em2emRREE3 muone 9

run 58612 0.220000 em2emRREE3 muone 8
run 51198 0.220000 em2emRREE3 muone 8
run 52635 0.220000 em2emRREE3 muone 8
run 50258 0.220000 em2emRREE3 muone 9
run 53838 0.220000 em2emRREE3 muone 9
run 59140 0.220000 em2emRREE3 muone 9

run 58612 0.300000 em2emRREE3 muone 8
run 51198 0.300000 em2emRREE3 muone 8
run 52635 0.300000 em2emRREE3 muone 8
run 50258 0.300000 em2emRREE3 muone 9
run 53838 0.300000 em2emRREE3 muone 9
run 59140 0.300000 em2emRREE3 muone 9

run 58612 1.000000 em2emRREE3 muone 8
run 51198 1.000000 em2emRREE3 muone 8
run 52635 1.000000 em2emRREE3 muone 8
run 50258 1.000000 em2emRREE3 muone 9
run 53838 1.000000 em2emRREE3 muone 9
run 59140 1.000000 em2emRREE3 muone 9
