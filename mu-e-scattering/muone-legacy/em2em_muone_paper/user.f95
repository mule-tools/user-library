


                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 12
  integer, parameter :: nr_bins = 300
  integer :: bandcut
  real, parameter :: &
     min_val(nr_q) = (/ 0.           ,0.               ,0.             ,0.          ,-150000.  , &
                        -150000.     ,0.               ,0.             ,0.          ,0.        , &
                        0.9003333333 ,-0.00996666666 /)
  real, parameter :: &
     max_val(nr_q) = (/ 30.E-3       ,6.E-3            ,150000.        ,150000.     ,0.        , &
                        0.           ,250.**2          ,200.           ,200.        ,200.      , &
                        1.1003333333 , 0.01003333333 /)

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  logical ::  pass_cut(nr_q)
  character (len = 6), dimension(nr_q) :: names
  character(len=10) :: filenamesuffix


!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains




  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

  mu = Me

  musq = mu**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  read*, bandcut
  write(filenamesuffix,'(I1)') bandcut

  if(bandcut==0) then
    print*, "filenamesuffix=0 -> no band cut"
  elseif(bandcut==1) then
    print*, "filenamesuffix=1 -> with band cut"
  else
    call crash("inituser")
  endif
  END SUBROUTINE




  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: q1lab(4), q2lab(4), q3lab(4), q4lab(4)
  real (kind=prec) :: theta_e, theta_m, phi_e, phi_m, tee, tmm, Ee, Em, mgg2, pTe, pTm, pTem
  real (kind=prec) :: gmu, gam, theta_m_el, bdev, phi_aco
  real (kind=prec) :: quant(nr_q)

  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  pass_cut = .true.
  call fix_mu
 
  q1lab = boost_rf(q1,q1)
  q2lab = boost_rf(q1,q2)
  q3lab = boost_rf(q1,q3)
  q4lab = boost_rf(q1,q4)
  
  theta_e = acos(cos_th(q2lab,q3lab))
  theta_m = acos(cos_th(q2lab,q4lab))
  phi_e = phi(q3lab)
  phi_m = phi(q4lab)
  tee = sq(q1-q3)
  tmm = sq(q2-q4)
  Ee = q3lab(4)
  Em = q4lab(4)
  mgg2 = sq(q3+q5+q6)
  pTe = PT(q3lab)
  pTm = PT(q4lab)
  pTem = PT(q3lab+q4lab)
  phi_aco = pi-abs(phi_e-phi_m)

  !elasticity curve
  gmu = (q2lab(4)*me+mm**2)/(q2lab(4)*me+me**2)
  gam = (q2lab(4)+me)/sqrt(scms)
  theta_m_el = atan(2*tan(theta_e)/((1+gam**2*tan(theta_e)**2)*(1+gmu)-2))
  bdev = theta_m/theta_m_el

  !energy&angular cut
  if(Ee<1000.) pass_cut = .false.
  if(theta_m<0.3E-3) pass_cut = .false.
  !band cut
  if(bandcut==1) then
    if(bdev<0.9) pass_cut = .false.
    if(bdev>1.1) pass_cut = .false.
  endif


  names(1) = "thetae"
  names(2) = "thetam"
  names(3) = "Ee"
  names(4) = "Em"
  names(5) = "tee"
  names(6) = "tmm"
  names(7) = "mgg2"
  names(8) = "pTe"
  names(9) = "pTm"
  names(10) = "pTem"
  names(11) = "bdev"
  names(12) = "phiaco"

  quant(1) = theta_e
  quant(2) = theta_m
  quant(3) = Ee
  quant(4) = Em
  quant(5) = tee
  quant(6) = tmm
  quant(7) = mgg2
  quant(8) = pTe
  quant(9) = pTm
  quant(10) = pTem
  quant(11) = bdev
  quant(12) = phi_aco
    
  END FUNCTION QUANT






                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!



