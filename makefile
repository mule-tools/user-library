FOLDERS=mu-e-scattering/muone \
        mu-e-scattering/muone-legacy \
        mu-e-scattering/muone-full-legacy \
        dilepton/belle \
        bhabha-scattering/validation \
        moller-scattering/prad \
        l-p-scattering/muse \
        l-p-scattering/muse-legacy l-p-scattering/mesa-legacy \
        l-p-scattering/muse-tpe \
        radiative-lepton-decay/babar/example \
        michel-decay/neutrino-spectrum \
        michel-decay/validation michel-decay/demo-observable michel-decay/f-and-g

.DEFAULT_GOAL=all
.PHONY:check clean ci
all:
	for i in $(FOLDERS) ; do make -C $$i all ; done
check:
	for i in $(FOLDERS) ; do make -C $$i check ; done
clean:
	for i in $(FOLDERS) ; do make -C $$i clean ; done
ci:
	rm -rf ci
	for i in $(FOLDERS) ; do \
	    make -C $$i ci.tar && \
	    mkdir -p ci/$$i && \
	    tar xf $$i/ci.tar -C ci/$$i ; \
	done
zenodo:
	rm -rf zenodo
	mkdir zenodo
	for i in $(FOLDERS) ; do make -C $$i zenodo ; done


public/index.html: ci
	rm -rf public
	cp -R ci public
	python3 tools/mkindex.py
