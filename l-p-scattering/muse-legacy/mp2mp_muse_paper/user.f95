


                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 6
  integer, parameter :: nr_bins = 120
  real, parameter :: &
     min_val(nr_q) = (/ 20., 10., 10., 10., 10., 10. /)
  real, parameter :: &
     max_val(nr_q) = (/ 100., 130.,130., 130., 130., 130. /)

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  logical ::  pass_cut(nr_q)
  character (len = 6), dimension(nr_q) :: names
  character(len=10) :: filenamesuffix


!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains




  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

  mu = Mmu

  musq = mu**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  print*, "MUSE with muons, momentum 210 MeV"
  END SUBROUTINE




  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: q1Rest(4),q2Rest(4),q3Rest(4),q4Rest(4)
  real (kind=prec) :: quant(nr_q)
  real (kind=prec) :: thetal, cthetal, qsq
  real (kind=prec) :: thB1l,thB1u,thB2l,thB2u,thB3l,thB3u,thB4l,thB4u


  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  pass_cut = .true.
  call fix_mu

  q1Rest = boost_rf(q2,q1)  ! incomping lepton
  q2Rest = boost_rf(q2,q2)  ! proton at rest
  q3Rest = boost_rf(q2,q3)  ! outgoing lepton
  q4Rest = boost_rf(q2,q4)  ! recoling proton

  cthetal = cos_th(q1Rest,q3Rest)
  thetal = acos(cos_th(q1Rest,q3Rest))
  qsq = -sq(q2-q4)

  thB1l = 0.3875697  ! about 22 degrees -> 126 MeV
  thB1u = 0.7708983  ! about 44 degrees -> 117 MeV

  thB2l = 0.8054385  ! about 46 degrees -> 116 MeV
  thB2u = 1.0939359  ! about 63 degrees -> 107 MeV

  thB3l = 1.124739   ! about 64 degrees -> 106 MeV
  thB3u = 1.403278   ! about 80 degrees ->  97 MeV

  thB4l = 1.435036   ! about 82 degrees ->  96 MeV
  thB4u = 1.739436   ! about 99 degrees ->  87 MeV

  if (thetal.lt.  pi/9.) pass_cut=.false.  ! theta < 20 degrees
  if (thetal.gt.5*pi/9.) pass_cut=.false.  ! theta > 100 deg

  if(thetal.lt.thB1l) pass_cut(3)=.false.
  if(thetal.gt.thB1u) pass_cut(3)=.false.

  if(thetal.lt.thB2l) pass_cut(4)=.false.
  if(thetal.gt.thB2u) pass_cut(4)=.false.

  if(thetal.lt.thB3l) pass_cut(5)=.false.
  if(thetal.gt.thB3u) pass_cut(5)=.false.

  if(thetal.lt.thB4l) pass_cut(6)=.false.
  if(thetal.gt.thB4u) pass_cut(6)=.false.


  names(1) = "thetal"
  quant(1) = 180*thetal/pi
  names(2) = "Emu"
  quant(2) = q3rest(4) - Mmu
  names(3) = "EmuB1"
  quant(3) = q3rest(4) - Mmu
  names(4) = "EmuB2"
  quant(4) = q3rest(4) - Mmu
  names(5) = "EmuB3"
  quant(5) = q3rest(4) - Mmu
  names(6) = "EmuB4"
  quant(6) = q3rest(4) - Mmu

  END FUNCTION QUANT






                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!



