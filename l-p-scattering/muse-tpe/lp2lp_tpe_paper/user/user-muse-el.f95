                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions
  use user_dummy, only: pass_cut, userweight, names, filenamesuffix
  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 13
  integer, parameter :: nr_bins = 260
  integer :: hardycut
  real, parameter :: &
     min_val(nr_q) = (/ 20., 0., 0., 0., 0., &
                             0., 0., 0., 0., &
                             0., 0., 0., 0. /)
  real, parameter :: &
     max_val(nr_q) = (/ 100., 260., 260., 260., 260.,     &
                              130e3, 130e3, 130e3, 130e3, &
                              130e3, 130e3, 130e3, 130e3 /)

  integer :: userdim = 0

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer, parameter :: namesLen=6
  integer, parameter :: filenamesuffixLen=10



!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains




  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

  musq = me**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  print*, "MUSE with Electrons, momentum 210 MeV"

  read*, hardycut
  write(filenamesuffix,'(I1)') hardycut

  if(hardycut==0) then
    print*, "filenamesuffix=0 -> no h photon cut"
  elseif(hardycut==1) then
    print*, "filenamesuffix=1 -> with h photon cut"
  else
    call crash("inituser")
  endif

  END SUBROUTINE




  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: q1Rest(4),q2Rest(4),q3Rest(4),q4Rest(4),q5Rest(4),q6Rest(4)
  real (kind=prec) :: quant(nr_q)
  real (kind=prec) :: thetal, cthetal, qsql, qsqp, thetay1, cthetay1, thetay2, cthetay2
  real (kind=prec) :: pel, ecal, pmin, eytot
  real (kind=prec) :: thEB1l, thEB1u, thEB2l, thEB2u, thEB3l, thEB3u
  real (kind=prec) :: thQB1l, thQB1u, thQB2l, thQB2u, thQB3l, thQB3u

  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  pass_cut = .true.
  call fix_mu

  q1Rest = boost_rf(q2,q1)  ! incomping lepton
  q2Rest = boost_rf(q2,q2)  ! proton at rest
  q3Rest = boost_rf(q2,q3)  ! outgoing lepton
  q4Rest = boost_rf(q2,q4)  ! recoiling proton
  q5Rest = boost_rf(q2,q5)  ! outgoing photon (if present)
  q6Rest = boost_rf(q2,q6)  ! outgoing photon (if present)

  cthetal = cos_th(q1Rest,q3Rest)
  thetal = acos(cos_th(q1Rest,q3Rest))
  cthetay1 = cos_th(q1Rest,q5Rest)
  thetay1 = acos(cthetay1)
  cthetay2 = cos_th(q1Rest,q6Rest)
  thetay2 = acos(cthetay2)
  qsql = -sq(q1-q3)
  qsqp = -sq(q2-q4)

  pmin = 15._prec                          ! minimum el momentum (MeV)
  if (sqrt(q3Rest(1)**2+q3Rest(2)**2+q3Rest(3)**2) < pmin) pass_cut = .false.
  if (thetal.lt.  pi/9.) pass_cut=.false.  ! theta < 20 degrees
  if (thetal.gt.5*pi/9.) pass_cut=.false.  ! theta > 100 deg

  !hard cut on electron and photons
  pel  = 210._prec       ! MeV
  ecal = 0.1_prec        ! Ecal within 100 mrad

  eytot = 0._prec
  if(hardycut==1) then
    if (acos(cos_th(q1Rest,q5Rest)) < ecal) eytot = eytot + q5Rest(4)
    if (acos(cos_th(q1Rest,q6Rest)) < ecal) eytot = eytot + q6Rest(4)
    if (eytot > 0.4_prec * pel) pass_cut = .false.
  endif

  thEB1l = 0.3910741566  ! about 22.4 degrees -> 206 MeV [23]
  thEB1u = 0.4947918753  ! about 28.3 degrees -> 204 MeV [27]
  thEB2l = 0.9968887781  ! about 57.1 degrees -> 190 MeV [58]
  thEB2u = 1.088154002   ! about 62.3 degrees -> 187 MeV [62]
  thEB3l = 1.605568909   ! about 92.0 degrees -> 170 MeV [93]
  thEB3u = 1.704510365   ! about 97.7 degrees -> 167 MeV [97]

  if(thetal.lt.thEB1l) pass_cut(3)=.false.
  if(thetal.gt.thEB1u) pass_cut(3)=.false.
  if(thetal.lt.thEB2l) pass_cut(4)=.false.
  if(thetal.gt.thEB2u) pass_cut(4)=.false.
  if(thetal.lt.thEB3l) pass_cut(5)=.false.
  if(thetal.gt.thEB3u) pass_cut(5)=.false.

  thQB1l = 0.3738697123  ! about 21.4 degrees ->  6000 MeV^2 [23]
  thQB1u = 0.4871526663  ! about 27.9 degrees -> 10000 MeV^2 [27]
  thQB2l = 1.003883868   ! about 57.5 degrees -> 37000 MeV^2 [58]
  thQB2u = 1.084897704   ! about 62.2 degrees -> 42000 MeV^2 [62]
  thQB3l = 1.621035022   ! about 92.9 degrees -> 75000 MeV^2 [93]
  thQB3u = 1.709304798   ! about 97.9 degrees -> 80000 MeV^2 [97]

  if(thetal.lt.thQB1l) pass_cut(7)=.false.
  if(thetal.gt.thQB1u) pass_cut(7)=.false.
  if(thetal.lt.thQB2l) pass_cut(8)=.false.
  if(thetal.gt.thQB2u) pass_cut(8)=.false.
  if(thetal.lt.thQB3l) pass_cut(9)=.false.
  if(thetal.gt.thQB3u) pass_cut(9)=.false.

  if(thetal.lt.thQB1l) pass_cut(11)=.false.
  if(thetal.gt.thQB1u) pass_cut(11)=.false.
  if(thetal.lt.thQB2l) pass_cut(12)=.false.
  if(thetal.gt.thQB2u) pass_cut(12)=.false.
  if(thetal.lt.thQB3l) pass_cut(13)=.false.
  if(thetal.gt.thQB3u) pass_cut(13)=.false.

  names(1) = "thetal"
  quant(1) = 180*thetal/pi
  names(2) = "El"
  quant(2) = q3rest(4) - Mel
  names(3) = "ElB1"
  quant(3) = q3rest(4) - Mel
  names(4) = "ElB2"
  quant(4) = q3rest(4) - Mel
  names(5) = "ElB3"
  quant(5) = q3rest(4) - Mel
  names(6) = "qsql"
  quant(6) = qsql
  names(7) = "qsqlB1"
  quant(7) = qsql
  names(8) = "qsqlB2"
  quant(8) = qsql
  names(9) = "qsqlB3"
  quant(9) = qsql
  names(10) = "qsqp"
  quant(10) = qsqp
  names(11) = "qsqpB1"
  quant(11) = qsqp
  names(12) = "qsqpB2"
  quant(12) = qsqp
  names(13) = "qsqpB3"
  quant(13) = qsqp

  END FUNCTION QUANT


  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE USEREVENT




                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
