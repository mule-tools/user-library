# vim: foldmethod=marker
# py2nb: title=Virtual TPE
## Virtual TPE ($\delta_{2\gamma})${{{
# We can plot, for both $ep$ and $\mu p$ scattering, the virtual
#contribution to the TPE, i.e.
# \begin{equation}
#   \delta_{2\gamma}(\text{IR}) =
#   \frac{\mathcal{M}^{(1)}_{n}(q^3_\ell,F^3)\big|_\text{IR}}
#   {\mathcal{M}^{(0)}_n(q^2_\ell,F^2)}
# \end{equation}
# using the Maximon-Tjon
# scheme (${\tt MTj}$) to subtract the IR singularities.
#
# The results are presented for three different values of beam
# momentum, according to the MUSE experiment.
##########################################################################}}}

from pymule import *

dat115e = np.loadtxt('out-d2g/out115e.txt')
dat153e = np.loadtxt('out-d2g/out153e.txt')
dat210e = np.loadtxt('out-d2g/out210e.txt')

dat115m = np.loadtxt('out-d2g/out115m.txt')
dat153m = np.loadtxt('out-d2g/out153m.txt')
dat210m = np.loadtxt('out-d2g/out210m.txt')


fig, axs = subplots(
    3, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios':[1,1,1]},
    figsize=(5.9, 7.84)
    )
axs = list(axs)
sca(axs[0])
for i in range(1, len(dat115e[0])):
    plot(dat115e[:-1,0], -dat115e[:-1,i])
ylabel("$\delta_{2\gamma}\\,({\\tt MTj})$")
title("$e^-p$")
legend([
    "$\Lambda^2 = 0.60\,\,{\\rm GeV}^2$",
    "$\Lambda^2 = 0.66\,\,{\\rm GeV}^2$",
    "$\Lambda^2 = 0.71\,\,{\\rm GeV}^2$",
    "$\Lambda^2 = 0.86\,\,{\\rm GeV}^2$",
], ncol=1, loc='upper right', framealpha=0)
sca(axs[1])
for i in range(1, len(dat153e[0])):
    plot(dat153e[:-1,0], -dat153e[:-1,i])
ylabel("$\delta_{2\gamma}\\,({\\tt MTj})$")
sca(axs[2])
for i in range(1, len(dat210e[0])):
    plot(dat210e[:-1,0], -dat210e[:-1,i])
ylabel("$\delta_{2\gamma}\\,({\\tt MTj})$")

xlabel("$Q^2\,\, /\,\,{\\rm GeV}^2$")
figtext(.75,.7,"$\\boxed{p_{\\rm beam} = 115\,{\\rm MeV}}$")
figtext(.75,.4,"$\\boxed{p_{\\rm beam} = 153\,{\\rm MeV}}$")
figtext(.75,.1,"$\\boxed{p_{\\rm beam} = 210\,{\\rm MeV}}$")
draw()
fig.tight_layout()
mulify(fig, delx=0., dely=4.4)
fig.savefig("plots/d2g-e.pdf")


fig, axs = subplots(
    3, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios':[1,1,1]},
    figsize=(5.9, 7.84)
    )
axs = list(axs)
sca(axs[0])
for i in range(1, len(dat115m[0])):
    plot(dat115m[:-1,0], -dat115m[:-1,i])
ylabel("$\delta_{2\gamma}\\,({\\tt MTj})$")
title("$\\mu^-p$")
legend([
    "$\Lambda^2 = 0.60\,\,{\\rm GeV}^2$",
    "$\Lambda^2 = 0.66\,\,{\\rm GeV}^2$",
    "$\Lambda^2 = 0.71\,\,{\\rm GeV}^2$",
    "$\Lambda^2 = 0.86\,\,{\\rm GeV}^2$",
], ncol=1, loc='upper right', framealpha=0)
sca(axs[1])
for i in range(1, len(dat153m[0])):
    plot(dat153m[:-1,0], -dat153m[:-1,i])
ylabel("$\delta_{2\gamma}\\,({\\tt MTj})$")
sca(axs[2])
for i in range(1, len(dat210m[0])):
    plot(dat210m[:-1,0], -dat210m[:-1,i])
ylabel("$\delta_{2\gamma}\\,({\\tt MTj})$")

xlabel("$Q^2\,\, /\,\,{\\rm GeV}^2$")
figtext(.75,.7,"$\\boxed{p_{\\rm beam} = 115\,{\\rm MeV}}$")
figtext(.75,.4,"$\\boxed{p_{\\rm beam} = 153\,{\\rm MeV}}$")
figtext(.75,.1,"$\\boxed{p_{\\rm beam} = 210\,{\\rm MeV}}$")
draw()
fig.tight_layout()
mulify(fig, delx=0., dely=4.4)
fig.savefig("plots/d2g-m.pdf")
