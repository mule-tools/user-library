# vim: foldmethod=marker
# py2nb: title=Full MUonE legacy
## Impact of NNLO QED on $\ell-p$ scattering at MUSE{{{
# The results for lepton-proton scattering are tailored to the characteristics
# of the MUSE experiment. The kinematics of the process is defined by
#
# \begin{equation}
#   \ell^\pm\,(p_1, m) + p\,(p_2, M) \to \ell^\pm\,(p_3,
#   \theta_\ell) + p\,(p_4)
# \end{equation}
#
# The lepton beam momentum is set to $|\vec{p}_1|=210$ MeV, consistent
# with one of the MUSE setups. Leptons can be electrons or muons, with
# both polarities.
#
# Cuts are imposed on the outgoing lepton scattering angle, $20^\circ
# < \theta_\ell < 100^\circ$, and on the outgoing lepton momentum,
# $|\vec{p}_3| > 15$ MeV.
#
# A cut on energetic forward photons can be also enforced,
# \begin{equation}
#   E_\gamma > 0.4\,|\vec{p}_1| \,,
# \end{equation}
# where $E_\gamma$ is the sum of the energies carried by photons
# contained in a 100 mrad cone centred on the beam axis. This
# simulates the forward-angle calorimeter employed by MUSE to remove
# radiative events.
#
# Based on the constraints above, we can define two scenarios:
#
# - S0  : $20^\circ < \theta_\ell < 100^\circ$, $|\vec{p}_3| > 15$ MeV
# - S1  : $20^\circ < \theta_\ell < 100^\circ$, $|\vec{p}_3| > 15$ MeV,
#         $E_\gamma > 0.4\,|\vec{p}_1|$
#
# NLO and NNLO corrections can be split into gauge-invariant subsets
# according to their nominal leptonic and protonic charge.  If $q$
# resp. $Q$ is assigned to each photon emission from a lepton
# resp. proton line, we will have, on top of the LO charge $q^2\,Q^2$,
# three subsets at NLO, i.e. $\{q^2,\, qQ,\, Q^2\}$ and five subsets
# at NNLO, i.e. $\{q^4,\,q^3 Q,\, q^2 Q^2,\,q Q^3,\,Q^4\}$.
#
# The corrections can be generically split into *photonic* and
# *fermionic*, where the latter include all the contributions with VP
# insertions. Thus we have:
#
# - NLO photonic (@PH) contributions, where @ can be (E, X, M)
# - NLO fermionic (@VP) contrbution, where @ is conventionally "E"
# - NNLO photonic (@PH) contributions, where @ can be (E, X31, X22, X13, M)
# - NNLO fermionic (@VP) contributions, where @ can be (E, X, M)
#
# In addition, the last two characters in the names of the different
# contributions, i.e. *m* or *p*, and 0 or 1, refer to the
# polarisation of the leptons (*minus* or *plus*) and to the scenarios
# where the elasticity cut is enforced, 1, or not, 0.
#
# Finally, some contributions are presented with a form-factor
# description of the photon-proton vertex, see (9) in the paper,
# depending on one parameter, $\Lambda$. This can assume four
# different values, as shown below. In this case, the label "E" and
# "X" turn into "F" and "Y", and at the end of the names Lxx denotes
# the value 0.xx for $\Lambda^2$, in GeV$^2$. "Y" can also denote the
# sum of the three mixed photonic NNLO corrections.
#
from pymule import *
from pymule.plot import twopanel
import pymule.mpl_axes_aligner as mpl_axes_aligner
from tabulate import tabulate, SEPARATING_LINE

# Before starting, some helper functions for later
def pn(a):
    return printnumber(a)
def dn(a,b):
    return dividenumbers(a,b)
def tn(a,b):
    return timesnumbers(a,b)

##########################################################################}}}
### Load McMule data{{{
setup(cachefolder='/tmp/mcmule/')
#### $e^\pm p \to e^\pm p${{{
# $\bullet$ pure QED
setup(obs='0')
setup(folder='lp2lp_tpe_paper/out-muse-e-qed.tar.bz2')
lo0         = scaleset(mergefks(sigma('em2em0')), alpha**2*conv)
nloEPHm0    = scaleset(mergefks(sigma('em2emFEE'),sigma('em2emREE15'),sigma('em2emREE35')), alpha**3*conv)
nloEVPm0    = scaleset(mergefks(sigma('em2emA')), alpha**3*conv)
nloXPHm0    = scaleset(mergefks(sigma('em2emFEM'),sigma('em2emREM')), alpha**3*conv)
nloMPHm0    = scaleset(mergefks(sigma('em2emFMM'),sigma('em2emRMM')), alpha**3*conv)
nnloEPHm0   = scaleset(mergefks(sigma('em2emFFEEEE',obs='1'),sigma('em2emRFEEEE15'),sigma('em2emRFEEEE35'),sigma('em2emRREEEE1516'),sigma('em2emRREEEE3536')), alpha**4*conv)
nnloX31PHm0 = scaleset(mergefks(sigma('em2emFF31z',obs='1'),sigma('em2emRF3115'),sigma('em2emRF3135'),sigma('em2emRR311516'),sigma('em2emRR313536')), alpha**4*conv)
nnloX22PHm0 = scaleset(mergefks(sigma('em2emFF22z',obs='1'),sigma('em2emRF2215'),sigma('em2emRF2235'),sigma('em2emRR221516'),sigma('em2emRR223536')), alpha**4*conv)
nnloX13PHm0 = scaleset(mergefks(sigma('em2emFF13z',obs='1'),sigma('em2emRF1315'),sigma('em2emRF1335'),sigma('em2emRR131516'),sigma('em2emRR133536')), alpha**4*conv)
nnloMPHm0   = scaleset(mergefks(sigma('em2emFFMMMM',obs='1'),sigma('em2emRFMMMM'),sigma('em2emRRMMMM')), alpha**4*conv)
nnloEVPm0   = scaleset(mergefks(sigma('em2emAFEE'), sigma('em2emAREE15'), sigma('em2emAREE35'),
                                anyxi1=sigma('em2emAA'), anyxi2=sigma('em2emNFEE')), alpha**4*conv)
nnloXVPm0   = scaleset(mergefks(sigma('em2emAFEM'), sigma('em2emAREM'),anyxi1=sigma('em2emNFEM',obs='0')), alpha**4*conv)
nnloMVPm0   = scaleset(mergefks(sigma('em2emAFMM'), sigma('em2emARMM'),anyxi1=sigma('em2emNFMM',obs='0')), alpha**4*conv)

setup(obs='1')
lo1         = scaleset(mergefks(sigma('em2em0',obs='0')), alpha**2*conv)
nloEPHm1    = scaleset(mergefks(sigma('em2emFEE',obs='0'),sigma('em2emREE15'),sigma('em2emREE35')), alpha**3*conv)
nloEVPm1    = scaleset(mergefks(sigma('em2emA',obs='0')), alpha**3*conv)
nloXPHm1    = scaleset(mergefks(sigma('em2emFEM',obs='0'),sigma('em2emREM')), alpha**3*conv)
nloMPHm1    = scaleset(mergefks(sigma('em2emFMM',obs='0'),sigma('em2emRMM')), alpha**3*conv)
nnloEPHm1   = scaleset(mergefks(sigma('em2emFFEEEE'),sigma('em2emRFEEEE15'),sigma('em2emRFEEEE35'),sigma('em2emRREEEE1516'),sigma('em2emRREEEE3536')), alpha**4*conv)
nnloX31PHm1 = scaleset(mergefks(sigma('em2emFF31z'),sigma('em2emRF3115'),sigma('em2emRF3135'),sigma('em2emRR311516'),sigma('em2emRR313536')), alpha**4*conv)
nnloX22PHm1 = scaleset(mergefks(sigma('em2emFF22z'),sigma('em2emRF2215'),sigma('em2emRF2235'),sigma('em2emRR221516'),sigma('em2emRR223536')), alpha**4*conv)
nnloX13PHm1 = scaleset(mergefks(sigma('em2emFF13z'),sigma('em2emRF1315'),sigma('em2emRF1335'),sigma('em2emRR131516'),sigma('em2emRR133536')), alpha**4*conv)
nnloMPHm1   = scaleset(mergefks(sigma('em2emFFMMMM'),sigma('em2emRFMMMM'),sigma('em2emRRMMMM')), alpha**4*conv)
nnloEVPm1   = scaleset(mergefks(sigma('em2emAFEE',obs='0'), sigma('em2emAREE15'), sigma('em2emAREE35'),
                                anyxi1=sigma('em2emAA',obs='0'), anyxi2=sigma('em2emNFEE',obs='0')), alpha**4*conv)
nnloXVPm1   = scaleset(mergefks(sigma('em2emAFEM',obs='0'), sigma('em2emAREM'),anyxi1=sigma('em2emNFEM',obs='0')), alpha**4*conv)
nnloMVPm1   = scaleset(mergefks(sigma('em2emAFMM',obs='0'), sigma('em2emARMM'),anyxi1=sigma('em2emNFMM',obs='0')), alpha**4*conv)

# $\bullet$ $\Lambda^2=0.86$ GeV^2
setup(folder='lp2lp_tpe_paper/out-muse-e-86.tar.bz2')
setup(obs='0')
lo086          = scaleset(mergefks(sigma('mp2mp0')), alpha**2*conv)
nloFPHp0L86    = scaleset(mergefks(sigma('mp2mpF'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
nloFVPp0L86    = scaleset(mergefks(sigma('mp2mpA')), alpha**3*conv)
nloYPHp0L86    = scaleset(mergefks(sigma('mp2mpFMP'),sigma('mp2mpRMP')), alpha**3*conv)

setup(obs='1')
nloFVPp1L86    = scaleset(mergefks(sigma('mp2mpA',obs='0')), alpha**3*conv)
nloFPHp1L86    = scaleset(mergefks(sigma('mp2mpF',obs='0'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
nloYPHp1L86    = scaleset(mergefks(sigma('mp2mpFMP',obs='0'),sigma('mp2mpRMP')), alpha**3*conv)

# $\bullet$ $\Lambda^2=0.71$ GeV^2
setup(folder='lp2lp_tpe_paper/out-muse-e-71.tar.bz2')
setup(obs='0')
lo071          = scaleset(mergefks(sigma('mp2mp0')), alpha**2*conv)
nloFPHp0L71    = scaleset(mergefks(sigma('mp2mpF'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
nloFVPp0L71    = scaleset(mergefks(sigma('mp2mpA')), alpha**3*conv)
nloYPHp0L71    = scaleset(mergefks(sigma('mp2mpFMP'),sigma('mp2mpRMP')), alpha**3*conv)
nnloFPHp0L71   = scaleset(mergefks(sigma('mp2mpFF'),sigma('mp2mpRF15'),sigma('mp2mpRF35'),sigma('mp2mpRR1516'),sigma('mp2mpRR3536')), alpha**4*conv)
nnloFVPp0L71   = scaleset(mergefks(sigma('mp2mpAF'), sigma('mp2mpAR15'), sigma('mp2mpAR35'),
                                   anyxi1=sigma('mp2mpAA'), anyxi2=sigma('mp2mpNF')), alpha**4*conv)

setup(obs='1')
nloFPHp1L71    = scaleset(mergefks(sigma('mp2mpF',obs='0'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
nloFVPp1L71    = scaleset(mergefks(sigma('mp2mpA',obs='0')), alpha**3*conv)
nloYPHp1L71    = scaleset(mergefks(sigma('mp2mpFMP',obs='0'),sigma('mp2mpRMP')), alpha**3*conv)
nnloFPHp1L71   = scaleset(mergefks(sigma('mp2mpFF',obs='0'),sigma('mp2mpRF15'),sigma('mp2mpRF35'),sigma('mp2mpRR1516'),sigma('mp2mpRR3536')), alpha**4*conv)
nnloFVPp1L71   = scaleset(mergefks(sigma('mp2mpAF',obs='0'), sigma('mp2mpAR15'), sigma('mp2mpAR35'),
                                   anyxi1=sigma('mp2mpAA',obs='0'), anyxi2=sigma('mp2mpNF',obs='0')), alpha**4*conv)

# $\bullet$ $\Lambda^2=0.66$ GeV^2
setup(folder='lp2lp_tpe_paper/out-muse-e-66.tar.bz2')
setup(obs='0')
lo066          = scaleset(mergefks(sigma('mp2mp0')), alpha**2*conv)
nloFPHp0L66    = scaleset(mergefks(sigma('mp2mpF'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
nloFVPp0L66    = scaleset(mergefks(sigma('mp2mpA')), alpha**3*conv)
nloYPHp0L66    = scaleset(mergefks(sigma('mp2mpFMP'),sigma('mp2mpRMP')), alpha**3*conv)

setup(obs='1')
nloFVPp1L66    = scaleset(mergefks(sigma('mp2mpA',obs='0')), alpha**3*conv)
nloFPHp1L66    = scaleset(mergefks(sigma('mp2mpF',obs='0'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
nloYPHp1L66    = scaleset(mergefks(sigma('mp2mpFMP',obs='0'),sigma('mp2mpRMP')), alpha**3*conv)

# $\bullet$ $\Lambda^2=0.60$ GeV^2
setup(folder='lp2lp_tpe_paper/out-muse-e-60.tar.bz2')
setup(obs='0')
lo060          = scaleset(mergefks(sigma('mp2mp0')), alpha**2*conv)
nloFPHp0L60    = scaleset(mergefks(sigma('mp2mpF'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
nloFVPp0L60    = scaleset(mergefks(sigma('mp2mpA')), alpha**3*conv)
nloYPHp0L60    = scaleset(mergefks(sigma('mp2mpFMP'),sigma('mp2mpRMP')), alpha**3*conv)

setup(obs='1')
nloFVPp1L60    = scaleset(mergefks(sigma('mp2mpA',obs='0')), alpha**3*conv)
nloFPHp1L60    = scaleset(mergefks(sigma('mp2mpF',obs='0'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
nloYPHp1L60    = scaleset(mergefks(sigma('mp2mpFMP',obs='0'),sigma('mp2mpRMP')), alpha**3*conv)

########################################################################}}}
#########################################################################}}}
### Total cross sections ($\mu$b){{{
# $\bullet$ S0
lo00 = lo0['value']
lo86 = lo086['value']
lo71 = lo071['value']
lo66 = lo066['value']
lo60 = lo060['value']

nloXPHp0    = tn(nloXPHm0['value'], [-1., 0])
nloYPHm0L86 = tn(nloYPHp0L86['value'], [-1., 0])
nloYPHm0L71 = tn(nloYPHp0L71['value'], [-1., 0])
nloYPHm0L66 = tn(nloYPHp0L66['value'], [-1., 0])
nloYPHm0L60 = tn(nloYPHp0L60['value'], [-1., 0])

nlom0     = plusnumbers(nloEPHm0['value'], nloEVPm0['value'],  nloXPHm0['value'],  nloMPHm0['value'],  lo00)
nlop0     = plusnumbers(nloEPHm0['value'], nloEVPm0['value'], -nloXPHm0['value'],  nloMPHm0['value'],  lo00)
nlom071   = plusnumbers(nloFPHp0L71['value'], nloFVPp0L71['value'], -nloYPHp0L71['value'],  nloMPHm0['value'],  lo71)
nlop071   = plusnumbers(nloFPHp0L71['value'], nloFVPp0L71['value'],  nloYPHp0L71['value'],  nloMPHm0['value'],  lo71)

nnloYPHm0 = plusnumbers( nnloX31PHm0['value'], nnloX22PHm0['value'],  nnloX13PHm0['value'])
nnloYPHp0 = plusnumbers(-nnloX31PHm0['value'], nnloX22PHm0['value'], -nnloX13PHm0['value'])
nnloXVPp0 = tn(nnloXVPm0['value'], [-1., 0])
nnlom0    = plusnumbers(nnloEPHm0['value'], nnloYPHm0, nnloMPHm0['value'], nlom0)
nnlop0    = plusnumbers(nnloEPHm0['value'], nnloYPHp0, nnloMPHm0['value'], nlop0)

print("")
tabE0 = [
["\sigma_0",          pn(lo0['value']),          pn(lo0['value'])],
["\sigma_0_86",       pn(lo086['value']),        pn(lo086['value'])],
["\sigma_0_71",       pn(lo071['value']),        pn(lo071['value'])],
["\sigma_0_66",       pn(lo066['value']),        pn(lo066['value'])],
["\sigma_0_60",       pn(lo060['value']),        pn(lo060['value'])],
SEPARATING_LINE,
["\sigma^{(1)}_e",    pn(nloEPHm0['value']),     pn(nloEPHm0['value']),     pn(dn(nloEPHm0['value'],lo00)*100.),           pn(dn(nloEPHm0['value'],lo00)*100.)],
["\sigma^{(1)}_eF",   pn(nloEVPm0['value']),     pn(nloEVPm0['value']),     pn(dn(nloEVPm0['value'],lo00)*100.),           pn(dn(nloEVPm0['value'],lo00)*100.)],
["\sigma^{(1)}_e86",  pn(nloFPHp0L86['value']),  pn(nloFPHp0L86['value']),  pn(dn(nloFPHp0L86['value'],lo86)*100.),        pn(dn(nloFPHp0L86['value'],lo86)*100.)],
["\sigma^{(1)}_eF86", pn(nloFVPp0L86['value']),  pn(nloFVPp0L86['value']),  pn(dn(nloFVPp0L86['value'],lo86)*100.),        pn(dn(nloFVPp0L86['value'],lo86)*100.)],
["\sigma^{(1)}_e71",  pn(nloFPHp0L71['value']),  pn(nloFPHp0L71['value']),  pn(dn(nloFPHp0L71['value'],lo71)*100.),        pn(dn(nloFPHp0L71['value'],lo71)*100.)],
["\sigma^{(1)}_eF71", pn(nloFVPp0L71['value']),  pn(nloFVPp0L71['value']),  pn(dn(nloFVPp0L71['value'],lo71)*100.),        pn(dn(nloFVPp0L71['value'],lo71)*100.)],
["\sigma^{(1)}_e66",  pn(nloFPHp0L66['value']),  pn(nloFPHp0L66['value']),  pn(dn(nloFPHp0L66['value'],lo66)*100.),        pn(dn(nloFPHp0L66['value'],lo66)*100.)],
["\sigma^{(1)}_eF66", pn(nloFVPp0L66['value']),  pn(nloFVPp0L66['value']),  pn(dn(nloFVPp0L66['value'],lo66)*100.),        pn(dn(nloFVPp0L66['value'],lo66)*100.)],
["\sigma^{(1)}_e60",  pn(nloFPHp0L60['value']),  pn(nloFPHp0L60['value']),  pn(dn(nloFPHp0L60['value'],lo60)*100.),        pn(dn(nloFPHp0L60['value'],lo60)*100.)],
["\sigma^{(1)}_eF60", pn(nloFVPp0L60['value']),  pn(nloFVPp0L60['value']),  pn(dn(nloFVPp0L60['value'],lo60)*100.),        pn(dn(nloFVPp0L60['value'],lo60)*100.)],
["\sigma^{(1)}_x",    pn(nloXPHm0['value']),     pn(nloXPHp0),              pn(dn(nloXPHm0['value'],lo00)*100.),           pn(dn(nloXPHp0,lo00)*100.)],
["\sigma^{(1)}_x86",  pn(nloYPHm0L86),           pn(nloYPHp0L86['value']),  pn(dn(nloYPHm0L86,lo86)*100.),                 pn(dn(nloYPHp0L86['value'],lo86)*100.)],
["\sigma^{(1)}_x71",  pn(nloYPHm0L71),           pn(nloYPHp0L71['value']),  pn(dn(nloYPHm0L71,lo71)*100.),                 pn(dn(nloYPHp0L71['value'],lo71)*100.)],
["\sigma^{(1)}_x66",  pn(nloYPHm0L66),           pn(nloYPHp0L66['value']),  pn(dn(nloYPHm0L66,lo66)*100.),                 pn(dn(nloYPHp0L66['value'],lo66)*100.)],
["\sigma^{(1)}_x60",  pn(nloYPHm0L60),           pn(nloYPHp0L60['value']),  pn(dn(nloYPHm0L60,lo60)*100.),                 pn(dn(nloYPHp0L60['value'],lo60)*100.)],
["\sigma^{(1)}_p",    pn(nloMPHm0['value']),     pn(nloMPHm0['value']),     pn(dn(nloMPHm0['value'],lo00)*100.),           pn(dn(nloMPHm0['value'],lo00)*100.)],
SEPARATING_LINE,
["\sigma^{(2)}_e",    pn(nnloEPHm0['value']),    pn(nnloEPHm0['value']),    pn(dn(nnloEPHm0['value'],nlom0)*100.),         pn(dn(nnloEPHm0['value'],nlop0)*100.)],
["\sigma^{(2)}_eF",   pn(nnloEVPm0['value']),    pn(nnloEVPm0['value']),    pn(dn(nnloEVPm0['value'],nlom0)*100.),         pn(dn(nnloEVPm0['value'],nlop0)*100.)],
["\sigma^{(2)}_e71",  pn(nnloFPHp0L71['value']), pn(nnloFPHp0L71['value']), pn(dn(nnloFPHp0L71['value'],nlom071)*100.),    pn(dn(nnloFPHp0L71['value'],nlop071)*100.)],
["\sigma^{(2)}_eF71", pn(nnloFVPp0L71['value']), pn(nnloFVPp0L71['value']), pn(dn(nnloFVPp0L71['value'],nlom071)*100.),    pn(dn(nnloFVPp0L71['value'],nlop071)*100.)],
["\sigma^{(2)}_x",    pn(nnloYPHm0),             pn(nnloYPHp0),             pn(dn(nnloYPHm0,nlom0)*100.),                  pn(dn(nnloYPHp0,nlop0)*100.)],
["\sigma^{(2)}_xF",   pn(nnloXVPm0['value']),    pn(nnloXVPp0),             pn(dn(nnloXVPm0['value'],nlom0)*100.),         pn(dn(nnloXVPp0,nlop0)*100.)],
["\sigma^{(2)}_p",    pn(nnloMPHm0['value']),    pn(nnloMPHm0['value']),    pn(dn(nnloMPHm0['value'],nlom0)*100.),         pn(dn(nnloMPHm0['value'],nlop0)*100.)],
["\sigma^{(2)}_pF",   pn(nnloMVPm0['value']),    pn(nnloMVPm0['value']),    pn(dn(nnloMVPm0['value'],nlom0)*100.),         pn(dn(nnloMVPm0['value'],nlop0)*100.)],
SEPARATING_LINE
]

print(tabulate(tabE0, headers=["MUSE_e 210 [S0]","e+p", "e-p", "dK+%", "dK-%"]))

# $\bullet$ S1
nloXPHp1    = tn(nloXPHm1['value'], [-1., 0])
nloYPHm1L86 = tn(nloYPHp1L86['value'], [-1., 0])
nloYPHm1L71 = tn(nloYPHp1L71['value'], [-1., 0])
nloYPHm1L60 = tn(nloYPHp1L60['value'], [-1., 0])
nloYPHm1L66 = tn(nloYPHp1L66['value'], [-1., 0])

nlom1     = plusnumbers(nloEPHm1['value'], nloEVPm1['value'],  nloXPHm1['value'],  nloMPHm1['value'],  lo00)
nlop1     = plusnumbers(nloEPHm1['value'], nloEVPm1['value'], -nloXPHm1['value'],  nloMPHm1['value'],  lo00)
nlom171   = plusnumbers(nloFPHp1L71['value'], nloFVPp1L71['value'], -nloYPHp1L71['value'],  nloMPHm1['value'],  lo71)
nlop171   = plusnumbers(nloFPHp1L71['value'], nloFVPp1L71['value'],  nloYPHp1L71['value'],  nloMPHm1['value'],  lo71)

nnloYPHm1 = plusnumbers( nnloX31PHm1['value'], nnloX22PHm1['value'],  nnloX13PHm1['value'])
nnloYPHp1 = plusnumbers(-nnloX31PHm1['value'], nnloX22PHm1['value'], -nnloX13PHm1['value'])
nnloXVPp1 = tn(nnloXVPm1['value'], [-1., 0])
nnlom1    = plusnumbers(nnloEPHm1['value'], nnloYPHm1, nnloMPHm1['value'], nlom1)
nnlop1    = plusnumbers(nnloEPHm1['value'], nnloYPHp1, nnloMPHm1['value'], nlop1)

print("")
tabE1 = [
["\sigma_0",          pn(lo0['value']),          pn(lo0['value'])],
["\sigma_0_86",       pn(lo086['value']),        pn(lo086['value'])],
["\sigma_0_71",       pn(lo071['value']),        pn(lo071['value'])],
["\sigma_0_66",       pn(lo066['value']),        pn(lo066['value'])],
["\sigma_0_60",       pn(lo060['value']),        pn(lo060['value'])],
SEPARATING_LINE,
["\sigma^{(1)}_e",    pn(nloEPHm1['value']),     pn(nloEPHm1['value']),     pn(dn(nloEPHm1['value'],lo00)*100.),           pn(dn(nloEPHm1['value'],lo00)*100.)],
["\sigma^{(1)}_eF",   pn(nloEVPm1['value']),     pn(nloEVPm1['value']),     pn(dn(nloEVPm1['value'],lo00)*100.),           pn(dn(nloEVPm1['value'],lo00)*100.)],
["\sigma^{(1)}_e86",  pn(nloFPHp1L86['value']),  pn(nloFPHp1L86['value']),  pn(dn(nloFPHp1L86['value'],lo86)*100.),        pn(dn(nloFPHp1L86['value'],lo86)*100.)],
["\sigma^{(1)}_eF86", pn(nloFVPp1L86['value']),  pn(nloFVPp1L86['value']),  pn(dn(nloFVPp1L86['value'],lo86)*100.),        pn(dn(nloFVPp1L86['value'],lo86)*100.)],
["\sigma^{(1)}_e71",  pn(nloFPHp1L71['value']),  pn(nloFPHp1L71['value']),  pn(dn(nloFPHp1L71['value'],lo71)*100.),        pn(dn(nloFPHp1L71['value'],lo71)*100.)],
["\sigma^{(1)}_eF71", pn(nloFVPp1L71['value']),  pn(nloFVPp1L71['value']),  pn(dn(nloFVPp1L71['value'],lo71)*100.),        pn(dn(nloFVPp1L71['value'],lo71)*100.)],
["\sigma^{(1)}_e66",  pn(nloFPHp1L66['value']),  pn(nloFPHp1L66['value']),  pn(dn(nloFPHp1L66['value'],lo66)*100.),        pn(dn(nloFPHp1L66['value'],lo66)*100.)],
["\sigma^{(1)}_eF66", pn(nloFVPp1L66['value']),  pn(nloFVPp1L66['value']),  pn(dn(nloFVPp1L66['value'],lo66)*100.),        pn(dn(nloFVPp1L66['value'],lo66)*100.)],
["\sigma^{(1)}_e60",  pn(nloFPHp1L60['value']),  pn(nloFPHp1L60['value']),  pn(dn(nloFPHp1L60['value'],lo60)*100.),        pn(dn(nloFPHp1L60['value'],lo60)*100.)],
["\sigma^{(1)}_eF60", pn(nloFVPp1L60['value']),  pn(nloFVPp1L60['value']),  pn(dn(nloFVPp1L60['value'],lo60)*100.),        pn(dn(nloFVPp1L60['value'],lo60)*100.)],
["\sigma^{(1)}_x",    pn(nloXPHm1['value']),     pn(nloXPHp1),              pn(dn(nloXPHm1['value'],lo00)*100.),           pn(dn(nloXPHp1,lo00)*100.)],
["\sigma^{(1)}_x86",  pn(nloYPHm1L86),           pn(nloYPHp1L86['value']),  pn(dn(nloYPHm1L86,lo86)*100.),                 pn(dn(nloYPHp1L86['value'],lo86)*100.)],
["\sigma^{(1)}_x71",  pn(nloYPHm1L71),           pn(nloYPHp1L71['value']),  pn(dn(nloYPHm1L71,lo71)*100.),                 pn(dn(nloYPHp1L71['value'],lo71)*100.)],
["\sigma^{(1)}_x66",  pn(nloYPHm1L66),           pn(nloYPHp1L66['value']),  pn(dn(nloYPHm1L66,lo66)*100.),                 pn(dn(nloYPHp1L66['value'],lo66)*100.)],
["\sigma^{(1)}_x60",  pn(nloYPHm1L60),           pn(nloYPHp1L60['value']),  pn(dn(nloYPHm1L60,lo60)*100.),                 pn(dn(nloYPHp1L60['value'],lo60)*100.)],
["\sigma^{(1)}_p",    pn(nloMPHm1['value']),     pn(nloMPHm1['value']),     pn(dn(nloMPHm1['value'],lo00)*100.),           pn(dn(nloMPHm1['value'],lo00)*100.)],
SEPARATING_LINE,
["\sigma^{(2)}_e",    pn(nnloEPHm1['value']),    pn(nnloEPHm1['value']),    pn(dn(nnloEPHm1['value'],nlom1)*100.),         pn(dn(nnloEPHm1['value'],nlop1)*100.)],
["\sigma^{(2)}_eF",   pn(nnloEVPm1['value']),    pn(nnloEVPm1['value']),    pn(dn(nnloEVPm1['value'],nlom1)*100.),         pn(dn(nnloEVPm1['value'],nlop1)*100.)],
["\sigma^{(2)}_e71",  pn(nnloFPHp1L71['value']), pn(nnloFPHp1L71['value']), pn(dn(nnloFPHp1L71['value'],nlom171)*100.),    pn(dn(nnloFPHp1L71['value'],nlop171)*100.)],
["\sigma^{(2)}_eF71", pn(nnloFVPp1L71['value']), pn(nnloFVPp1L71['value']), pn(dn(nnloFVPp1L71['value'],nlom171)*100.),    pn(dn(nnloFVPp1L71['value'],nlop171)*100.)],
["\sigma^{(2)}_x",    pn(nnloYPHm1),             pn(nnloYPHp1),             pn(dn(nnloYPHm1,nlom1)*100.),                  pn(dn(nnloYPHp1,nlop1)*100.)],
["\sigma^{(2)}_xF",   pn(nnloXVPm1['value']),    pn(nnloXVPp1),             pn(dn(nnloXVPm1['value'],nlom1)*100.),         pn(dn(nnloXVPp1,nlop1)*100.)],
["\sigma^{(2)}_p",    pn(nnloMPHm1['value']),    pn(nnloMPHm1['value']),    pn(dn(nnloMPHm1['value'],nlom1)*100.),         pn(dn(nnloMPHm1['value'],nlop1)*100.)],
["\sigma^{(2)}_pF",   pn(nnloMVPm1['value']),    pn(nnloMVPm1['value']),    pn(dn(nnloMVPm1['value'],nlom1)*100.),         pn(dn(nnloMVPm1['value'],nlop1)*100.)],
SEPARATING_LINE
]
print(tabulate(tabE1, headers=["MUSE_e 210 [S1]","e+p", "e-p", "dK+%", "dK-%"]))

##########################################################################}}}
### Total cross section -- Bar plots{{{
def get_cumulated_array(data, **kwargs):
    cum = data.clip(**kwargs)
    cum = np.cumsum(cum, axis=0)
    d = np.zeros(np.shape(data))
    d[1:] = cum[:-1]
    return d

def cumulate(countsPH, countsVP, errorFF):
    data = np.array([countsPH, countsVP])
    data_shape = np.shape(data)
    cumulated_data = get_cumulated_array(data, min=0)
    cumulated_data_neg = get_cumulated_array(data, max=0)
    row_mask = (data<0)
    cumulated_data[row_mask] = cumulated_data_neg[row_mask]
    data_stack = cumulated_data
    return data_stack

def barplot(xorder, countsPH, countsVP, data_stack, errorFF):
    a11=ax1.bar(xorder, countsPH, bottom=data_stack[0])
    a12=ax2.bar(xorder, countsPH, bottom=data_stack[0])
    a13=ax3.bar(xorder, countsPH, bottom=data_stack[0])
    a21=ax1.bar(xorder, countsVP, bottom=data_stack[1], yerr=errorFF)
    a22=ax2.bar(xorder, countsVP, bottom=data_stack[1], yerr=errorFF)
    a23=ax3.bar(xorder, countsVP, bottom=data_stack[1], yerr=errorFF)

    ax1.xaxis.set_ticks_position('none')
    ax2.xaxis.set_ticks_position('none')
    ax1.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax3.spines['top'].set_visible(False)

    dslant = .3
    kwargs = dict(marker=[(-1, -dslant), (1, dslant)], markersize=12,
              linestyle="none", color='k', mec='k', mew=1, clip_on=False)
    ax1.plot([0, 1], [0, 0], transform=ax1.transAxes, **kwargs)
    ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs)
    ax2.plot([0, 1], [0, 0], transform=ax2.transAxes, **kwargs)
    ax3.plot([0, 1], [1, 1], transform=ax3.transAxes, **kwargs)
    ax3.axhline(0, color='black', linewidth=0.2, zorder=1)
    ax1.legend((a11[0], a21[0]), ('{\\rm photonic}', '{\\rm fermionic}'),loc='upper right')

xorder = [
    '$\sigma_0^{\\infty}$', '$\sigma_0^{71}$',
    '$\sigma^{(1)\\infty}_e$', '$\sigma^{(1)71}_e$', '$\sigma^{(1)\\infty}_x$', '$\sigma^{(1)71}_x$', '$\sigma^{(1)\\infty}_p$',
    '$\sigma^{(2)\\infty}_e$', '$\sigma^{(2)71}_e$', '$\sigma^{(2)\\infty}_x$', '$\sigma^{(2)\\infty}_p$'
]

# $\bullet$ S0
fig, (ax1, ax2, ax3) = subplots(
    3, 1,
    sharex=True,
    gridspec_kw={'hspace': 0.03, 'height_ratios':[.5,.7,1]}
)

countsPH = [
    lo0['value'][0], lo071['value'][0],
    nloEPHm0['value'][0], nloFPHp0L71['value'][0], nloXPHp0[0], nloYPHp0L71['value'][0], nloMPHm0['value'][0],
    nnloEPHm0['value'][0], nnloFPHp0L71['value'][0], nnloYPHp0[0], nnloMPHm0['value'][0]
]
countsVP = [
    0., 0.,
    nloEVPm0['value'][0], nloFVPp0L71['value'][0], 0., 0., 0.,
    nnloEVPm0['value'][0], nnloFVPp0L71['value'][0], nnloXVPp0[0], nnloMVPm0['value'][0]
]
errorFF = [
    0., abs(lo086['value'][0]-lo060['value'][0]),
    0., np.sqrt((nloFPHp0L86['value'][0]-nloFPHp0L60['value'][0])**2+(nloFVPp0L86['value'][0]-nloFVPp0L60['value'][0])**2),
    0., abs(nloYPHp0L86['value'][0]-nloYPHp0L60['value'][0]), 0.,  0., 0., 0., 0.
]

stack = cumulate(countsPH, countsVP, errorFF)
barplot(xorder, countsPH, countsVP, stack, errorFF)
ax1.set_title('$e^-p \quad/ {\\rm\\upmu b}\quad [{\\tt S0}]$')
ax1.set_ylim(29., 42.)
ax2.set_ylim(.76, 6.99)
ax3.set_ylim(-0.02, 0.21)
draw()
mulify(fig, delx=3.8, dely=2.2)
fig.savefig("plots/xsec-ep-0.pdf")

# $\bullet$ S1
fig, (ax1, ax2, ax3) = subplots(
    3, 1,
    sharex=True,
    gridspec_kw={'hspace': 0.03, 'height_ratios':[.5,.7,1]}
)

countsPH = [
    lo0['value'][0], lo071['value'][0],
    nloEPHm1['value'][0],nloFPHp1L71['value'][0], nloXPHp1[0],nloYPHp1L71['value'][0], nloMPHm1['value'][0],
    nnloEPHm1['value'][0],nnloFPHp1L71['value'][0], nnloYPHp1[0], nnloMPHm1['value'][0]
]
countsVP = [
    0., 0.,
    nloEVPm1['value'][0],nloFVPp1L71['value'][0], 0., 0., 0.,
    nnloEVPm1['value'][0],nnloFVPp1L71['value'][0], nnloXVPp1[0], nnloMVPm1['value'][0]
]
errorFF = [
    0., abs(lo086['value'][0]-lo060['value'][0]),
    0., np.sqrt((nloFPHp1L86['value'][0]-nloFPHp1L60['value'][0])**2+(nloFVPp1L86['value'][0]-nloFVPp1L60['value'][0])**2),
    0., abs(nloYPHp1L86['value'][0]-nloYPHp1L60['value'][0]), 0.,  0., 0., 0., 0.
]

stack = cumulate(countsPH, countsVP, errorFF)
barplot(xorder, countsPH, countsVP, stack, errorFF)
ax1.set_ylim(29., 42.)
ax2.set_ylim(.76, 6.99)
ax3.set_ylim(-0.02, 0.21)
ax1.set_title('$e^-p \quad/ {\\rm\\upmu b}\quad [{\\tt S1}]$')
draw()
mulify(fig, delx=3.8, dely=2.2)
fig.savefig("plots/xsec-ep-1.pdf")
###########################################################################}}}
### Differential distributions $-$ $Q^2${{{
def ebandr0ls(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var0, msc, *args, **kwargs):
       nlol  = addplots(var9['qsql'],addplots(var10['qsql'],addplots(var11['qsql'],var12['qsql'])))
       nnlol = addplots(var1['qsql'],
                        addplots(scaleplot(var2['qsql'],1.,-1.),addplots(var3['qsql'],addplots(scaleplot(var4['qsql'],1.,-1.),
                        addplots(var5['qsql'],
                        addplots(var6['qsql'],addplots(scaleplot(var7['qsql'],1.,-1.),var8['qsql'])))))))
       errorband(mergebins(scaleplot(divideplots(
           addplots(nnlol,nlol), var0['qsql']),1e6,1.), msc), *args, **kwargs)
def ebandr0ps(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var0, msc, *args, **kwargs):
       nlop  = addplots(var9['qsqp'],addplots(var10['qsqp'],addplots(var11['qsqp'],var12['qsqp'])))
       nnlop = addplots(var1['qsqp'],
                        addplots(scaleplot(var2['qsqp'],1.,-1.),addplots(var3['qsqp'],addplots(scaleplot(var4['qsqp'],1.,-1.),
                        addplots(var5['qsqp'],
                        addplots(var6['qsqp'],addplots(scaleplot(var7['qsqp'],1.,-1.),var8['qsqp'])))))))
       errorband(mergebins(scaleplot(divideplots(
           addplots(nnlop,nlop), var0['qsqp']),1e6,1.), msc), *args, **kwargs)

# $\bullet$ S0
fig, axs = plt.subplots(
    1, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios':[1]},
)
sca(axs)
mmsc=5

ebandr0ls(nnloEPHm0,nnloX31PHm0,nnloX22PHm0,nnloX13PHm0,nnloMPHm0,nnloEVPm0,nnloXVPm0,nnloMVPm0,
          nloFPHp0L71,nloFVPp0L71,nloYPHp0L71,nloMPHm0,
          lo071, mmsc, col='blue',linestyle='--')
ebandr0ps(nnloEPHm0,nnloX31PHm0,nnloX22PHm0,nnloX13PHm0,nnloMPHm0,nnloEVPm0,nnloXVPm0,nnloMVPm0,
          nloFPHp0L71,nloFVPp0L71,nloYPHp0L71,nloMPHm0,
          lo071, mmsc, col='red',linestyle='--')
ebandr0ls(nnloEPHm1,nnloX31PHm1,nnloX22PHm1,nnloX13PHm1,nnloMPHm1,nnloEVPm1,nnloXVPm1,nnloMVPm1,
          nloFPHp1L71,nloFVPp1L71,nloYPHp1L71,nloMPHm1,
          lo071, mmsc, col='cyan',linestyle=':')
ebandr0ps(nnloEPHm1,nnloX31PHm1,nnloX22PHm1,nnloX13PHm1,nnloMPHm1,nnloEVPm1,nnloXVPm1,nnloMVPm1,
          nloFPHp1L71,nloFVPp1L71,nloYPHp1L71,nloMPHm1,
          lo071, mmsc, col='orange',linestyle=':')

axs.set_title('$e^-p \quad [{\\tt S0, S1}]$')
axs.legend([
    r'$[{\tt S0}]\,\, (Q^2_e)$',
    r'$[{\tt S0}]\,\, (Q^2_p)$',
    r'$[{\tt S1}]\,\, (Q^2_e)$',
    r'$[{\tt S1}]\,\, (Q^2_p)$',
], ncol=2, loc='lower center',framealpha=0)

xlabel(r'$Q^2_e\,{\rm or}\,Q^2_p\,/\,{\rm GeV}^2$')
ylabel(r'$\frac{\sigma^{(1)\,71}+\sigma^{(2)\infty}}{\sigma_0^{71}}$')
xlim(8.e-3, 8.e-2)
draw()
fig.tight_layout()
mulify(fig, delx=4.2, dely=-0.1)
fig.savefig("plots/qsql-e-S0S1-2-diff.pdf", bbox_inches='tight')
###########################################################################}}}
### Differential distributions $-$ $\theta_e${{{
def diffset(a,b):
    return addsets([a, scaleset(b,-1)])

def eband(var, msc, *args, **kwargs):
       errorband(mergebins(var['thetal'], msc), *args, **kwargs)
def ebanda(var, msc, *args, **kwargs):
       errorband(mergebins(abs(var['thetal']), msc), *args, **kwargs)
def ebandp(var, msc, *args, **kwargs):
       errorband(mergebins(abs(scaleplot(var['thetal'],2.)), msc), *args, **kwargs)

# $\bullet$ S0

fig, axs = subplots(
    1,
    sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios':[1]},
    figsize=(5.9, 6.)
)
sca(axs)

mmsc=5
nloXPHp0 = scaleset(nloXPHm0,-1)
eband(lo0,mmsc,col='slategray')
eband(addsets([nloEPHm0, nloEVPm0]),mmsc,col='gold')
eband(nloXPHp0,mmsc,col='green')
eband(diffset(lo0, lo071),mmsc,col='tab:red')
ebanda(nnloEPHm0,mmsc,linestyle='--',col='cyan')
ebanda(nnloEVPm0,mmsc,linestyle=':',col='darkcyan')
eband(diffset(nnloX22PHm0, addsets([nnloX31PHm0, nnloX13PHm0])),mmsc)
eband(diffset(nloXPHp0, nloYPHp0L71),mmsc, col='pink')

axs.set_title('$e^-p \quad [{\\tt S0}]$')
axs.legend([
    r'$\sigma_0^\infty$', r'$\sigma^{(1)\infty}_e+\sigma^{(1)\infty}_\Pi$', r'$\sigma^{(1)\infty}_x$', r'$\sigma_0^\infty-\sigma_0^{71}$',
    r'$|\sigma^{(2)\infty}_e|$', r'$\sigma^{(2)\infty}_{e\Pi}$', r'$\sigma^{(2)\infty}_x$', r'$\sigma^{(1)\infty}_x-\sigma^{(1)71}_x$'
], ncol=2, loc='upper right',framealpha=0)

xlabel(r'$\theta_e\,/\,{\rm deg}$')
ylabel('$\\D\\sigma/\\D\\theta_e\ /\ {\\rm\\upmu b}$')
yscale('log')
ylim(3.e-6, 9.e1)
draw()
fig.tight_layout()
mulify(fig, delx=0.3, dely=3.4)
fig.savefig("plots/the-S0-diff.pdf")

# $\bullet$ S0 ($e^-$ and $e^+$)

fig, axs = subplots(
    1,
    sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios':[1]},
    figsize=(5.9, 6.)
)
sca(axs)

mmsc=5
nloYPHp0L71 = scaleset(nloXPHm0,-1)
ebandp(nloYPHp0L71,mmsc)
ebandp(addsets([nnloX31PHm0, nnloX13PHm0, nnloXVPm0]),mmsc)

axs.set_title('$e^-p - e^+p \quad [{\\tt S0}]$')
axs.legend([
    r'$\sigma^{(1)\,71}_x$', r'$\sigma^{(2)\infty}_x+\sigma^{(2)\infty}_{x\Pi}$'
], ncol=1, loc='upper right',framealpha=0)

xlabel(r'$\theta_e\,/\,{\rm deg}$')
ylabel('$\\D\\sigma/\\D\\theta_e\ /\ {\\rm\\upmu b}$')
yscale('log')
ylim(3.e-6, 2.e-1)
draw()
fig.tight_layout()
mulify(fig, delx=0.3, dely=3.4)
fig.savefig("plots/the-S0-diff-x.pdf")

# $\bullet$ S1

fig, axs = subplots(
    1,
    sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios':[1]},
    figsize=(5.9, 6.)
)
sca(axs)

mmsc=5
nloXPHp1 = scaleset(nloXPHm1,-1)
eband(lo0,mmsc,col='slategray')
eband(addsets([nloEPHm1, nloEVPm1]),mmsc,col='gold')
eband(nloXPHp1,mmsc,col='green')
eband(diffset(lo0, lo071),mmsc,col='tab:red')
ebanda(nnloEPHm1,mmsc,linestyle='--',col='cyan')
ebanda(nnloEVPm1,mmsc,linestyle=':',col='darkcyan')
eband(diffset(nnloX22PHm1, addsets([nnloX31PHm1, nnloX13PHm1])),mmsc)
eband(diffset(nloXPHp1, nloYPHp1L71),mmsc, col='pink')

axs.set_title('$e^-p \quad [{\\tt S1}]$')
axs.legend([
    r'$\sigma_0^\infty$', r'$\sigma^{(1)\infty}_e+\sigma^{(1)\infty}_\Pi$', r'$\sigma^{(1)\infty}_x$', r'$\sigma_0^\infty-\sigma_0^{71}$',
    r'$|\sigma^{(2)\infty}_e|$', r'$\sigma^{(2)\infty}_{e\Pi}$', r'$\sigma^{(2)\infty}_x$', r'$\sigma^{(1)\infty}_x-\sigma^{(1)71}_x$'
], ncol=2, loc='upper right',framealpha=0)

xlabel(r'$\theta_e\,/\,{\rm deg}$')
ylabel('$\\D\\sigma/\\D\\theta_e\ /\ {\\rm\\upmu b}$')
yscale('log')
ylim(3.e-6, 9.e1)
draw()
fig.tight_layout()
mulify(fig, delx=0.3, dely=3.4)
fig.savefig("plots/the-S1-diff.pdf")

# $\bullet$ S1 ($e^-$ and $e^+$)

fig, axs = subplots(
    1,
    sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios':[1]},
    figsize=(5.9, 6.)
)
sca(axs)

mmsc=5
nloXPHp0 = scaleset(nloXPHm0,-1)
ebandp(nloYPHp1L71,mmsc)
ebandp(addsets([nnloX31PHm1, nnloX13PHm1, nnloXVPm1]),mmsc)

axs.set_title('$e^-p - e^+p \quad [{\\tt S1}]$')
axs.legend([
    r'$\sigma^{(1)\,71}_x$', r'$\sigma^{(2)\infty}_x+\sigma^{(2)\infty}_{x\Pi}$'
], ncol=1, loc='upper right',framealpha=0)

xlabel(r'$\theta_e\,/\,{\rm deg}$')
ylabel('$\\D\\sigma/\\D\\theta_e\ /\ {\\rm\\upmu b}$')
yscale('log')
ylim(3.e-6, 2.e-1)
draw()
fig.tight_layout()
mulify(fig, delx=0.3, dely=3.4)
fig.savefig("plots/the-S1-diff-x.pdf")

##########################################################################}}}
#### $\mu^\pm p \to \mu^\pm p${{{
# $\bullet$ pure QED

setup(folder='lp2lp_tpe_paper/out-muse-m-qed.tar.bz2')
setup(obs='0')
Mlo0         = scaleset(mergefks(sigma('em2em0')), alpha**2*conv)
MnloEPHm0    = scaleset(mergefks(sigma('em2emFEE'),sigma('em2emREE15'),sigma('em2emREE35')), alpha**3*conv)
MnloEVPm0    = scaleset(mergefks(sigma('em2emA')), alpha**3*conv)
MnloXPHm0    = scaleset(mergefks(sigma('em2emFEM'),sigma('em2emREM')), alpha**3*conv)
MnloMPHm0    = scaleset(mergefks(sigma('em2emFMM'),sigma('em2emRMM')), alpha**3*conv)
MnnloEPHm0   = scaleset(mergefks(sigma('em2emFFEEEE',obs='1'),sigma('em2emRFEEEE15'),sigma('em2emRFEEEE35'),sigma('em2emRREEEE1516'),sigma('em2emRREEEE3536')), alpha**4*conv)
MnnloX31PHm0 = scaleset(mergefks(sigma('em2emFF31z',obs='1'),sigma('em2emRF3115'),sigma('em2emRF3135'),sigma('em2emRR311516'),sigma('em2emRR313536')), alpha**4*conv)
MnnloX22PHm0 = scaleset(mergefks(sigma('em2emFF22z',obs='1'),sigma('em2emRF2215'),sigma('em2emRF2235'),sigma('em2emRR221516'),sigma('em2emRR223536')), alpha**4*conv)
MnnloX13PHm0 = scaleset(mergefks(sigma('em2emFF13z',obs='1'),sigma('em2emRF1315'),sigma('em2emRF1335'),sigma('em2emRR131516'),sigma('em2emRR133536')), alpha**4*conv)
MnnloMPHm0   = scaleset(mergefks(sigma('em2emFFMMMM',obs='1'),sigma('em2emRFMMMM'),sigma('em2emRRMMMM')), alpha**4*conv)
MnnloEVPm0   = scaleset(mergefks(sigma('em2emAFEE'), sigma('em2emAREE15'), sigma('em2emAREE35'),
                                 anyxi1=sigma('em2emAA'), anyxi2=sigma('em2emNFEE')), alpha**4*conv)
MnnloXVPm0   = scaleset(mergefks(sigma('em2emAFEM'), sigma('em2emAREM'),anyxi1=sigma('em2emNFEM',obs='0')), alpha**4*conv)
MnnloMVPm0   = scaleset(mergefks(sigma('em2emAFMM'), sigma('em2emARMM'),anyxi1=sigma('em2emNFMM',obs='0')), alpha**4*conv)

setup(obs='1')
Mlo1         = scaleset(mergefks(sigma('em2em0',obs='0')), alpha**2*conv)
MnloEPHm1    = scaleset(mergefks(sigma('em2emFEE',obs='0'),sigma('em2emREE15'),sigma('em2emREE35')), alpha**3*conv)
MnloEVPm1    = scaleset(mergefks(sigma('em2emA',obs='0')), alpha**3*conv)
MnloXPHm1    = scaleset(mergefks(sigma('em2emFEM',obs='0'),sigma('em2emREM')), alpha**3*conv)
MnloMPHm1    = scaleset(mergefks(sigma('em2emFMM',obs='0'),sigma('em2emRMM')), alpha**3*conv)
MnnloEPHm1   = scaleset(mergefks(sigma('em2emFFEEEE'),sigma('em2emRFEEEE15'),sigma('em2emRFEEEE35'),sigma('em2emRREEEE1516'),sigma('em2emRREEEE3536')), alpha**4*conv)
MnnloX31PHm1 = scaleset(mergefks(sigma('em2emFF31z'),sigma('em2emRF3115'),sigma('em2emRF3135'),sigma('em2emRR311516'),sigma('em2emRR313536')), alpha**4*conv)
MnnloX22PHm1 = scaleset(mergefks(sigma('em2emFF22z'),sigma('em2emRF2215'),sigma('em2emRF2235'),sigma('em2emRR221516'),sigma('em2emRR223536')), alpha**4*conv)
MnnloX13PHm1 = scaleset(mergefks(sigma('em2emFF13z'),sigma('em2emRF1315'),sigma('em2emRF1335'),sigma('em2emRR131516'),sigma('em2emRR133536')), alpha**4*conv)
MnnloMPHm1   = scaleset(mergefks(sigma('em2emFFMMMM'),sigma('em2emRFMMMM'),sigma('em2emRRMMMM')), alpha**4*conv)
MnnloEVPm1   = scaleset(mergefks(sigma('em2emAFEE',obs='0'), sigma('em2emAREE15'), sigma('em2emAREE35'),
                                 anyxi1=sigma('em2emAA',obs='0'), anyxi2=sigma('em2emNFEE',obs='0')), alpha**4*conv)
MnnloXVPm1   = scaleset(mergefks(sigma('em2emAFEM',obs='0'), sigma('em2emAREM'),anyxi1=sigma('em2emNFEM',obs='0')), alpha**4*conv)
MnnloMVPm1   = scaleset(mergefks(sigma('em2emAFMM',obs='0'), sigma('em2emARMM'),anyxi1=sigma('em2emNFMM',obs='0')), alpha**4*conv)

# $\bullet$ $\Lambda^2=0.86$ GeV^2

setup(folder='lp2lp_tpe_paper/out-muse-m-86.tar.bz2')
setup(obs='0')
Mlo086          = scaleset(mergefks(sigma('mp2mp0')), alpha**2*conv)
MnloFPHp0L86    = scaleset(mergefks(sigma('mp2mpF'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
MnloFVPp0L86    = scaleset(mergefks(sigma('mp2mpA')), alpha**3*conv)
MnloYPHp0L86    = scaleset(mergefks(sigma('mp2mpFMP'),sigma('mp2mpRMP')), alpha**3*conv)

setup(obs='1')
MnloFVPp1L86    = scaleset(mergefks(sigma('mp2mpA',obs='0')), alpha**3*conv)
MnloFPHp1L86    = scaleset(mergefks(sigma('mp2mpF',obs='0'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
MnloYPHp1L86    = scaleset(mergefks(sigma('mp2mpFMP',obs='0'),sigma('mp2mpRMP')), alpha**3*conv)

# $\bullet$ $\Lambda^2=0.71$ GeV^2

setup(folder='lp2lp_tpe_paper/out-muse-m-71.tar.bz2')
setup(obs='0')
Mlo071          = scaleset(mergefks(sigma('mp2mp0')), alpha**2*conv)
MnloFPHp0L71    = scaleset(mergefks(sigma('mp2mpF'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
MnloFVPp0L71    = scaleset(mergefks(sigma('mp2mpA')), alpha**3*conv)
MnloYPHp0L71    = scaleset(mergefks(sigma('mp2mpFMP'),sigma('mp2mpRMP')), alpha**3*conv)
MnnloFPHp0L71   = scaleset(mergefks(sigma('mp2mpFF'),sigma('mp2mpRF15'),sigma('mp2mpRF35'),sigma('mp2mpRR1516'),sigma('mp2mpRR3536')), alpha**4*conv)
MnnloFVPp0L71   = scaleset(mergefks(sigma('mp2mpAF'), sigma('mp2mpAR15'), sigma('mp2mpAR35'),
                                    anyxi1=sigma('mp2mpAA'), anyxi2=sigma('mp2mpNF')), alpha**4*conv)

setup(obs='1')
MnloFPHp1L71    = scaleset(mergefks(sigma('mp2mpF',obs='0'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
MnloFVPp1L71    = scaleset(mergefks(sigma('mp2mpA',obs='0')), alpha**3*conv)
MnloYPHp1L71    = scaleset(mergefks(sigma('mp2mpFMP',obs='0'),sigma('mp2mpRMP')), alpha**3*conv)
MnnloFPHp1L71   = scaleset(mergefks(sigma('mp2mpFF',obs='0'),sigma('mp2mpRF15'),sigma('mp2mpRF35'),sigma('mp2mpRR1516'),sigma('mp2mpRR3536')), alpha**4*conv)
MnnloFVPp1L71   = scaleset(mergefks(sigma('mp2mpAF',obs='0'), sigma('mp2mpAR15'), sigma('mp2mpAR35'),
                                    anyxi1=sigma('mp2mpAA',obs='0'), anyxi2=sigma('mp2mpNF',obs='0')), alpha**4*conv)

# $\bullet$ $\Lambda^2=0.66$ GeV^2

setup(folder='lp2lp_tpe_paper/out-muse-m-66.tar.bz2')
setup(obs='0')
Mlo066          = scaleset(mergefks(sigma('mp2mp0')), alpha**2*conv)
MnloFPHp0L66    = scaleset(mergefks(sigma('mp2mpF'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
MnloFVPp0L66    = scaleset(mergefks(sigma('mp2mpA')), alpha**3*conv)
MnloYPHp0L66    = scaleset(mergefks(sigma('mp2mpFMP'),sigma('mp2mpRMP')), alpha**3*conv)

setup(obs='1')
MnloFVPp1L66    = scaleset(mergefks(sigma('mp2mpA',obs='0')), alpha**3*conv)
MnloFPHp1L66    = scaleset(mergefks(sigma('mp2mpF',obs='0'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
MnloYPHp1L66    = scaleset(mergefks(sigma('mp2mpFMP',obs='0'),sigma('mp2mpRMP')), alpha**3*conv)

# $\bullet$ $\Lambda^2=0.60$ GeV^2

setup(folder='lp2lp_tpe_paper/out-muse-m-60.tar.bz2')
setup(obs='0')
Mlo060          = scaleset(mergefks(sigma('mp2mp0')), alpha**2*conv)
MnloFPHp0L60    = scaleset(mergefks(sigma('mp2mpF'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
MnloFVPp0L60    = scaleset(mergefks(sigma('mp2mpA')), alpha**3*conv)
MnloYPHp0L60    = scaleset(mergefks(sigma('mp2mpFMP'),sigma('mp2mpRMP')), alpha**3*conv)

setup(obs='1')
MnloFVPp1L60    = scaleset(mergefks(sigma('mp2mpA',obs='0')), alpha**3*conv)
MnloFPHp1L60    = scaleset(mergefks(sigma('mp2mpF',obs='0'),sigma('mp2mpR15'),sigma('mp2mpR35')), alpha**3*conv)
MnloYPHp1L60    = scaleset(mergefks(sigma('mp2mpFMP',obs='0'),sigma('mp2mpRMP')), alpha**3*conv)

#########################################################################}}}
### Total cross sections ($\mu$b){{{
# $\bullet$ S0

Mlo00 = Mlo0['value']
Mlo86 = Mlo086['value']
Mlo71 = Mlo071['value']
Mlo66 = Mlo066['value']
Mlo60 = Mlo060['value']

MnloXPHp0    = tn(MnloXPHm0['value'], [-1., 0])
MnloYPHm0L86 = tn(MnloYPHp0L86['value'], [-1., 0])
MnloYPHm0L71 = tn(MnloYPHp0L71['value'], [-1., 0])
MnloYPHm0L60 = tn(MnloYPHp0L60['value'], [-1., 0])
MnloYPHm0L66 = tn(MnloYPHp0L66['value'], [-1., 0])

Mnlom0     = plusnumbers(MnloEPHm0['value'], MnloEVPm0['value'],  MnloXPHm0['value'],  MnloMPHm0['value'],  Mlo00)
Mnlop0     = plusnumbers(MnloEPHm0['value'], MnloEVPm0['value'], -MnloXPHm0['value'],  MnloMPHm0['value'],  Mlo00)
Mnlom071   = plusnumbers(MnloFPHp0L71['value'], MnloFVPp0L71['value'], -MnloYPHp0L71['value'],  MnloMPHm0['value'],  Mlo71)
Mnlop071   = plusnumbers(MnloFPHp0L71['value'], MnloFVPp0L71['value'],  MnloYPHp0L71['value'],  MnloMPHm0['value'],  Mlo71)

MnnloYPHm0 = plusnumbers( MnnloX31PHm0['value'], MnnloX22PHm0['value'],  MnnloX13PHm0['value'])
MnnloYPHp0 = plusnumbers(-MnnloX31PHm0['value'], MnnloX22PHm0['value'], -MnnloX13PHm0['value'])
MnnloXVPp0 = tn(MnnloXVPm0['value'], [-1., 0])
Mnnlom0    = plusnumbers(MnnloEPHm0['value'], MnnloYPHm0, MnnloMPHm0['value'], Mnlom0)
Mnnlop0    = plusnumbers(MnnloEPHm0['value'], MnnloYPHp0, MnnloMPHm0['value'], Mnlop0)

print("")
tabM0 = [
["\sigma_0",          pn(Mlo0['value']),          pn(Mlo0['value'])],
["\sigma_0_86",       pn(Mlo086['value']),        pn(Mlo086['value'])],
["\sigma_0_71",       pn(Mlo071['value']),        pn(Mlo071['value'])],
["\sigma_0_66",       pn(Mlo066['value']),        pn(Mlo066['value'])],
["\sigma_0_60",       pn(Mlo060['value']),        pn(Mlo060['value'])],
SEPARATING_LINE,
["\sigma^{(1)}_m",    pn(MnloEPHm0['value']),     pn(MnloEPHm0['value']),     pn(dn(MnloEPHm0['value'],Mlo00)*100.),            pn(dn(MnloEPHm0['value'],Mlo00)*100.)],
["\sigma^{(1)}_mF",   pn(MnloEVPm0['value']),     pn(MnloEVPm0['value']),     pn(dn(MnloEVPm0['value'],Mlo00)*100.),            pn(dn(MnloEVPm0['value'],Mlo00)*100.)],
["\sigma^{(1)}_m86",  pn(MnloFPHp0L86['value']),  pn(MnloFPHp0L86['value']),  pn(dn(MnloFPHp0L86['value'],Mlo86)*100.),         pn(dn(MnloFPHp0L86['value'],Mlo86)*100.)],
["\sigma^{(1)}_mF86", pn(MnloFVPp0L86['value']),  pn(MnloFVPp0L86['value']),  pn(dn(MnloFVPp0L86['value'],Mlo86)*100.),         pn(dn(MnloFVPp0L86['value'],Mlo86)*100.)],
["\sigma^{(1)}_m71",  pn(MnloFPHp0L71['value']),  pn(MnloFPHp0L71['value']),  pn(dn(MnloFPHp0L71['value'],Mlo71)*100.),         pn(dn(MnloFPHp0L71['value'],Mlo71)*100.)],
["\sigma^{(1)}_mF71", pn(MnloFVPp0L71['value']),  pn(MnloFVPp0L71['value']),  pn(dn(MnloFVPp0L71['value'],Mlo71)*100.),         pn(dn(MnloFVPp0L71['value'],Mlo71)*100.)],
["\sigma^{(1)}_m66",  pn(MnloFPHp0L66['value']),  pn(MnloFPHp0L66['value']),  pn(dn(MnloFPHp0L66['value'],Mlo66)*100.),         pn(dn(MnloFPHp0L66['value'],Mlo66)*100.)],
["\sigma^{(1)}_mF66", pn(MnloFVPp0L66['value']),  pn(MnloFVPp0L66['value']),  pn(dn(MnloFVPp0L66['value'],Mlo66)*100.),         pn(dn(MnloFVPp0L66['value'],Mlo66)*100.)],
["\sigma^{(1)}_m60",  pn(MnloFPHp0L60['value']),  pn(MnloFPHp0L60['value']),  pn(dn(MnloFPHp0L60['value'],Mlo60)*100.),         pn(dn(MnloFPHp0L60['value'],Mlo60)*100.)],
["\sigma^{(1)}_mF60", pn(MnloFVPp0L60['value']),  pn(MnloFVPp0L60['value']),  pn(dn(MnloFVPp0L60['value'],Mlo60)*100.),         pn(dn(MnloFVPp0L60['value'],Mlo60)*100.)],
["\sigma^{(1)}_x",    pn(MnloXPHm0['value']),     pn(MnloXPHp0),              pn(dn(MnloXPHm0['value'],Mlo00)*100.),            pn(dn(MnloXPHp0,Mlo00)*100.)],
["\sigma^{(1)}_x86",  pn(MnloYPHm0L86),           pn(MnloYPHp0L86['value']),  pn(dn(MnloYPHm0L86,Mlo86)*100.),                  pn(dn(MnloYPHp0L86['value'],Mlo86)*100.)],
["\sigma^{(1)}_x71",  pn(MnloYPHm0L71),           pn(MnloYPHp0L71['value']),  pn(dn(MnloYPHm0L71,Mlo71)*100.),                  pn(dn(MnloYPHp0L71['value'],Mlo71)*100.)],
["\sigma^{(1)}_x66",  pn(MnloYPHm0L66),           pn(MnloYPHp0L66['value']),  pn(dn(MnloYPHm0L66,Mlo66)*100.),                  pn(dn(MnloYPHp0L66['value'],Mlo66)*100.)],
["\sigma^{(1)}_x60",  pn(MnloYPHm0L60),           pn(MnloYPHp0L60['value']),  pn(dn(MnloYPHm0L60,Mlo60)*100.),                  pn(dn(MnloYPHp0L60['value'],Mlo60)*100.)],
["\sigma^{(1)}_p",    pn(MnloMPHm0['value']),     pn(MnloMPHm0['value']),     pn(dn(MnloMPHm0['value'],Mlo00)*100.),            pn(dn(MnloMPHm0['value'],Mlo00)*100.)],
SEPARATING_LINE,
["\sigma^{(2)}_m",    pn(MnnloEPHm0['value']),    pn(MnnloEPHm0['value']),    pn(dn(MnnloEPHm0['value'],Mnlom0)*100.),          pn(dn(MnnloEPHm0['value'],Mnlop0)*100.)],
["\sigma^{(2)}_mF",   pn(MnnloEVPm0['value']),    pn(MnnloEVPm0['value']),    pn(dn(MnnloEVPm0['value'],Mnlom0)*100.),          pn(dn(MnnloEVPm0['value'],Mnlop0)*100.)],
["\sigma^{(2)}_m71",  pn(MnnloFPHp0L71['value']), pn(MnnloFPHp0L71['value']), pn(dn(MnnloFPHp0L71['value'],Mnlom071)*100.),     pn(dn(MnnloFPHp0L71['value'],Mnlop071)*100.)],
["\sigma^{(2)}_mF71", pn(MnnloFVPp0L71['value']), pn(MnnloFVPp0L71['value']), pn(dn(MnnloFVPp0L71['value'],Mnlom071)*100.),     pn(dn(MnnloFVPp0L71['value'],Mnlop071)*100.)],
["\sigma^{(2)}_x",    pn(MnnloYPHm0),             pn(MnnloYPHp0),             pn(dn(MnnloYPHm0,Mnlom0)*100.),                   pn(dn(MnnloYPHp0,Mnlop0)*100.)],
["\sigma^{(2)}_xF",   pn(MnnloXVPm0['value']),    pn(MnnloXVPp0),             pn(dn(MnnloXVPm0['value'],Mnlom0)*100.),          pn(dn(MnnloXVPp0,Mnlop0)*100.)],
["\sigma^{(2)}_p",    pn(MnnloMPHm0['value']),    pn(MnnloMPHm0['value']),    pn(dn(MnnloMPHm0['value'],Mnlom0)*100.),          pn(dn(MnnloMPHm0['value'],Mnlop0)*100.)],
["\sigma^{(2)}_pF",   pn(MnnloMVPm0['value']),    pn(MnnloMVPm0['value']),    pn(dn(MnnloMVPm0['value'],Mnlom0)*100.),          pn(dn(MnnloMVPm0['value'],Mnlop0)*100.)],
SEPARATING_LINE
]

print(tabulate(tabM0, headers=["MUSE_m 210 [0]","m+p", "m-p", "dK+%", "dK-%"]))

# $\bullet$ S1

MnloXPHp1    = tn(MnloXPHm1['value'], [-1., 0])
MnloYPHm1L86 = tn(MnloYPHp1L86['value'], [-1., 0])
MnloYPHm1L71 = tn(MnloYPHp1L71['value'], [-1., 0])
MnloYPHm1L60 = tn(MnloYPHp1L60['value'], [-1., 0])
MnloYPHm1L66 = tn(MnloYPHp1L66['value'], [-1., 0])

Mnlom1     = plusnumbers(MnloEPHm1['value'], MnloEVPm1['value'],  MnloXPHm1['value'],  MnloMPHm1['value'],  Mlo00)
Mnlop1     = plusnumbers(MnloEPHm1['value'], MnloEVPm1['value'], -MnloXPHm1['value'],  MnloMPHm1['value'],  Mlo00)
Mnlom171   = plusnumbers(MnloFPHp1L71['value'], MnloFVPp1L71['value'], -MnloYPHp1L71['value'],  MnloMPHm1['value'],  Mlo71)
Mnlop171   = plusnumbers(MnloFPHp1L71['value'], MnloFVPp1L71['value'],  MnloYPHp1L71['value'],  MnloMPHm1['value'],  Mlo71)

MnnloYPHm1 = plusnumbers( MnnloX31PHm1['value'], MnnloX22PHm1['value'],  MnnloX13PHm1['value'])
MnnloYPHp1 = plusnumbers(-MnnloX31PHm1['value'], MnnloX22PHm1['value'], -MnnloX13PHm1['value'])
MnnloXVPp1 = tn(MnnloXVPm1['value'], [-1., 0])
Mnnlom1    = plusnumbers(MnnloEPHm1['value'], MnnloYPHm1, MnnloMPHm1['value'], Mnlom1)
Mnnlop1    = plusnumbers(MnnloEPHm1['value'], MnnloYPHp1, MnnloMPHm1['value'], Mnlop1)

print("")
tabM1 = [
["\sigma_0",          pn(Mlo0['value']),          pn(Mlo0['value'])],
["\sigma_0_86",       pn(Mlo086['value']),        pn(Mlo086['value'])],
["\sigma_0_71",       pn(Mlo071['value']),        pn(Mlo071['value'])],
["\sigma_0_66",       pn(Mlo066['value']),        pn(Mlo066['value'])],
["\sigma_0_60",       pn(Mlo060['value']),        pn(Mlo060['value'])],
SEPARATING_LINE,
["\sigma^{(1)}_m",    pn(MnloEPHm1['value']),     pn(MnloEPHm1['value']),     pn(dn(MnloEPHm1['value'],Mlo00)*100.),            pn(dn(MnloEPHm1['value'],Mlo00)*100.)],
["\sigma^{(1)}_mF",   pn(MnloEVPm1['value']),     pn(MnloEVPm1['value']),     pn(dn(MnloEVPm1['value'],Mlo00)*100.),            pn(dn(MnloEVPm1['value'],Mlo00)*100.)],
["\sigma^{(1)}_m86",  pn(MnloFPHp1L86['value']),  pn(MnloFPHp1L86['value']),  pn(dn(MnloFPHp1L86['value'],Mlo86)*100.),         pn(dn(MnloFPHp1L86['value'],Mlo86)*100.)],
["\sigma^{(1)}_mF86", pn(MnloFVPp1L86['value']),  pn(MnloFVPp1L86['value']),  pn(dn(MnloFVPp1L86['value'],Mlo86)*100.),         pn(dn(MnloFVPp1L86['value'],Mlo86)*100.)],
["\sigma^{(1)}_m71",  pn(MnloFPHp1L71['value']),  pn(MnloFPHp1L71['value']),  pn(dn(MnloFPHp1L71['value'],Mlo71)*100.),         pn(dn(MnloFPHp1L71['value'],Mlo71)*100.)],
["\sigma^{(1)}_mF71", pn(MnloFVPp1L71['value']),  pn(MnloFVPp1L71['value']),  pn(dn(MnloFVPp1L71['value'],Mlo71)*100.),         pn(dn(MnloFVPp1L71['value'],Mlo71)*100.)],
["\sigma^{(1)}_m66",  pn(MnloFPHp1L66['value']),  pn(MnloFPHp1L66['value']),  pn(dn(MnloFPHp1L66['value'],Mlo66)*100.),         pn(dn(MnloFPHp1L66['value'],Mlo66)*100.)],
["\sigma^{(1)}_mF66", pn(MnloFVPp1L66['value']),  pn(MnloFVPp1L66['value']),  pn(dn(MnloFVPp1L66['value'],Mlo66)*100.),         pn(dn(MnloFVPp1L66['value'],Mlo66)*100.)],
["\sigma^{(1)}_m60",  pn(MnloFPHp1L60['value']),  pn(MnloFPHp1L60['value']),  pn(dn(MnloFPHp1L60['value'],Mlo60)*100.),         pn(dn(MnloFPHp1L60['value'],Mlo60)*100.)],
["\sigma^{(1)}_mF60", pn(MnloFVPp1L60['value']),  pn(MnloFVPp1L60['value']),  pn(dn(MnloFVPp1L60['value'],Mlo60)*100.),         pn(dn(MnloFVPp1L60['value'],Mlo60)*100.)],
["\sigma^{(1)}_x",    pn(MnloXPHm1['value']),     pn(MnloXPHp1),              pn(dn(MnloXPHm1['value'],Mlo00)*100.),            pn(dn(MnloXPHp1,Mlo00)*100.)],
["\sigma^{(1)}_x86",  pn(MnloYPHm1L86),           pn(MnloYPHp1L86['value']),  pn(dn(MnloYPHm1L86,Mlo86)*100.),                  pn(dn(MnloYPHp1L86['value'],Mlo86)*100.)],
["\sigma^{(1)}_x71",  pn(MnloYPHm1L71),           pn(MnloYPHp1L71['value']),  pn(dn(MnloYPHm1L71,Mlo71)*100.),                  pn(dn(MnloYPHp1L71['value'],Mlo71)*100.)],
["\sigma^{(1)}_x66",  pn(MnloYPHm1L66),           pn(MnloYPHp1L66['value']),  pn(dn(MnloYPHm1L66,Mlo66)*100.),                  pn(dn(MnloYPHp1L66['value'],Mlo66)*100.)],
["\sigma^{(1)}_x60",  pn(MnloYPHm1L60),           pn(MnloYPHp1L60['value']),  pn(dn(MnloYPHm1L60,Mlo60)*100.),                  pn(dn(MnloYPHp1L60['value'],Mlo60)*100.)],
["\sigma^{(1)}_p",    pn(MnloMPHm1['value']),     pn(MnloMPHm1['value']),     pn(dn(MnloMPHm1['value'],Mlo00)*100.),            pn(dn(MnloMPHm1['value'],Mlo00)*100.)],
SEPARATING_LINE,
["\sigma^{(2)}_m",    pn(MnnloEPHm1['value']),    pn(MnnloEPHm1['value']),    pn(dn(MnnloEPHm1['value'],Mnlom1)*100.),          pn(dn(MnnloEPHm1['value'],Mnlop1)*100.)],
["\sigma^{(2)}_mF",   pn(MnnloEVPm1['value']),    pn(MnnloEVPm1['value']),    pn(dn(MnnloEVPm1['value'],Mnlom1)*100.),          pn(dn(MnnloEVPm1['value'],Mnlop1)*100.)],
["\sigma^{(2)}_m71",  pn(MnnloFPHp1L71['value']), pn(MnnloFPHp1L71['value']), pn(dn(MnnloFPHp1L71['value'],Mnlom171)*100.),     pn(dn(MnnloFPHp1L71['value'],Mnlop171)*100.)],
["\sigma^{(2)}_mF71", pn(MnnloFVPp1L71['value']), pn(MnnloFVPp1L71['value']), pn(dn(MnnloFVPp1L71['value'],Mnlom171)*100.),     pn(dn(MnnloFVPp1L71['value'],Mnlop171)*100.)],
["\sigma^{(2)}_x",    pn(MnnloYPHm1),             pn(MnnloYPHp1),             pn(dn(MnnloYPHm1,Mnlom1)*100.),                   pn(dn(MnnloYPHp1,Mnlop1)*100.)],
["\sigma^{(2)}_xF",   pn(MnnloXVPm1['value']),    pn(MnnloXVPp1),             pn(dn(MnnloXVPm1['value'],Mnlom1)*100.),          pn(dn(MnnloXVPp1,Mnlop1)*100.)],
["\sigma^{(2)}_p",    pn(MnnloMPHm1['value']),    pn(MnnloMPHm1['value']),    pn(dn(MnnloMPHm1['value'],Mnlom1)*100.),          pn(dn(MnnloMPHm1['value'],Mnlop1)*100.)],
["\sigma^{(2)}_pF",   pn(MnnloMVPm1['value']),    pn(MnnloMVPm1['value']),    pn(dn(MnnloMVPm1['value'],Mnlom1)*100.),          pn(dn(MnnloMVPm1['value'],Mnlop1)*100.)],
SEPARATING_LINE
]

print(tabulate(tabM1, headers=["MUSE_m 210 [1]","m+p", "m-p", "dK+%", "dK-%"]))

##########################################################################}}}
### Total cross section -- Bar plots{{{
xorder = [
    '$\sigma_0^\infty$', '$\sigma_0^{71}$',
    '$\sigma^{(1)\infty}_\mu$','$\sigma^{(1)71}_\mu$','$\sigma^{(1)\infty}_x$','$\sigma^{(1)71}_x$','$\sigma^{(1)\infty}_p$',
    '$\sigma^{(2)\infty}_\mu$','$\sigma^{(2)71}_\mu$','$\sigma^{(2)\infty}_x$','$\sigma^{(2)\infty}_p$'
]

# $\bullet$ S0
fig, (ax1, ax2, ax3) = subplots(
    3, 1,
    sharex=True,
    gridspec_kw={'hspace': 0.03, 'height_ratios':[.5,.7,1]}
)

countsPH = [
    Mlo0['value'][0], Mlo071['value'][0],
    MnloEPHm0['value'][0],MnloFPHp0L71['value'][0], MnloXPHp0[0],MnloYPHp0L71['value'][0], MnloMPHm0['value'][0],
    MnnloEPHm0['value'][0],MnnloFPHp0L71['value'][0], MnnloYPHp0[0], MnnloMPHm0['value'][0]
]
countsVP = [
    0., 0.,
    MnloEVPm0['value'][0],MnloFVPp0L71['value'][0], 0., 0., 0.,
    MnnloEVPm0['value'][0],MnnloFVPp0L71['value'][0], MnnloXVPp0[0], MnnloMVPm0['value'][0]
]
errorFF = [
    0., abs(Mlo086['value'][0]-Mlo060['value'][0]),
    0., np.sqrt((MnloFPHp0L86['value'][0]-MnloFPHp0L60['value'][0])**2+(MnloFVPp0L86['value'][0]-MnloFVPp0L60['value'][0])**2),
    0., abs(MnloYPHp0L86['value'][0]-MnloYPHp0L60['value'][0]), 0.,
    0.,0.,0.,0.
]

stack = cumulate(countsPH, countsVP, errorFF)
barplot(xorder, countsPH, countsVP, stack, errorFF)
ax1.set_title('$\mu^-p \quad/ {\\rm\\upmu b}\quad [{\\tt S0}]$')
ax1.set_ylim(38., 53.)
ax2.set_ylim(.59, .81)
ax3.set_ylim(-0.1, 0.25)
draw()
mulify(fig, delx=3.8, dely=1.8)
fig.savefig("plots/xsec-mp-0.pdf")

# $\bullet$ S1
fig, (ax1, ax2, ax3) = subplots(
    3, 1,
    sharex=True,
    gridspec_kw={'hspace': 0.03, 'height_ratios':[.5,.7,1]}
)

countsPH = [
    Mlo0['value'][0], Mlo071['value'][0],
    MnloEPHm1['value'][0],MnloFPHp1L71['value'][0], MnloXPHp1[0],MnloYPHp1L71['value'][0], MnloMPHm1['value'][0],
    MnnloEPHm1['value'][0],MnnloFPHp1L71['value'][0], MnnloYPHp1[0], MnnloMPHm1['value'][0]
]
countsVP = [
    0., 0.,
    MnloEVPm1['value'][0],MnloFVPp1L71['value'][0], 0., 0., 0.,
    MnnloEVPm1['value'][0],MnnloFVPp1L71['value'][0], MnnloXVPp1[0], MnnloMVPm1['value'][0]
]
errorFF = [
    0., abs(Mlo086['value'][0]-Mlo060['value'][0]),
    0., np.sqrt((MnloFPHp1L86['value'][0]-MnloFPHp1L60['value'][0])**2+(MnloFVPp1L86['value'][0]-MnloFVPp1L60['value'][0])**2),
    0., abs(MnloYPHp1L86['value'][0]-MnloYPHp1L60['value'][0]), 0.,
    0.,0.,0.,0.
]

stack = cumulate(countsPH, countsVP, errorFF)
barplot(xorder, countsPH, countsVP, stack, errorFF)
ax1.set_title('$\mu^-p \quad/ {\\rm\\upmu b}\quad [{\\tt S1}]$')
ax1.set_ylim(38., 53.)
ax2.set_ylim(.59, .81)
ax3.set_ylim(-0.1, 0.25)
draw()
mulify(fig, delx=3.8, dely=1.8)
fig.savefig("plots/xsec-mp-1.pdf")
###########################################################################}}}
### Differential distributions $-$ $Q^2${{{
# $\bullet$ S0
fig, axs = plt.subplots(
    1, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios':[1]},
)
sca(axs)
mmsc=5

ebandr0ls(MnnloEPHm0,MnnloX31PHm0,MnnloX22PHm0,MnnloX13PHm0,MnnloMPHm0,MnnloEVPm0,MnnloXVPm0,MnnloMVPm0,
          MnloFPHp0L71,MnloFVPp0L71,MnloYPHp0L71,MnloMPHm0,
          Mlo071, mmsc, col='blue',linestyle='--')
ebandr0ps(MnnloEPHm0,MnnloX31PHm0,MnnloX22PHm0,MnnloX13PHm0,MnnloMPHm0,MnnloEVPm0,MnnloXVPm0,MnnloMVPm0,
          MnloFPHp0L71,MnloFVPp0L71,MnloYPHp0L71,MnloMPHm0,
          Mlo071, mmsc, col='red',linestyle='--')
ebandr0ls(MnnloEPHm1,MnnloX31PHm1,MnnloX22PHm1,MnnloX13PHm1,MnnloMPHm1,MnnloEVPm1,MnnloXVPm1,MnnloMVPm1,
          MnloFPHp1L71,MnloFVPp1L71,MnloYPHp1L71,MnloMPHm1,
          Mlo071, mmsc, col='cyan',linestyle=':')
ebandr0ps(MnnloEPHm1,MnnloX31PHm1,MnnloX22PHm1,MnnloX13PHm1,MnnloMPHm1,MnnloEVPm1,MnnloXVPm1,MnnloMVPm1,
          MnloFPHp1L71,MnloFVPp1L71,MnloYPHp1L71,MnloMPHm1,
          Mlo071, mmsc, col='orange',linestyle=':')

axs.set_title('$\mu^-p \quad [{\\tt S0, S1}]$')
axs.legend([
    r'$[{\tt S0}]\,\, (Q^2_\mu)$',
    r'$[{\tt S0}]\,\, (Q^2_p)$',
    r'$[{\tt S1}]\,\, (Q^2_\mu)$',
    r'$[{\tt S1}]\,\, (Q^2_p)$',
], ncol=2, loc='lower center',framealpha=0)

xlabel(r'$Q^2_\mu\,{\rm or}\,Q^2_p\,/\,{\rm GeV}^2$')
ylabel(r'$\frac{\sigma^{(1)\,71}+\sigma^{(2)\infty}}{\sigma_0^{71}}$')
xlim(8.e-3, 8.e-2)
draw()
fig.tight_layout()
mulify(fig, delx=4.2, dely=-0.1)
fig.savefig("plots/qsql-m-S0S1-2-diff.pdf", bbox_inches='tight')
###########################################################################}}}
### Differential distributions $-$ $\theta_\mu${{{
# $\bullet$ S0

fig, axs = subplots(
    1,
    sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios':[1]},
    figsize=(5.9, 6.)
)
sca(axs)

mmsc=5
MnloXPHp0 = scaleset(MnloXPHm0,-1)
eband(Mlo0,mmsc,col='slategray')
eband(addsets([MnloEPHm0, MnloEVPm0]),mmsc,col='gold')
eband(MnloXPHp0,mmsc,col='green')
eband(diffset(Mlo0, Mlo071),mmsc,col='tab:red')
ebanda(MnnloEPHm0,mmsc,linestyle='--',col='cyan')
ebanda(MnnloEVPm0,mmsc,linestyle=':',col='darkcyan')
eband(diffset(MnnloX22PHm0, addsets([MnnloX31PHm0, MnnloX13PHm0])),mmsc)
eband(diffset(MnloXPHp0, MnloYPHp0L71),mmsc, col='pink')

axs.set_title('$\mu^-p \quad [{\\tt S0}]$')
axs.legend([
    r'$\sigma_0^\infty$', r'$\sigma^{(1)\infty}_\mu+\sigma^{(1)\infty}_\Pi$', r'$\sigma^{(1)\infty}_x$', r'$\sigma_0^\infty-\sigma_0^{71}$',
    r'$|\sigma^{(2)\infty}_\mu|$', r'$\sigma^{(2)\infty}_{\mu\Pi}$', r'$\sigma^{(2)\infty}_x$', r'$\sigma^{(1)\infty}_x-\sigma^{(1)71}_x$'
], ncol=2, loc='upper right',framealpha=0)

xlabel(r'$\theta_\mu\,/\,{\rm deg}$')
ylabel('$\\D\\sigma/\\D\\theta_\\mu\ /\ {\\rm\\upmu b}$')
yscale('log')
ylim(3.e-6, 9.e1)
draw()
fig.tight_layout()
mulify(fig, delx=0.3, dely=3.4)
fig.savefig("plots/thm-S0-diff.pdf")

# $\bullet$ S0 ($\mu^-$ and $\mu^+$)

fig, axs = subplots(
    1,
    sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios':[1]},
    figsize=(5.9, 6.)
)
sca(axs)

mmsc=5
MnloXPHp0 = scaleset(MnloXPHm0,-1)
ebandp(MnloYPHp0L71,mmsc)
ebandp(addsets([MnnloX31PHm0, MnnloX13PHm0, MnnloXVPm0]),mmsc)

axs.set_title('$\mu^-p - \mu^+p \quad [{\\tt S0}]$')
axs.legend([
    r'$\sigma^{(1)\,71}_x$',
    r'$\sigma^{(2)\infty}_x+\sigma^{(2)\infty}_{x\Pi}$'
], ncol=1, loc='upper right',framealpha=0)

xlabel(r'$\theta_\mu\,/\,{\rm deg}$')
ylabel('$\\D\\sigma/\\D\\theta_\mu\ /\ {\\rm\\upmu b}$')
yscale('log')
ylim(3.e-6, 2.e-1)
draw()
fig.tight_layout()
mulify(fig, delx=0.3, dely=3.4)
fig.savefig("plots/thm-S0-diff-x.pdf")

# $\bullet$ S1

fig, axs = subplots(
    1,
    sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios':[1]},
    figsize=(5.9, 6.)
)
sca(axs)

mmsc=5
MnloXPHp1 = scaleset(MnloXPHm1,-1)
eband(Mlo0,mmsc,col='slategray')
eband(addsets([MnloEPHm1, MnloEVPm1]),mmsc,col='gold')
eband(MnloXPHp1,mmsc,col='green')
eband(diffset(Mlo0, Mlo071),mmsc,col='tab:red')
ebanda(MnnloEPHm1,mmsc,linestyle='--',col='cyan')
ebanda(MnnloEVPm1,mmsc,linestyle=':',col='darkcyan')
eband(diffset(MnnloX22PHm1, addsets([MnnloX31PHm1, MnnloX13PHm1])),mmsc)
eband(diffset(MnloXPHp1, MnloYPHp1L71),mmsc, col='pink')

axs.set_title('$\mu^-p \quad [{\\tt S1}]$')
axs.legend([
    r'$\sigma_0^\infty$', r'$\sigma^{(1)\infty}_\mu+\sigma^{(1)\infty}_\Pi$', r'$\sigma^{(1)\infty}_x$', r'$\sigma_0^\infty-\sigma_0^{71}$',
    r'$|\sigma^{(2)\infty}_\mu|$', r'$\sigma^{(2)\infty}_{\mu\Pi}$', r'$\sigma^{(2)\infty}_x$', r'$\sigma^{(1)\infty}_x-\sigma^{(1)71}_x$'
], ncol=2, loc='upper right',framealpha=0)

xlabel(r'$\theta_\mu\,/\,{\rm deg}$')
ylabel('$\\D\\sigma/\\D\\theta_\\mu\ /\ {\\rm\\upmu b}$')
yscale('log')
ylim(3.e-6, 9.e1)
draw()
fig.tight_layout()
mulify(fig, delx=0.3, dely=3.4)
fig.savefig("plots/thm-S1-diff.pdf")

# $\bullet$ S1 ($\mu^-$ and $\mu^+$)

fig, axs = subplots(
    1,
    sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios':[1]},
    figsize=(5.9, 6.)
)
sca(axs)

mmsc=5
MnloXPHp1 = scaleset(MnloXPHm1,-1)
ebandp(MnloYPHp1L71,mmsc)
ebandp(addsets([MnnloX31PHm1, MnnloX13PHm1, MnnloXVPm1]),mmsc)

axs.set_title('$\mu^-p - \mu^+p \quad [{\\tt S1}]$')
axs.legend([
    r'$\sigma^{(1)\,71}_x$',
    r'$\sigma^{(2)\infty}_x+\sigma^{(2)\infty}_{x\Pi}$'
], ncol=1, loc='upper right',framealpha=0)

xlabel(r'$\theta_\mu\,/\,{\rm deg}$')
ylabel('$\\D\\sigma/\\D\\theta_\\mu\ /\ {\\rm\\upmu b}$')
yscale('log')
ylim(3.e-6, 2.e-1)
draw()
fig.tight_layout()
mulify(fig, delx=0.3, dely=3.4)
fig.savefig("plots/thm-S1-diff-x.pdf")
##########################################################################}}}
