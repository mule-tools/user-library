# lepton proton scatter $`\mu p\to \mu p`$

References: [[2007.01654]](https://arxiv.org/abs/2007.01654)

Configurations implemented
 * MESA $`ep\to ep`$, P2 experiment $`p_\text{in} = 155\,{\rm MeV}`$ with $`E_e > 45\,{\rm MeV}`$
   and $`25^\circ < \theta < 45^\circ`$
 * MUSE, $`ep\to ep`$ at $`p_\text{in} = 115\,{\rm MeV}`$
   [[ref]](https://gitlab.com/mule-tools/user-library/-/jobs/artifacts/master/file/ci/l-p-scattering/muse/muse.ipynb?job=run)
 * MUSE, $`\mu p\to \mu p`$ at $`p_\text{in} = 210\,{\rm MeV}`$
   [[ref]](https://gitlab.com/mule-tools/user-library/-/jobs/artifacts/master/file/ci/l-p-scattering/legacy/legacy.ipynb?job=run)
