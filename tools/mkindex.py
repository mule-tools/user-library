import nbconvert
import nbformat
import nbformat.v4
import os
import re
from copy import deepcopy


def build_index(title, children=[], description="", path=[]):
    nb = nbformat.v4.new_notebook()

    nb.cells.append(nbformat.v4.new_markdown_cell("# "+ title))

    if description:
        nb.cells.append(nbformat.v4.new_markdown_cell(description))

    def add_children(l, indent=0):
        for item in l:
            if isinstance(item, list):
                yield ''.join(add_children(item, indent+1))
            else:
                yield ('  ' * indent) + item

    nb.cells.append(nbformat.v4.new_markdown_cell(''.join(
        add_children(children)
    )))

    nb.metadata['py2nb_path'] = path
    nb.metadata['title'] = title
    nb.metadata['offset'] = 2

    ex = nbconvert.HTMLExporter(template_file='tools/template.tpl')
    html, _ = ex.from_notebook_node(nb)
    return html


def checkdir(dir, urlroot, pageroot):
    files = os.listdir(dir)

    if not 'makefile' in files:
        raise KeyError("No makefile found in " + dir)

    with open(os.path.join(dir, 'makefile')) as fp:
        makefile = fp.read()

        path = re.findall('path=\'([^\']*)\'', makefile)[0]
        path = path.replace('$$','$').split(':')

        group = re.findall('group=(.*)', makefile)[0]
        name = re.findall('name=(.*)', makefile)[0]

    itemised = []

    for file in files:
        if not file.endswith('nb'):
            continue

        with open(os.path.join(dir, file)) as fp:
            nb = nbformat.read(fp, 4)

        if path != nb.metadata['py2nb_path']:
            raise ValueError("Paths don't match")

        itemised.append(" * [%s](%s/%s/%s)\n\n" % (
            nb.metadata['title'],
            urlroot, dir, file.replace('ipynb', 'html')
        ))

    with open(os.path.join(pageroot, dir, 'index.html'), 'w') as fp:
        fp.write(build_index(path[-1], children=itemised, path=path[:-1]))

    return path, [
        " * [%s](%s/%s)\n\n" % (
            path[-1],
            urlroot, dir
        ),
        itemised
    ]


def main(basedir='./', urlroot='', pageroot='public'):
    with open(os.path.join(basedir, 'makefile')) as fp:
        folders = []
        reading = False
        for i in fp.readlines():
            if i.startswith('FOLDERS='):
                reading = True
            if reading:
                new = re.findall('(?:FOLDERS=)?\W*([\w\-/]+)', i)
                if len(new) == 0:
                    break
                folders += new

    paths = {}
    for i in folders:
        humanpath, children = checkdir(i, urlroot, pageroot)
        folderpath = i.split('/')

        if len(humanpath) != len(folderpath):
            raise ValueError("Length of paths doesn't match")


        for n in range(len(humanpath)):
            if tuple(folderpath[:n+1]) in paths:
                assert paths[tuple(folderpath[:n+1])][0] == humanpath[:n+1]
                paths[tuple(folderpath[:n+1])][1] += deepcopy(children)
            else:
                paths[tuple(folderpath[:n+1])] = [
                    humanpath[:n+1],
                    deepcopy(children)
                ]

    rootpage = []
    for folderpath, (humanpath, children) in paths.items():
        with open(os.path.join(pageroot,*folderpath,'index.html'), 'w') as fp:
            fp.write(
                build_index(
                    humanpath[-1],
                    children,
                    path=humanpath[:-1]
                )
            )
        if len(folderpath) == 1:
            rootpage.append([' * [%s](%s/%s)\n' % (
                humanpath[-1],
                urlroot,
                os.path.join(*folderpath)
            ), children])

    with open(os.path.join(pageroot,'index.html'), 'w') as fp:
        fp.write(
            build_index(
                "McMule User Library",
                rootpage
            )
        )


if __name__ == "__main__":
    main(urlroot=os.environ.get('CI_PAGES_URL','') + '/')
