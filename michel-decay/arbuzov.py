import requests
import zlib
import numpy as np
import tarfile
import pymule.maths
import sys
import json
import re
import bz2
import os
try:
    import StringIO
    import sympy as sp
    import sympy.parsing.latex
    import scipy.special
    Printer = sys.modules['sympy.printing.pycode'].NumPyPrinter
    x, xe, beta, z = sp.var('x xe beta z')
except ImportError:
    pass

sys.dont_write_bytecode = True
dilog = pymule.maths.Li2
trilog = pymule.maths.Li3
sqrt = np.sqrt


def zeta(n):
    v = [
        -0.5,
        np.inf,
        pymule.maths.zeta2,
        pymule.maths.zeta3
    ]
    return v[int(n)]


def Sot(z):
    return (
        0.5*np.log(1-z)**2*np.log(z) + np.log(1-z)*dilog(1-z)
        - trilog(1-z) + pymule.maths.zeta3
    )


def Psi(x):
    return (
        3*trilog(1-x) - 2*Sot(1-x) + np.log(1-x)**3 - np.log(x)**3/6.
        + 3*np.log(x)**2*np.log(1-x)/2. - 3*np.log(x)*np.log(1-x)**2
        - 3*dilog(1-x)*np.log(1-x) + 2*pymule.maths.zeta3
        - 3*pymule.maths.zeta2 * np.log((1-x) / x)
    )


def Phi(z):
    return dilog(-(1-z)/z) + np.log((1-z)/z)**2-np.pi**2/6.


def parse(lines, s):
    return sympy.parsing.latex.parse_latex(
        ''.join(lines)
        .replace("f^{\\mathrm{Born}}(x)", "f*")
        .replace("g^{\\mathrm{Born}}(x)", "g*")
        .replace("\\frac{q^2}{m_\\mu^2}", "\\qq")
        .replace("\\nonumber", "")
        .replace("\\biggr", "")
        .replace("\\biggl", "")
        .replace("\\bigl", "")
        .replace("\\bigr", "")
        .replace("\\left", "")
        .replace("\\right", "")
        .replace("\\\\", "")
        .replace("&", "")
        .replace("\\{", "(")
        .replace('\\}', ")")
        .replace('\\Sot{x}', "\\Sot(x)")
        .replace('\\Sot{1-x}', "\\Sot(1-x)")
        .replace('\\Li_2', "\\dilog")
        .replace('\\Li_3', "\\trilog")
        .replace('\\Li_3', "\\trilog")
        .replace('\\zeta_2', "\\zeta(2)")
        .replace('\\zeta_3', "\\zeta(3)")
        .replace('\\Li{2}{1-x}', "\\dilog(1-x)")
        .replace('\\Li{3}{1-x}', "\\trilog(1-x)")
        .replace(
            r'\Li{2}{\frac{(1-\beta)(2-x(1+\beta))}{(1+\beta)(2-x(1-\beta))}}',
            r'\dilog(\frac{(1-\beta)(2-x(1+\beta))}{(1+\beta)(2-x(1-\beta))})'
        ).replace(
            r'\Li{2}{\frac{2-x(1+\beta)}{2-x(1-\beta)}}',
            r'\dilog(\frac{2-x(1+\beta)}{2-x(1-\beta)})'
        ).replace('x(', 'x*(')
        .replace('L (', 'L*(')
        .replace('beta(', 'beta*(')[s:-1]).subs('x', 'xe')


def load0110047():
    """
    This loads hep-ph/0110047, i.e. the NLO paper
    """
    r = requests.get('https://arxiv.org/e-print/hep-ph/0110047')
    tex = zlib.decompress(r.content, 16+zlib.MAX_WBITS)

    flo = 3-2*xe+xe/4*(3*xe-4)*(1-beta**2)
    glo = (1-2*xe)*beta+3*xe**2/4*(1-beta**2)*beta

    fnlo = parse(tex.splitlines()[231:262], 9)
    Adef = parse(tex.splitlines()[265:276], 3)
    gnlo = parse(tex.splitlines()[279:326], 9)

    subs = {
        'f': flo, 'g': glo, 'A': Adef,
        'beta': sp.sqrt(1 - 4*z**2/xe**2),
        'L': -2*sp.log(z), 'qq': (1-xe)+z**2
    }

    fnlo = fnlo.subs(subs).subs(subs)
    gnlo = gnlo.subs(subs).subs(subs)

    return fnlo, gnlo


def load0202102():
    """
    This loads hep-ph/0202102, i.e. the LL paper
    """
    def dosub(ex):
        return ex.subs('z', 'xe/(1+m**2/M**2)').subs('m', 'z*M')

    r = requests.get('https://arxiv.org/e-print/hep-ph/0202102')
    fp = StringIO.StringIO(r.content)
    tar = tarfile.open(fileobj=fp)
    tex = tar.extractfile('mu_lla_2.tex').read()

    f1LL = dosub(parse(tex.replace("1-z\\over z", "(1-z)/z")
                          .splitlines()[291:293], 1))
    g1LL = dosub(parse(tex.replace("1-z\\over z", "(1-z)/z")
                          .splitlines()[295:297], 1))

    fLL = dosub(parse(tex.splitlines()[322:327], 1))
    gLL = dosub(parse(tex.splitlines()[330:335], 1))

    fLLns = f1LL
    gLLns = g1LL

    fLLs = dosub(parse(tex.splitlines()[391:394], 1))
    gLLs = dosub(parse(tex.splitlines()[396:399], 1))

    return (fLL, gLL), (fLLns, gLLns), (fLLs, gLLs)


def load0205172():
    """
    This loads hep-ph/0205172, i.e. the NLL paper for f. It also
    returns the LL of f
    """
    r = requests.get('https://arxiv.org/e-print/hep-ph/0205172')
    fp = StringIO.StringIO(r.content)
    tar = tarfile.open(fileobj=fp)
    tex = tar.extractfile('mu_ff_k4.tex').read()

    fLL = parse(tex.splitlines()[542:550], 21)
    fNLL = parse(tex.replace("\\ln^2x \\biggl(", "\\ln(x)^2 * (")
                    .replace('\\ln x \n\\biggl(', '\\ln(x)*\n(')
                    .splitlines()[557:577], 0)

    fLLns = parse(tex.splitlines()[598:600], 20)
    fNLLns = parse(tex.replace('\\ln x \\biggl(', '\\ln (x)* \\biggl(')
                      .splitlines()[602:610], 0)
    fLLs = parse(tex.splitlines()[625:628], 0)
    fNLLs = parse(tex.replace("\\ln x\\biggl(", "\\ln(x)\\biggl(")
                     .replace("\\ln^2x \\biggr(", "\\ln(x)^2* \\biggr(")
                     .splitlines()[630:641], 0)
    fNLLint = parse(tex.replace("\\ln^2x \\biggl(", "\\ln(x)^2 * (")
                       .replace('\\ln x \\biggl(', '\\ln(x)*(')
                       .splitlines()[655:662], 0)

    return (fLL, fNLL), (fLLns, fNLLns), (fLLs, fNLLs), fNLLint


def load0206036():
    """
    This loads hep-ph/0206036. This means it returns gNLL as well as the
    N3LO-LL results for f and g
    """
    r = requests.get('https://arxiv.org/e-print/hep-ph/0206036')
    fp = StringIO.StringIO(r.content)
    tar = tarfile.open(fileobj=fp)
    tex = tar.extractfile('mu_jhep.tex').read()

    gNLL = parse(tex.splitlines()[678:697], 21)
    gNLLns = parse(tex.splitlines()[699:707], 0)
    gNLLs = parse(tex.splitlines()[709:720], 0)
    gNLLint = parse(tex.splitlines()[722:729], 0)

    f3LL = parse(tex.splitlines()[746:759], 23)
    g3LL = parse(tex.splitlines()[760:773], 23)

    f3LLns = parse(tex.splitlines()[783:790], 18)
    g3LLns = parse(tex.splitlines()[792:797], 0)

    f3LLs = parse(tex.splitlines()[798:808], 18)
    g3LLs = parse(tex.splitlines()[809:820], 18)

    return (gNLL, gNLLns, gNLLs, gNLLint), (f3LL, g3LL), \
           (f3LLns, g3LLns), (f3LLs, g3LLs)


def tostr(args, expr):
    printer = Printer()
    block = printer.doprint(expr).splitlines()[-1]
    block = re.sub("([^\*]\d+)", r"\1.", block)
    block = block.replace("numpy", "np")
    return "lambda %s: (%s)" % (
        ",".join(args), block
    )


def loadall(f='arbuzov.json.bz2'):
    fnlo, gnlo = load0110047()
    (fLL, gLL), (fLLns, gLLns), (fLLs, gLLs) = load0202102()
    (fLLb, fNLL), (fLLnsb, fNLLns), (fLLsb, fNLLs), fNLLint = load0205172()
    (gNLL, gNLLns, gNLLs, gNLLint), (f3LL, g3LL), (f3LLns, g3LLns), (f3LLs, g3LLs) = load0206036()  # nopep8

    b = {
        "fnlo": tostr(["xe", "z"], fnlo),
        "gnlo": tostr(["xe", "z"], gnlo),

        "f2LL": tostr(["xe"], fLL.subs(z, 0)),
        "f2LLns": tostr(["xe"], fLLns.subs(z, 0)),
        "f2LLs": tostr(["xe"], fLLs.subs(z, 0)),

        "g2LL": tostr(["xe"], gLL.subs(z, 0)),
        "g2LLns": tostr(["xe"], gLLns.subs(z, 0)),
        "g2LLs": tostr(["xe"], gLLs.subs(z, 0)),

        "f2NLL": tostr(["xe"], fNLL),
        "f2NLLns": tostr(["xe"], fNLLns),
        "f2NLLs": tostr(["xe"], fNLLs),
        "f2NLLint": tostr(["xe"], fNLLint),

        "g2NLL": tostr(["xe"], gNLL),
        "g2NLLns": tostr(["xe"], gNLLns),
        "g2NLLs": tostr(["xe"], gNLLs),
        "g2NLLint": tostr(["xe"], gNLLint),

        "f3LL": tostr(["xe"], f3LL),
        "f3LLns": tostr(["xe"], f3LLns),
        "f3LLs": tostr(["xe"], f3LLs),

        "g3LL": tostr(["xe"], g3LL),
        "g3LLns": tostr(["xe"], g3LLns),
        "g3LLs": tostr(["xe"], g3LLs)
    }

    with bz2.BZ2File(f, 'w') as fp:
        json.dump(b, fp)


def loadfile(f='arbuzov.json.bz2'):
    with bz2.BZ2File(f) as fp:
        b = json.load(fp)
    for key, value in b.items():
        globals()[key] = eval(value)


if __name__ == "__main__":
    loadall()
else:
    dir_path = os.path.dirname(os.path.realpath(__file__))
    loadfile(dir_path + '/arbuzov.json.bz2')


# print "{" + ",\n".join(
#     str(i) for i in [fLL2, fNLL, fLLns, fNLLns, fLLs, fNLLs, fNLLint]
# ) + "}"
