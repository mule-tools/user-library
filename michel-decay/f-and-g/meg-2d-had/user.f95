                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions
  use user_dummy, only: pass_cut, userweight, names, filenamesuffix

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!
!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 5
  integer, parameter :: nr_bins = 1000
  integer :: whatplot
  real, parameter ::                                                  &
     min_val(nr_q) = (/ 0., 26., 42., 50., -1. /)
  real, parameter ::                                                  &
     max_val(nr_q) = (/ 26., 42., 50., 54., +1. /)
  integer :: userdim = 0

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!
!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer, parameter :: namesLen=6
  integer, parameter :: filenamesuffixLen=10


!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

  musq = Mm**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER

  integer :: p_encrypted

  read*, p_encrypted
  write(filenamesuffix,'(I2)') p_encrypted

  ! 1st digit is cos sign (1 = -, 2 = +)
  ! 2nd digit is the energy range

  whatplot = mod(p_encrypted,10)
  
  if(p_encrypted/10 == 1) then
    whatplot = - whatplot
  endif

  Nel = 0
  Nmu = 0
  Ntau = 0
  Nhad = 1

  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: quant(nr_q),ez(4)

  !! ==== keep the line below in any case ==== !!

  pass_cut = .true.
  call fix_mu

! Polarization vector

  pol1 = (/ 0._prec, 0._prec, 0.85_prec, 0._prec /)
  ez = (/ 0._prec,0._prec, 1._prec,0._prec /)

! Energy range selection

  if(abs(whatplot) == 1) then
     if(q2(4) > 26.) pass_cut = .false.
   elseif(abs(whatplot) == 2) then
     if(q2(4) < 26. .or. q2(4) > 42.) pass_cut = .false.
   elseif(abs(whatplot) == 3) then
     if(q2(4) < 42. .or. q2(4) > 50.) pass_cut = .false.
   elseif(abs(whatplot) == 4) then
     if(q2(4) < 50.) pass_cut = .false.
   endif
   if (whatplot > 0) then
     ! We are in the cos > 0 region
     if (cos_th(q2,ez) < 0) pass_cut = .false.
   else
     ! We are in the cos < 0 region
     if (cos_th(q2,ez) > 0) pass_cut = .false.
   endif
  
! Observables

  names(1) = "Ee1"
  quant(1) = q2(4)
  names(2) = "Ee2"
  quant(2) = q2(4)
  names(3) = "Ee3"
  quant(3) = q2(4)
  names(4) = "Ee4"
  quant(4) = q2(4)
  names(5) = "cthe"
  quant(5) = cos_th(q2,ez)


  END FUNCTION QUANT

  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE USEREVENT


                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
