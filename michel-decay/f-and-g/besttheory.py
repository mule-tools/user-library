import numpy as np
F = np.loadtxt('Fbest.csv')
G = np.loadtxt('Gbest.csv')

def get_best(what, includeOL=True, merge=1):
    """
    get_best(what, ...) returns the theory prediction for F
    or G. what needs to be either 'F' or 'G'.  The following
    options exist
      * includeOL: where to include open lepton
        contributions [default: True]
      * merge: how many bins to merge [default: 1, i.e.
        none]
    """
    if what == 'F':
        best = F
    elif what == 'G':
        best = G
    else:
        raise KeyError("Only 'F' or 'G' are allowed")
    assert best.shape == (3688, 6)

    ans = np.zeros((3688, 4))
    ans[:,0] = best[:,0] # x
    ans[:,1] = best[:,1] # y
    ans[:,2] = best[:,3] # stat error
    ans[:,3] = best[:,4] # sys error
    if includeOL:
        ans[:,1] += best[:,2]
        ans[:,3] = np.sqrt(best[:,4]**2 + best[:,5]**2) # sys error

    if merge > 1:
        # Make sure the length fits
        if len(ans) % merge:
            short = ans[:-(len(ans) % merge)]
        else:
            short = ans[:]
        part = np.split(short, len(short)/merge)
        ans = np.array([
            (
                sum(i[:,0])/merge,
                sum(i[:,1])/merge,
                np.sqrt(sum(i[:,2]**2))/merge,
                sum(i[:,3])/merge
            ) for i in part
        ])

    return np.column_stack((
        ans[:,0], ans[:,1], np.sqrt(ans[:,2]**2 + ans[:,3]**2)
    ))
