


                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 10
  integer, parameter :: nr_bins = 100
  real, parameter :: &
     min_val(nr_q) = (/ 3., 3., 3., 3., 3., 3., 3., 3., 3., 3. /)
  real, parameter :: &
     max_val(nr_q) = (/ 53., 53., 53., 53., 53.,53., 53., 53., 53., 53. /)

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  logical ::  pass_cut(nr_q)
  character (len = 6), dimension(nr_q) :: names
  character(len=10) :: filenamesuffix


!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;
  real (kind=prec) :: coscut, ecut

  contains




  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

!  mu = Mm
  mu = Mmu    !! ONLY for muonic decays !!!

  musq = mu**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  implicit none
  integer:: icut
  icut = 0
!  read*, icut
!  select case(icut)
!    case(0)
!      coscut = -4.
!      ecut = 10.e8
!    case(1)
!      coscut = 2.90
!      ecut = 500.
!    case(2)
!      coscut = 2.93
!      ecut = 300.
!  end select
  write(filenamesuffix,'(I1)') icut
!  print*, "coscut, ecut", coscut, ecut
  END SUBROUTINE




  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: ql(4), qs(4)
!  real (kind=prec) :: q1com(4), q2com(4), q5com(4), q6com(4), q7com(4) 
!  real (kind=prec) :: sqsBelle, ptau, taurf(4), sumcos
  integer :: c2p, c2m, c5p, c5m, c6p, c6m, clp, clm, csp, csm
  real (kind=prec) :: quant(nr_q)
  
!  nel = 0
  nmu = 0
  ntau = 0
  nhad = 0  

!  pol1 = (/ 0._prec, 0._prec, 0.85_prec, 0._prec /)
  pol1 = (/ 0._prec, 0._prec, 1._prec, 0._prec /)

  c2p = 0
  c5p = 0
  c6p = 0
  clp = 0
  csp = 0

  if(cos_th(q2,pol1)>0) c2p=1
  c2m = 1-c2p
  if(cos_th(q5,pol1)>0) c5p=1
  c5m = 1-c5p
  if(cos_th(q6,pol1)>0) c6p=1
  c6m = 1-c6p


  pass_cut = .true.
  call fix_mu

  if(q2(4) > q6(4)) then
    ql=q2; qs=q6; clm=c2m; clp=c2p; csm=c6m; csp=c6p
  else
    ql=q6; qs=q2; clm=c6m; clp=c6p; csm=c2m; csp=c2p
  endif


!!$  names(1) = "Ep2"
!!$  quant(1) = q2(4)
!!$
!!$  names(2) = "Ep6"
!!$  quant(2) = q6(4)
!!$
!!$  names(3) = "Em5"
!!$  quant(3) = q5(4)
!!$
!!$  names(4) = "Epl"
!!$  quant(4) = ql(4)
!!$
!!$  names(5) = "Eps"
!!$  quant(5) = qs(4)

  names(1) = "Ep2p"
  quant(1) = q2(4)*c2p

  names(2) = "Ep6p"
  quant(2) = q6(4)*c6p

  names(3) = "Em5p"
  quant(3) = q5(4)*c5p

  names(4) = "Eplp"
  quant(4) = ql(4)*clp

  names(5) = "Epsp"
  quant(5) = qs(4)*csp

  names(6) = "Ep2m"
  quant(6) = q2(4)*c2m

  names(7) = "Ep6m"
  quant(7) = q6(4)*c6m

  names(8) = "Em5m"
  quant(8) = q5(4)*c5m

  names(9) = "Eplm"
  quant(9) = ql(4)*clm

  names(10) = "Epsm"
  quant(10) = qs(4)*csm

  END FUNCTION QUANT






                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!



