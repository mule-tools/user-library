                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions
  use user_dummy, only: pass_cut, userweight, names, filenamesuffix

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 2
  integer, parameter :: nr_bins = 40
  real, parameter ::                                                  &
     min_val(nr_q) = (/ 0., 0. /)
  real, parameter ::                                                &
     max_val(nr_q) = (/ 1., 1. /)
  integer :: userdim = 0
  integer mc

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer, parameter :: namesLen=6
  integer, parameter :: filenamesuffixLen=10


!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU

  musq = mm**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  ! This is called without arguments once as soon as McMule
  ! starts and has read all other configuration, i.e. you can
  ! access which_piece and flavour. Use this to read any
  ! further information from the user (like cut configuration
  ! etc). You do not have to print the hashes - this is
  ! already taken care of - but you are very much invited to
  ! include information of what it is you are doing
  !
  ! If you are using the cut channel of the menu, you may need to set
  ! the filenamesuffix variable which is appended to the name of the
  ! VEGAS file.

  ! Example for reading a cut:
  ! integer cut
  ! read*,cut
  ! write(filenamesuffix,'(I2)') cut

  print*, "This calculates the muon decay in the configuration"
  print*, "(presumably) used by [hep-ph/0505069] and definitely"
  print*, "used by our paper."

  read*,mc
  write(filenamesuffix,'(I1)') mc

  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: peL(4),peS(4),peE(4)
  real (kind=prec) :: quant(nr_q)
  real (kind=prec) :: ez(4),elec(4),etot,mtot
  real (kind=prec) :: cthL, cthS, cthE, e_gg
  integer:: survive5, survive6

  !! ==== keep the line below in any case ==== !!
  call fix_mu

!  pol1 = (/ 0._prec, 0._prec, 0.85_prec, 0._prec /)
  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  pass_cut = .true.

  if (which_piece .eq. "m2ennee0") then
    names(1) = 'xh'
    names(2) = 'xs'
    if (q5(4).gt.q2(4)) then
      quant(1) = 2*q5(4) / Mm
      quant(2) = 2* q2(4) / Mm
    else
      quant(2) = 2*q5(4) / Mm
      quant(1) = 2* q2(4) / Mm
    endif
  else
    names(1) = 'xe'
    quant(1) = 2* q2(4) / Mm
    names(2) = 'xs'

    if (mc .eq. 1) then
        e_gg = 0._prec
        if(cos_th(q2,q5)>0.8_prec) e_gg = e_gg + q5(4)
        if(cos_th(q2,q6)>0.8_prec) e_gg = e_gg + q6(4)
        if(e_gg>10._prec) pass_cut = .false.
    endif
  endif

  END FUNCTION QUANT


  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE USEREVENT


                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
