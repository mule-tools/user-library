# vim: foldmethod=marker
# py2nb: title=Bhabha scattering validation
from pymule import *
import tabulate
## Settings{{{
SMALL_SIZE = 10
MEDIUM_SIZE = 12
BIGGER_SIZE = 14

rc('font', size=SMALL_SIZE)          # controls default text sizes
rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
rc('legend', fontsize=MEDIUM_SIZE)   # legend fontsize
rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
#####################################################################}}}
## Import results{{{
setup(folder='eb2eb_daphne/out.tar.bz2')
loConv=1000*conv*alpha**2
nloConv=1000*conv*alpha**3
nnloConv=1000*conv*alpha**4


lo = scaleset(mergefks(
        sigma('eb2eb0')
), loConv)


nlo = scaleset(mergefks(
        sigma('eb2ebF'),
        sigma('eb2ebR125'),
        sigma('eb2ebR35'),
        sigma('eb2ebR45')
), nloConv)


nnlo = scaleset(mergefks(
        sigma('eb2ebFF'),
        sigma('eb2ebRF125'),
        sigma('eb2ebRF35'),
        sigma('eb2ebRF45'),
        sigma('eb2ebRR15162526'),
        sigma('eb2ebRR3536'),
        sigma('eb2ebRR4546')
), nnloConv)
#####################################################################}}}
## Total cross section & $K$-factors{{{
sigma0 = lo['value']
sigma1 = plusnumbers([sigma0,nlo['value']])
sigma2 = plusnumbers([sigma1,nnlo['value']])


dk1=100*dividenumbers(nlo['value'], sigma0)
dk2=100*dividenumbers(nnlo['value'], sigma1)


print(tabulate.tabulate([
    ['LO'  , '+'+printnumber(  lo['value']/1000.), None],
    ['NLO' ,     printnumber( nlo['value']/1000.), printnumber(dk1)],
    ['NNLO', '+'+printnumber(nnlo['value']/1000.), '+ '+printnumber(dk2)]
], headers=['$\\sigma / ub$', '$\\delta K / \\%$']))
#####################################################################}}}
## Angular distribution{{{
# combine plots
dsdm0 = lo['th_e']
dsdm1 = addplots(dsdm0,nlo['th_e'])
dsdm2 = addplots(dsdm1,nnlo['th_e'])
# generate plot
fig, (ax1, ax2, ax3) = kplot(
    {
        'lo':   lo['th_e'],
        'nlo':  nlo['th_e'],
        'nnlo': nnlo['th_e'],
    },
    labelx="$\\theta_{-}\,/\,{\\rm deg}$",
    labelsigma="$\\D\\sigma/\\D\\theta_{-}\ /\ {\\rm\\upmu b}$",
    legend={
        'lo': '$\\sigma_{0}$',
        'nlo': '$\\sigma_{1}$',
        'nnlo': '$\\sigma_{2}$'
    },
    legendopts={'what': 'u', 'loc': 'upper right'}
)
ax1.set_yscale('log')
gcf().subplots_adjust(right=0.89)
fig.savefig('th_e.pdf')
#####################################################################}}}
## comparison with BabaYaga{{{
# BabaYaga results (from C.M. Carloni Calame):
loB       = np.array([6855.73536,0.00003])
nloBFull  = np.array([6060.042,0.007])
nnloBFull = np.array([6086.81,0.01])
nloB      = plusnumbers(nloBFull,-loB)
nnloB     = plusnumbers(nnloBFull,-nloBFull)
# BabaYaga is exact up to NLO. We therefore see agreement at LO and NLO:
chisq0=chisq([lo['value'],loB])
chisq1=chisq([nlo['value'],nloB])
# At NNLO BabaYaga yields a logarihtmic approximation. We therefore only
# expect approximate agreement.
dev2=100*(nnlo['value'][0]-nnloB[0])/nnloB[0]
# for the total cross section we find agreement belov 0.01%
sigma2=plusnumbers([lo['value'],nlo['value'],nnlo['value']])
sigma2B=plusnumbers([loB,nloB,nnloB])


devTot=100*(sigma2[0]-sigma2B[0])/sigma2B[0]


print(tabulate.tabulate([
    [  'LO', '+'+printnumber(  lo['value']/1000.), '+'+printnumber(  loB/1000.), None],
    [ 'NLO',     printnumber( nlo['value']/1000.),     printnumber( nloB/1000.), printnumber(dk1)],
    ['NNLO', '+'+printnumber(nnlo['value']/1000.), '+'+printnumber(nnloB/1000.), '+ '+printnumber(dk2)],
    ['sum' , '+'+printnumber(sigma2       /1000.), '+'+printnumber(nnloBFull/1000.)]
], headers=['', 'McMule', 'BABAYAGA', '$\\delta K / \\%$']))
#####################################################################}}}
