# vim: foldmethod=marker
# py2nb: title=Stability of the soft limit
from pymule import *
# The momentum configuration and other raw data for these tests can be found
# [here](https://gitlab.com/mule-tools/user-library/-/blob/master/bhabha-scattering/validation/eb2eb_daphne/softlimit_matels_rand.txt)
# and [here](https://gitlab.com/mule-tools/user-library/-/blob/master/bhabha-scattering/validation/eb2eb_daphne/softlimit_matels_collinitial.txt).
# # Quad precision in OpenLoops
# OpenLoops has an option for full quad precision calculations. The
# numbers obtained using it represent the highest available precision
# in OpenLoops. As we will see, numbers obtained in quad precision
# (qp) match our benchmark (exact result obtained from analytical
# calculation).
#
# Thanks to M. Zoller and J-N. Lang for providing us with this.
## Load and parse file{{{
import re
def load_file(fn):
    with open('eb2eb_daphne/' + fn) as fp:
        txt = fp.read()
        xis = np.array([
            float(i)
            for i in re.findall(r' xi  *([-\.\dE]*)', txt)
        ])
        valuesPre = re.findall(
            r' xi\*\*2\*([^ ]*)  *([-\.\dE]*)',
            txt
        )
        datOL = np.array([float(v) for k,v in valuesPre if k=='OL'])
        datOLqp = np.array([float(v) for k,v in valuesPre if k=='OL_QP'])
        datLS = np.array([float(v) for k,v in valuesPre if k=='S'])
        datNTS = np.array([float(v) for k,v in valuesPre if k=='NTS'])
        datExact = np.array([float(v) for k,v in valuesPre if k=='exact'])
    return xis, datOL, datOLqp, datLS, datNTS, datExact

xis, datROL, datROLqp, datRLS, datRNTS, datRExact = load_file('softlimit_matels_rand.txt')
xis, datCOL, datCOLqp, datCLS, datCNTS, datCExact = load_file('softlimit_matels_collinitial.txt')
#####################################################################}}}
## Make plots{{{
### Arbitrary phase space point{{{
figure()
plot(xis[:9], abs(datROL  / datRExact - 1)[:9], 'C0o--')
plot(xis[:9], abs(datROLqp/ datRExact - 1)[:9], 'C3o--')
plot(xis[:9], abs(datRLS  / datRExact - 1)[:9], 'C1o--')
plot(xis[:9], abs(datRNTS / datRExact - 1)[:9], 'C2o--')
xscale('log') ; xlim(3e-9,2)
yscale('log') ; ylim(2e-14,2.5)
xlabel(r'$\xi$')
ylabel(r'$\Big|1-\mathcal{M}/\mathcal{M}_\text{exact}\Big|$')
legend(['OpenLoops dp', 'OpenLoops qp', 'Soft', 'Next-to-soft'])
##########################################################}}}
### Collinear phase space point{{{
figure()
plot(xis[:9], abs(datCOL  / datCExact - 1)[:9], 'C0o--')
plot(xis[:9], abs(datCOLqp/ datCExact - 1)[:9], 'C3o--')
plot(xis[:9], abs(datCLS  / datCExact - 1)[:9], 'C1o--')
plot(xis[:9], abs(datCNTS / datCExact - 1)[:9], 'C2o--')
xscale('log') ; xlim(3e-9,2)
yscale('log') ; ylim(1.5e-13,15)
xlabel(r'$\xi$')
ylabel(r'$\Big|1-\mathcal{M}/\mathcal{M}_\text{exact}\Big|$')
legend(['OpenLoops dp', 'OpenLoops qp', 'Soft', 'Next-to-soft'])
##########################################################}}}
#####################################################################}}}
