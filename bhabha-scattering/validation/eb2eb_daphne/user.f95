                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions
  use user_dummy, only: pass_cut, userweight, names, filenamesuffix

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 6
  integer, parameter :: nr_bins = 40
  real, parameter :: &
     min_val(nr_q) = (/ 0.   ,  800.    ,    20.    ,    20.    ,   408.   ,    408./)
  real, parameter :: &
     max_val(nr_q) = (/ 10.  ,  1020.   ,    160.   ,    160.   ,   510.   ,    510.  /)
  integer :: userdim = 0

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer, parameter :: namesLen=6
  integer, parameter :: filenamesuffixLen=10

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

  musq = me**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  integer cut

  read*,cut
  if(cut == 0) then
    softcut = 0._prec
    collcut = 0._prec
  else if (cut == 1) then
    softcut = 1e-11
    collcut = 1e-11
  else if (cut == 2) then
    softcut = 1e-10
    collcut = 1e-11
  else if (cut == 3) then
    softcut = 1e-10
    collcut = 1e-11
  end if

  print*, "softcut is ", softcut
  print*, "collcut is ", collcut

  write(filenamesuffix,'(I1)') cut
  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: quant(nr_q)
  real (kind=prec) :: e_min, th_min, th_max, acoll_max
  real (kind=prec) :: e_e, e_b, th_e, th_b, acoll_eb, minv_eb
  !! ==== keep the line below in any case ==== !!
  call fix_mu

!  pol1 = (/ 0._prec, 0._prec, 0.85_prec, 0._prec /)
  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  quant = 0.
  pass_cut = .true.
  call fix_mu
  
  e_min = 408.
  th_min = 20.
  th_max = 160.
  acoll_max = 10.

  e_e = q3(4)
  e_b = q4(4)
  th_e = acos(cos_th(q1,q3))*180/pi
  th_b = acos(cos_th(q1,q4))*180/pi
  acoll_eb = abs(180. - th_e - th_b)
  minv_eb = sqrt(sq(q3 + q4))

  if(e_e < e_min) pass_cut = .false.
  if(e_b < e_min) pass_cut = .false.

  if(th_e < th_min) pass_cut = .false.
  if(th_b < th_min) pass_cut = .false.

  if(th_max < th_e) pass_cut = .false.
  if(th_max < th_b) pass_cut = .false.

  if(acoll_max < acoll_eb) pass_cut = .false.


  names(1) = "acoll_eb"
  names(2) = "minv_eb"
  names(3) = "th_e"
  names(4) = "th_b"
  names(5) = "e_e"
  names(6) = "e_b"

  quant(1) = acoll_eb
  quant(2) = minv_eb
  quant(3) = th_e
  quant(4) = th_b
  quant(5) = e_e
  quant(6) = e_b


  END FUNCTION QUANT


  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE USEREVENT


                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
